# Le site web https://www.ankataa.com[Ankataa]

![ankataa](ankataa.webp)

An expert linguist providing online courses, in depth research as well an awesome Youtube channel for learning basic Jula and Bambara.

Great for Jula learniers in Ivory Coast. (Just substitute 'ka' for ye, and 'le' or 'lo' for 'don'... and a few others but after all this **is** mandenkan!)

Web site:[Ankataa](https://www.ankataa.com)

Youtube channel:[Na baro kɛ](https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw)
