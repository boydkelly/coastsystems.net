= The Bible
:author: Boyd Kelly
:email:
:date: 2020-05-08
:description: Ressources pour apprendre le Jula
:keywords: "Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula, bambara, bible
:lang: en 
:page-lang: {lang}

image::bibleci.webp[Bible CI dioula,300,role=left]

[role="grid", width="100%",cols="2",frame="topbot",options="none",stripes="none"]
|===
2+|Compare the text of the Bible in Ivory Coast and Burkina Faso Jula with the French Louis Ségon version.
|link:https://play.google.com/store/apps/details?id=org.ipsapps.cotedivoire.dyu.dioula.jula.bible[Bible en Dioula de Côte d'Ivoire]
|link:https://play.google.com/store/apps/details?id=com.dioula.parolededieu.burkinafaso[Bible en Dioula de Burkina Faso]
|===
