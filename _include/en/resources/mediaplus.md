# Radio Media+ in the heart of Bouaké (FM 103)

![radiomedia+](radiomediaplus.webp)

Great resource for picking up the local accent, improving oral comprehension, and augmenting vocabulary.\
FM 103 broadcasting in the Jula language at the following times:

| Day     | Time     | Host | Program       |
| ------- | -------- | ---- | ------------- |
| Tuesday | 9h-10h30 |      | Social issues |
| Sunday  | 9h-10h30 |      | Social issues |

|[Facebook](https://www.facebook.com/RADIOMEDIAPLUSCIBOUAKE)

|Phone: +225 31 63 12 69

|Email: mediaplusci@yahoo.fr
