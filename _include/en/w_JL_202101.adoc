.Kɔrɔsili Sangaso, saan 2021, zanwiyekalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202101.svg[w_JL_202101,300]

* Total words 16351



* Unique words 1137



* Plural words 971



* Words to be confirmed 0
a|

. wala
. ton
. hakilijigi
. sera
. saga
. hami
. dawuda
. waajuli
. olu
. n’u
a|
* sɛgɛsɛgɛrikɛlaw
* ɔriganisasiyɔn
* kunkanbaarabaw
* hakilitigiyako
* weleweledalaw
* novanburukalo
* kunkanbaaraba
* kɛrɛnkɛrɛnnin
* kɛlɛbolowtigi
* kerecɛnɲɔgɔnw
|====
