.Kɔrɔsili Sangaso, saan 2022, desanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202212.svg[w_JL_202212,300]

* Total words 26774



* Unique words 1368



* Plural words 1161



* Words to be confirmed 0
a|

. kɔ
. suu
. kɔrɔ
. hɛɛrɛ
. ton
. don
. delili
. tiɲɛn
. tɛna
. sisan
a|
* ninsɔndiyaninba
* dusukasibagatɔw
* alaɲasiranbagaw
* timinadiyaninw
* sarakalasebaga
* sagakulufitini
* pereperelatigɛ
* ɔriganisasiyɔn
* mɔgɔmurutininw
* weleweledalaw
|====
