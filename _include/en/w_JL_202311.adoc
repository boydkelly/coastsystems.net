.Kɔrɔsili Sangaso, saan 2023, novanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202311.svg[w_JL_202311,300]

* Total words 16325



* Unique words 1055



* Plural words 903



* Words to be confirmed 2
a|

. jira
. sa
. kelen
. deliliw
. wala
. waajuli
. mɔgɔw
. daminɛ
. kumaw
. dusu
a|
* waajulikɛɲɔgɔn
* sɛgɛsɛgɛrikɛla
* ɔriganisasiyɔn
* weleweledalaw
* tilenninyakow
* novanburukalo
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
* desanburukalo
* dafabaliyakow
|====
