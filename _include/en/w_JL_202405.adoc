.Kɔrɔsili Sangaso, saan 2024, mɛkalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202405.svg[w_JL_202405,300]

* Total words 17908



* Unique words 965



* Plural words 832



* Words to be confirmed 0
a|

. t’a
. kosɔbɛ
. k’an
. yezu
. waajuli
. ɲini
. baara
. nin
. nan
. fɔlɔ
a|
* tilenbaliyakow
* pereperelatigɛ
* ninsɔndiyakoba
* hakilitigiyako
* tilenbaliyako
* sɔnɲumantigiw
* sɛtanburukalo
* ɔkutɔburukalo
* novanburukalo
* ninsɔndiyanin
|====
