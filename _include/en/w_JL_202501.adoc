.Kɔrɔsili Sangaso, saan 2025, January 2025
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202501.svg[w_JL_202501,300]

* Total words 19941



* Unique words 1118



* Plural words 951



* Words to be confirmed 22
a|

. k’i
. jaa
. tɔgɔ
. koow
. wagati
. dusu
. aw
. kanuya
. hakili
. faamu
a|
* sebagayabɛɛtigi
* sarakalasebaga
* ɔriganisasiyɔn
* ninsɔndiyatigi
* dusukasibagatɔ
* weleweledalaw
* mɔgɔtilenninw
* malobaliyakow
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
|====
