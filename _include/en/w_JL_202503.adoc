.Kɔrɔsili Sangaso, saan 2025, marisikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202503.svg[w_JL_202503,300]

* Total words 17054



* Unique words 986



* Plural words 852



* Words to be confirmed 44
a|

. kibaro
. janto
. faamu
. wala
. bɔ
. kama
. barokun
. y’o
. wa
. sabu
a|
* ɔriganisasiyɔn
* ɲumanlɔnbaliya
* hakilintanyako
* sɛtanburukalo
* ɔkutɔburukalo
* ninsɔndiyanin
* musoɲɔgɔnɲini
* kerecɛnɲɔgɔnw
* waajulikɛlaw
* sigiyɔrɔsoba
|====
