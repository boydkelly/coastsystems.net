:page-partial:
:uri-apkg: https://gitlab.com/diarrayouss2/mandenkan-docs/-/jobs/artifacts/master/raw/mandenkan/{docname}.adoc.apkg?job=lexicon
:lang: fr
include::locale/attributes.adoc[]

TIP: Le fichier Anki avec le vocabulaire de *_Kódì_* est link:/download/kodi.apkg[téléchargeable ici]. Voir aussi: Qu'est-ce que link:/fr/docs/anki[Anki]? 
