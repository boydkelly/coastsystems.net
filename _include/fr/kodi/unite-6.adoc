[#VI]
= {nbsp}
:sectnums:
:partnums:
:part: VI
:!sect:
:!exercise:
:!chapter-number:

== {nbsp}

=== {nbsp}

Il existe en dioula deux types de circonstants (ce que la grammaire traditionnelle appelle parfois compléments circonstanciels){nbsp}:
Les circonstants marqués et les circonstants non marqués{nbsp};
les circonstants marqués sont des syntagmes nominaux suivis d'une postpositions{nbsp};
les circonstants non marqués sont des syntagmes nominaux non suivis d'une postposition{nbsp};{nbsp}exemples{nbsp}:

[qanda.anki]
à táara Abidjan:: il est parti à Abidjan
à táara lɔ́gɔfɛ lá:: il est parti au marché

Dans le phrase 1, le circonstant est non marqué, dans la phrase 2, le circonstant est marqué (postposition lá).

=== {nbsp}

Les circonstants non marqués peuvent être{nbsp}:

. des noms de lieux
. des éléments nominaux "adverbiaux"{nbsp};{nbsp}en voici quelques exemples{nbsp}:
+
sísan:: maintenant
à kɛ́ sísan::: fais-le maintenant
bì:: aujourd'hui
à nàna bì::: il est venu aujourd'hui
+
jóona:: tôt, vite
à nàna jóona::: il est venu tôt
síni:: demain
à bé na síni::: il vient demain
gbánsan:: "pour rien"
à nàna gbánsan::: il est venu pour rien
+
dɔ́ɔnindɔɔnin:: un tout petit peu
à kà jí min dɔ́ɔnindɔ́ɔnin::: il a bu un tout petit peu d'eau

. des syntagmes à valeur temporelle, ou de manière, etc…{nbsp};
en voici quelques exemples{nbsp}:
+
à nàna sìɲɛ sàba:: il est venu trois fois
à nàna kálo tɛmɛnin:: il est venu le mois passé
ò nàna flà flà:: ils sont venus deux par deux

=== {nbsp}

Les circonstants marqués le sont par des postpositions, lesquelles se divisent en postpositions formelles et postpositions lexicales :

. postpositions formelles{nbsp}: elles n'ont par elle-mêmes aucun sens{nbsp};
elles sont au nombre de cinq{nbsp}: lá/ná, yé, mà, fɛ̀, kàn. 
Si l'on excepte kàn, qui se traduit le plus souvent par "sur", il est extrêmement difficile de donner un sens même approximatif aux postpositions formelles; voici quelques exemples d'emploi{nbsp}:
+
à táara lɔ́gɔfɛ lá:: il est allé au marché
à kà gbán kɛ́ ná lá:: elle a mis du gombo dans la sauce
à má sɔ̀n kúma nìn ná:: il n'a pas accepté cette parole
sèn náani bé sìgilan ná︄:: la chaise a quatre pieds
à bé lá ála lá:: il croit en Dieu
à kà nìn fɔ́ Musa yé:: il a dit cela à Moussa
à kà wári dí Musa mà:: il l donné de l'argent à Moussa
nìn tɛ́ wári yé:: ce n'est pas de l'argent
à ká dí musa yé:: c'est agréable à Moussa
à kà wùlu ta à kú mà:: il a pris le chien par la queue
à kó à mà︄:: il lui a dit
ń táara Musa fɛ̀ yen:: Je suis allé chez Moussa
ù táara ɲɔ̀gɔn fɛ̀:: ils sont partis ensembles
Musa bé màlo fɛ̀:: Moussa veut du riz
màlo bé Musa fɛ̀:: Moussa a du riz
à bé yàn fɛ̀:: c'est par ici
mùru bé tàbali kàn:: le couteau est sur la table
+
[NOTE]
====
[loweralpha]
. lá est la postposition la plus fréquemment employée.
. l'emploi d'une postposition est en général fixe pour chaque verbe.
Il est donc très utile de connaître les principales expressions verbales comportant l'association obligatoire d'un verbe et d'une postposition, comme{nbsp}:
+
--
fɔ́… yé︄:: dire à
dí… ma:: donner à
+
De même, il est utile de connaître certaines expressions (N + postposition) constituant des cirsonstants, comme par exemple{nbsp}:
+
ɲɔ́gɔn fɛ̀:: ensemble
--
. bien distinguer{nbsp}:
+
--
à táara Musa fɛ̀:: il est parti avec Moussa
--
+
de
+
à táara Musa fɛ̀ yèn:: il est parti chez Moussa
====

. postpositions lexicales{nbsp}: ce sont soit des postpositions simples, soit des locutions postpositives, constituées par des éléments lexicaux faisant office de postpositions{nbsp}: exemples{nbsp}:
+
--
kɔ́nɔ:: ventre +
dans, au bout de
kórɔ:: dessous, sens +
à côté, près de
bólo:: main +
dans, par
kɔ́︄:: dos +
après
kó fɛ̀:: dos + postp. +
derrière
ɲá fɛ:: oeil + postp. +
devant
dá fɛ̀:: bouche + postp. +
en face
kɛ̀rɛ fɛ̀︄:: côté + postp. +
à côté
--
+
kɔ́nɔ, kɔ́rɔ, bólo, kɔ́, sont des postpositions lexicales, kɔ́ fɛ̀, ɲá fɛ̀, dá fɛ̀, kɛ̀rɛ fɛ̀, des locutions postpositions lexicales.
Cette liste n'est pas limitative.
+
[NOTE]
====
júkɔrɔ (sous, en dessous de) est une postposition lexicale formée, comme un nom composé, de deux nominaux.
On peut penser que kɔ̀sɔn (à cause de, grâce à) est également un composé, bien que les éléments qui le composent ne soit plus actuellement discernables.
====

=== {nbsp}

La composition est un fait grammatical extrêmement important en _dioula_{nbsp};
les noms composés peuvent être formés de divers éléments, comme le montre les exemples suivants:

bólo (main) + kán (cou):: bólokan (poignet) [N + N]
bólo (main) + lá (sur) + nɛ̀gɛ (fer):: bólolanɛgɛ (bracelet) [N + postp. + N]
fàni (tissue) + fèere (vendre) + -lá (celui qui):: fànifeerela (vendeur de tissu) [N + V + suffixe]
dén (enfant) + mìsɛn (petit) + -ya (état de):: dénmisɛnya (enfance) [N + adj. + suffixe]

Tous ces noms composés ont en commun une caractéristique:
le schéma tonal, la règle étant la suivante :
si le premier élément du composé est à ton bas (fàni dans fànifeerela), le nom composé entier est à ton bas, excepté le dernier élément (lá dans fànifeerela) qui est à ton haut{nbsp};{nbsp}si le premier élément du composé est à ton haut (dén dans dénmisɛnya), le nom composé entier est à ton haut, excepté le dernier élément qui est à une hauteur un peu supérieure à celle des tons hauts précédents.

=== {nbsp}

bé nà et tɛ́ nà sont, comme bé et tɛ́, kà et mà, des marques d'énoncé.
bé nà et tɛ́ nà correspondent généralement au futur, affirmatif et négatif du français{nbsp};{nbsp}exemples :

à bé nà dɔ́ sàn:: il en achètera
à tɛ́ nà dɔ́ sàn:: il n'en achètera pas

=== {nbsp}

túgun est un élément qui se situe en fin d'énoncé, après le(s) circonstants(s), mais avant les particules phrastiques dɛ́, kɛ́, wà.
Suivant qu'il se trouve dans un phrase affirmative ou négative, il se traduit par _encore_ ou _ne plus_{nbsp};{nbsp}exemples :

à kà à kɛ́:: il l'a fait encore (de nouveau) +
il l'a refait
à má nà túgun:: il n'est plus revenu

=== {nbsp}

dɔ́ɔnin est un élément qualifiant, placé après le nom{nbsp};{nbsp}exemples :

à kà jí dɔ́ɔnin mìn:: il a bu un peu d'eau
wári dɔ́ɔnin d'à mà:: donne-lui un peu d'argent

Les qualifiant seront étudiés plus amplement en link:/fr/docs/cours/kodi#VII#_38[VII-1].

[glossary]
== Vocabulaire 

.Vocabulaire {part} 1
[glossary.anki]
gbán:: gombo
lɔ́gɔfɛ:: marché
ná:: sauce
túgun:: encore, ne plus
lá/ná:: (postposition)
fɛ̀:: (postposition)

.Vocabulaire {part} 2
[glossary.anki]
màrfa:: fusil
kɔ́nɔ:: dans
fɔ́:: dire
bón:: maison
jí:: eau
ní... yé:: avec
yé:: (postposition)
kàn:: sur
jùkɔrɔ:: sous, en dessous de
ɲá fɛ̀:: devant
kɔ́ fɛ̀:: derrière

.Vocabulaire {part} 3
[glossary.anki]
dɔ́ɔnin:: un peu
sú:: nuit
àyiwà:: eh bien, bon
dí:: donner
búru:: pain
fàni:: tissu, pagne
dɔ̀n:: danse
mìn:: boire
nɔ́rɔ:: lieu, endroit
bé nà:: (marque d'énoncé)
tɛ́ nà:: (marque d'énoncé)
mà:: (postposition)
ɲyɔ́gɔn fɛ̀:: ensemble
-la:: (suffixe d'agent)

