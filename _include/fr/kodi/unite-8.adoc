[#VIII]
= {nbsp}
:sectnums:
:partnums:
:part: VIII
:!sect:
:!exercise:
:!chapter-number:

==  {nbsp}

===  {nbsp}

L'injonction se forme à l'affirmatif par la copule yé, au négatif par la copule kána. ex.:

à kó musa yé nà:: il dit que Moussa vienne
à kó í yé nà:: il dit que tu viennes
à kó án yé nà:: il dit que nous venons
à kó Musa kána nà:: il dit que Moussa ne vienne pas
à kó í kána na:: il dit que tu ne viennes pas

Nous avons vu en III-1 comment se forme l'impératif à la deuxième personne du singulier{nbsp}la deuxième personne du singulier est la seule personne à laquelle l'impératif et l'injonctif soient différents;
aux autre personnes, les marques des discours direct et indirect sont les mêmes: ex.:

áw kána à kɛ́::  ne le faites pas
à b'à fɔ́ kó áw kána à kɛ́:: il dit que vous ne le fassiez pas
à b'á fɔ́ Musa yé nà:: Il dit que Moussa vienne
à yé nà:: qu'il vienne

===  {nbsp} 

_mín_ est une élément grammatical qui sert à former des phrases complexes: c'est un pronom relatif, qui ne devra pas être confondu avec _mín_, élément interrrogatif (II-0-4).
Le pronom relatif se place derrière le nom auquel il réfère, l'ordre des éléments restant le même;{nbsp}exemples:

mùso mín bé yàn, n bàmuso lò:: la femme qui est ici est ma mère
à kà mùso mìn yé, n bàmuso lò:: la femme qu'il a vue est ma mère
a kà wári dí mùso mín má, n bàmuso lò:: la femme à qui il a donné l'argent c'est ma mère

Le rélatif prend la marque du pluriel: exemples:

à kà mùso mínw fò, n m'ò lɔ́n:: les femmes qu'il a saluées, je ne les connais pas.

[NOTE]
====
[alpha]
. mínw se prononce très fréquemment múnunw
. mín peut s'employer sans référent; exemple:
//
à kà mín fɔ́, mɔ̀gɔ sí má lɔ̀n:: personne ne sait ce qu'il a dit
====

===  {nbsp}
// 0/1.3

_fó_ comme _ní_ et _mín_ sert à construire des phrases complexes;
deux constructions sont possibles:

[upperalpha]
. proposition 1 + fó + proposition 2
. proposition 1 + fó kà + (sujet) + verbe

Exemples:

[qanda]
à kà báara kɛ́ fó à sɛ̀gɛla:: il a travaillé jusqu'à se fatiguer
à kà báara kɛ́ fó kà sɛ̀gɛ:: il a travaillé jusqu'à se fatiguer
à kúmana fó Amadu nána:: il a parlé jusqu'à ce que Amadou vienne
à kúmana fó ka Amadu ná:: il a parlé jusqu'à la venue d'Amadou

[NOTE]
====
. si le sujet est le même dans les deux propositions, il n'est pas répété après _fó_ dans le type de construction B.
. les deux constructions ne sont pas, du point de vue du sens, équivalentes;{nbsp}la phrase 3 signifie: il a parlé de telle manière qu'Amadou est venu;{nbsp}le sens de l phrase 4 est le suivant: il a parlé jusqu'à la venu d'Amadou.
====

===  {nbsp}

_drɔ́n, fána, yɛ̀rɛ, kɔ̀ni_ sont des éléments qui peuvent se placer soit derrière le nom, soit à la fin de l'énoncé. 
Nous n'envisagerons dans ce cours que le premier cas;{nbsp}exemples:

mùso drɔ́n nàna:: la femme seulement est venue
mùso fána nàna:: la femme aussi est venue
mùso yɛ̀rɛ nàna:: la femme elle-même est venue
mùso kɔ̀ni nàna:: la femme, pour sa part est venue

Avec un pronom, on trouvera les formes suivantes: _né drɔ́n…, íle drɔ́n…_, etc..., de même, on dira _né kɔ̀ni…_

_drɔ́n, fána yɛ̀rɛ, kɔ̀ni_ peuvent êre employées derrière un nom ou un pronom en fonction d'objet ou de circonstant;{nbsp}exemples:

à ká à di né kɔ̀ni mà:: c'est à moi qu'il l'a donné
à kà ń mùso fána yé:: il a vu aussi ma femme

=== {nbsp}

_kà bán, kà mɛ́ɛn, kà ɲan_ sont des expressions très usuelles formées avec le connectif kà (cf 4-1-4) et un verbe;{nbsp}exemples:

à kà báara kɛ kà ban:: il a fini de travailler
à kà báara kɛ kà mɛ́ɛn:: il a travaillé longtemps (il a "duré" au travail)
à kà báara kɛ kà ɲan:: il a bien travaillé

=== {nbsp}

Le pronom réfléchi est le même que le pronom sujet, sauf ou la troisième personne du singulier ou _í_ remplace _à_;{nbsp}exemples:

ń kà ń sìgi:: je me suis assis
à k'i sìgi:: il s'est assis

NOTE: _ù_ est employé de préférence à _ò_ comme pronom réfléchi de la troisième personne du pluriel.

=== {nbsp}

Il existe en _dioula_ plusieurs type de dérivations verbales dont les deux plus fréquents sont la dérivation par préfixation de _lá-_, et la dérivation par redoublement.

La préfixation de _lá-_ transforme un verbe intransitif en verbe transitif;{nbsp}un verbe transitif auquel est ajouté _lá_ reste transitif{nbsp}les verbes dérivés en _lá-_ ont souvent une veleur factive;{nbsp}ex.:

à kà Musa lána:: il a fait venir Moussa
à kà Awa lákàsi:: il a fait pleurer Awa
à kà à lábɔ̀:: il l'a fait sortir

Les verbes dérivés par redoublement ont une valeur de répétition, d'intensité;{nbsp}exemples:

fára:: déchirer
fárafara:: déchirer en petits morceaux, déchiqueter…
tìgɛ:: couper
tìgɛtigɛ:: coupler en petits morceaux, hacher…

=== {nbsp}

Les nombres à partir de 10 se disent de la façon suivante:

[width="100%",cols="2",frame="topbot",opts="none",stripes="even"]
|====
|tán ní kélen|11
|tán ní flà|12
|tán ní sàba|13
|tán ní kɔ̀nɔntɔn|19
|mùgan ní kélen|21
|mùgan ní kɔ̀nɔtɔn|29
|bí sàba|30
|bí náani|40
|bí kɔ́nɔntɔ|90
|bí kɔ́nɔntɔ ní kélen|91
|kɛ̀mɛ|100
|kɛ̀mɛ flà|200
|kɛ̀mɛ flà ní bí sàba ní sàba|233
|wáa kélen a|1000footnote:[on emploie de préférence _wáa_ pour 5000 frs et _báa_ pour 1000.]
|====

[NOTE]
====
. _bí_ (10) n'est utilisé que pour compter à partir de 30.
. se souvenir que _kɛ̀mɛ flà_ = 200 ou 1000 francs., etc... link:/fr/docs/cours/kodi#[(cf.7 1 15)]
. pour les nombres supérieurs à 100, on remplace souvent _ní_ par _à ní_;{nbsp}exemple:

wáa kélen  à ní kɛ̀mɛ flà:: (6000 francs)
====

=== {nbsp}

il existe plusieurs façons d'indiquer l'âge, plus ou moins courantes:

[qanda]
Musa yé sánji jòli yé?:: Musa yé sánji tán yé
Musa sí yé jòli yé?:: Musa sí yé sán tán yé
Musa sánji yé jòli?:: Musa sánji yé tán
Musa sánji dá yé jòli?:: Musa sánji dá yé tán
Sáni jòli bé Musa fɛ̀?:: Sánji tán bé Musa fɛ̀
Musa bé sánji jòli?:: Musa bé sánji tán

Toutes ces formes sont équivalentes et signifient: _quel âge a Moussa?  Il a dix ans._ 
Nous choisirons dans les exercices la forme No 3.

[NOTE]
====
Une forme différente est employée pour exprimer un âge approximatif;{nbsp}exemples:

Musa bé sánji jòli bɔ́:: quel âge a à peu près Moussa?
à bé sán tán bɔ́:: il a à peu près dix ans

====

=== {nbsp}

On exprime le nom de quelque chose ou le prénom de quelqu'un par les formes:

à tɔ́gɔ:: (Bakari, Abidjan…)
à tɔ́gɔ kó:: (Bakari, Abidjan…)
à tɔ́gɔ yé:: (Bakari, Abidjan…)
à tɔ́gɔ bé:: (Bakari, Abidjan…)

Les questions correspondant à ces quatre réponses sont: _à tɔ́gɔ dì? à tɔ́gɔ kó dì? à tɔ́gɔ bé dì?_  Toutes ces formes sont équivalentes: exemple:

à tɔ́gɔ dì?:: à tɔ́gɔ kó Bakari
comment s'appelle-t-il?:: il s'appelle Bakari.

[glossary]
== Vocabulaire 

.Vocabulaire {part} 1
[glossary.anki]
sɛ́bɛ:: papier, lettre
blà:: mettre
fára:: déchirer
fìli:: jeter
áa:: ah!
sabari:: pardon
fén:: chose
mín:: (pronom relatif)
lá-:: (dérivatif verbal)

.Vocabulaire {part} 2
[glossary.anki]
téri:: ami
dòn:: entrer
fòro:: champ
mìsiri:: mosquée
tó:: rester
tɔ́gɔ::  nom (pour une chose){nbsp}prénom (pour une personne)
drɔ́n:: seulement
yɛ̀rɛ:: même (lui-même)
kɔ̀ni:: quant à
fána:: aussi
yé:: (marque d'énoncé)

.Vocabulaire {part} 3
[glossary.anki]
bán:: finir
nyɛ̀/nyà:: être bien
mɛ́ɛn:: durer
sɛ̀gɛ:: fatiguer
í ni cé:: merci
lò:: s'arrêter, être debout
sàra:: payer
sánji:: année
fó, fó ka:: jusqu'à
wɔ́ɔrɔ:: 6
wólónflà:: 7
ségi/séegi:: 8
kɔ̀nɔntɔ:: 9
tán:: 10
mùgan:: 20
wáa:: 1000, 5000 francs
bí:: 10 (dans les chiffres supérieurs à 30)

== {nbsp}

=== {nbsp}

[width="100%",cols="2",frame="none",opts="none",stripes="none", grid="none"]
|====
a|
Musa:: sɛ́bɛ mín tùn bé yèn, í kà à blà mín?
Awa:: í kɔ̀rɔmuso lò kà à fárafara kà á +
fili
Musa:: àyíwà, táa à lánà
Awa:: áa, ń dɔ́gɔcɛ, sábali kánà à bùgɔ dɛ́
a|
{zwsp}:: Le papier qui était là, où l'as-tu mis?
{zwsp}:: C'est ta grande soeur qui l'a déchiré en petits morceaux et l'a jeté.
{zwsp}:: Ah bon, fais-la venir!
{zwsp}:: Ah, mon petit frère, pardon, ne la frappe pas surtout!
|====

=== Exercise 

// VIII-1-3

nìn lámɛ́n

à kà mɔ̀gɔ min fò, Musa lò wa?::
ɔ̀nhɔ́n, à kà mɔ̀gɔ mín fò, Musa lò

ségi à kàn

à kà mɔ̀gɔ min fò, Musa lò wa?::
ɔ̀nhɔ́n, à kà mɔ̀gɔ mín fò, Musa lò

//

. à kà mɔ̀gɔ mín fò, Amadu lò wà?
. à kà mùso mín fò, a bámuso lò wà?
. à kà mùso mín fò, Musa bámuso lò wà?
. à kà cɛ̀ kɔrɔba mín fɔ́, Musa fàcɛ lò wà?
. à ka mùso fítini mín fò, Awa dɔ̀gɔmuso lò wà?
. í ka mùso mí fò, Awa bámuso lò wà?

.Jabiliw
[%collapsible]
====
. ɔ̀nhɔ́n, à kà mɔ̀gɔ mín fò, Amadu lò.
. ɔ̀nhɔ́n, à kà mùso mín fò, a bámuso lò.
. ɔ̀nhɔ́n, à kà mùso mín fò, Musa bámuso lò.
. ɔ̀nhɔ́n, à kà cɛ̀ kɔrɔba mín fɔ́, Musa fàcɛ lò.
. ɔ̀nhɔ́n, à ka mùso fítini mín fò, Awa dɔ̀gɔmuso lò.
. ɔ̀nhɔ́n, í ka mùso mí fò, Awa bámuso lò.
====

=== Exercise

(la principale seulement est à transformer)

nìn lámɛ́n

mùso mín nàna, ń kà à lɔ́n
mùso mín nàna, ń mà à lɔ́n

//

. cɛ̀ kɔrɔba mín nàna, à fàcɛ tɛ́.
. mùso flà mínw táara, à dɔ́gɔmusu lo.
. à kà wári dí cɛ̀ mín mà, Musa lò.
. à kà marfa dí cɛ̀ mín mà, Amadu lò.
. à kà sɛ́bɛ dí mùso mín má, Awa lò.
. à kà sɛ́bɛ mín fárafara, n tá tɛ.

.Jabiliw
[%collapsible]
====
. cɛ̀ kɔrɔba mín nàna, à fàcɛ lò.
. mùso flà mínw táara, à dɔ́gɔmusu tɛ́.
. à kà wári dí cɛ̀ mín mà, Musa tɛ́.
. à kà marfa dí cɛ̀ mín mà, Amadu tɛ́.
. à kà sɛ́bɛ dí mùso mín má, Awa tɛ́.
. à kà sɛ́bɛ mín fárafara, n tá lò.
====

=== Exercise

nìn lámɛ́n

