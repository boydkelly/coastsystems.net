# Radio France International Mandenkan

![RFI](rfi_mandenkan.webp)

Ecoutez l’actualité internationale, africaine, française et régionale
(Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse
régionale.

Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de
l'ouest. Mise-à-jour quotidiennement. Les infos en 10 minutes, suivi par une
émission d'actualité de 20 minutes du lundi au vendredi à 8h00 et 12h00.

Site web: [rfi](https://rfi.fr/ma)|
