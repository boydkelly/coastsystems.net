.Kɔrɔsili Sangaso, saan 2021, desanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202112.svg[w_JL_202112,300]

* Nombre total de mots 29515



* Mots uniques 1530



* Mots pluriels 1268



* Mots à confirmer 0
a|

. miiri
. b’o
. bibulu
. taga
. jija
. faamu
. yala
. t’a
. ɲini
. don
a|
* fanibugudilanbagaw
* farisogobanatigiw
* israɛldenɲɔgɔnw
* tilenbaliyakow
* sɛgɛsɛgɛrikɛla
* sarakalasebaga
* sagakulufitini
* ninsɔndiyatigi
* mɔgɔsabalininw
* kunkanbaarabaw
|====
