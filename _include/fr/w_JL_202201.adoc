.Kɔrɔsili Sangaso, saan 2022, zanwiyekalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202201.svg[w_JL_202201,300]

* Nombre total de mots 17006



* Mots uniques 1019



* Mots pluriels 871



* Mots à confirmer 0
a|

. fɛɛn
. delili
. ale
. t’a
. saga
. kuma
. loon
. kɔrɔtanin
. kosɔn
. nan
a|
* dusukasibagatɔw
* tilenbaliyakow
* sɛgɛsɛgɛrikɛla
* ɔriganisasiyɔn
* kunkanbaarabaw
* hakilitigiyako
* zeruzalɛmukaw
* sɛtanburukalo
* sagadɛndɛbaga
* ɔkutɔburukalo
|====
