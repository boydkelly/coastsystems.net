const fs = require('fs');
const path = require('path');
const glob = require('glob');
const cheerio = require('cheerio');
const asciidoctor = require('@asciidoctor/core')();
const [, , inputDir, outputDir] = process.argv;

// Check if both arguments are provided
if (!inputDir || !outputDir) {
  console.error('Usage: node adoc2dcomp.cjs <input-directory> <output-directory>');
  process.exit(1);
}

// Ensure the output directory exists
fs.mkdirSync(outputDir, { recursive: true });

// Determine if the input is a file or directory
let sourceFiles = [];

if (fs.statSync(inputDir).isDirectory()) {
  // Get all .adoc files in the input directory and its subdirectories
  sourceFiles = glob.sync(`${inputDir}/**/*.adoc`);
} else if (fs.statSync(inputDir).isFile() && inputDir.endsWith('.adoc')) {
  // Process a single .adoc file
  sourceFiles = [inputDir];
} else {
  console.error('Input must be a directory or a single .adoc file.');
  process.exit(1);
}

if (sourceFiles.length === 0) {
  console.error('No .adoc files found in the input directory:', inputDir);
  process.exit(1);
}

// Function to capitalize the first letter of the filename (excluding the extension)
const capitalizeFirstLetter = (filename) => {
  const extPart = path.extname(filename);
  const namePart = path.basename(filename, extPart);
  return filename.charAt(0).toUpperCase() + namePart.slice(1) + extPart;
};

sourceFiles.forEach((file) => {
  // Check if it's a file
  if (fs.statSync(file).isFile()) {
    // Ensure it's a file
    const relativePath = path.relative(inputDir, file); // Get the relative path from the input directory
    const titleCasedFilename = capitalizeFirstLetter(relativePath); // Capitalize the first letter of the filename

    const outputPath = fs.statSync(inputDir).isFile()
      ? path.join(
          outputDir,
          capitalizeFirstLetter(path.basename(inputDir)).replace(/\.adoc$/, '.svelte')
        )
      : path.join(outputDir, titleCasedFilename.replace(/\.adoc$/, '.svelte'));
    // Ensure the output directory exists
    fs.mkdirSync(path.dirname(outputPath), { recursive: true });

    // Read the file content
    const content = fs.readFileSync(file, 'utf8');

    const lang = 'fr'; // Set the language if necessary
    const html = asciidoctor.convert(content, {
      standalone: false,
      safe: 'unsafe',
      attributes: {
        showtitle: true,
        includedir: lang ? `_include/${lang}/` : '_include/',
        imagesdir: '/images/',
        'allow-uri-read': '',
        'skip-front-matter': true,
        icons: 'font'
      }
    });

    // Convert each .adoc file
    const $ = cheerio.load(html, { normalizeWhitespace: false }, false);

    // Add a <tbody> to each table for Svelte 5
    $('table').each((_, table) => {
      const $table = $(table);

      // Add Carbon Design class
      $table.addClass('bx--data-table');

      // Ensure a <tbody> exists
      if ($table.find('tbody').length === 0) {
        const tbody = $('<tbody></tbody>');
        $table.children('tr').each((_, tr) => {
          tbody.append(tr);
        });
        $table.append(tbody);
      }
    });

    // svelte complains when there is no title attribute to iframes
    $('iframe').each((_, iframe) => {
      const $iframe = $(iframe);
      if (!$iframe.attr('title')) {
        $iframe.attr('title', 'no_title');
      }
    });

    // Add Carbon Design classes to lists
    $('ul').addClass('bx--list--unordered bx--list--expressive');
    $('ol').addClass('bx--list--ordered bx--list--expressive');
    $('ol').removeClass('arabic');
    $('li').addClass('bx--list__item');

    $('p').addClass('bx--type-body-02');

    // Convert each .adoc file
    // Replace href='#' with href='#top'
    $('a[href="#"]').each((_, element) => {
      $(element).attr('href', '#top');
    });

    const modifiedHtml = $.html()
      .replace(/\u200B/g, '')
      .replace(/\u2009/g, '');
    // Get the modified HTML from Cheerio
    // Write the modified HTML to the output directory
    fs.writeFileSync(outputPath, modifiedHtml);
    console.log(`Converted: ${file} -> ${outputPath}`);
  }
});

// console.log('Conversion complete!');
