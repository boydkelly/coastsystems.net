#!/usr/bin/bash

# Input and output files
FONT_LIST="$HOME/dev/fontlist/fontlist.txt"
project=fontlist
for x in tsv adoc json; do export "${x}=${project}.$x"; done
number=$(wc -l "$FONT_LIST" | awk '{print $1}')
proverbs=./src/assets/metadata/proverbs.json
sample="Mɔgɔkɔrɔbaw kaɲi ka kɛ ni ŋania ɲuman ye"
base="https://fonts.google.com/specimen/"
text="M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye"
docdate=$(LC_TIME=fr_FR.UTF-8 date +"%b %d %Y")

cat <<EOL >"$adoc"
= Polices (fontes) qui gèrent les langues Mandingues
Boyd Kelly
{docdate}
:email: <bkelly@coastsystems.net>
:description: liste de polices (fontes) qui gère les langues mandingues (Bambara, Dioula et plus)
:keywords: dioula, bambara, mandenkan, mandenkan.com, cours, apprendre, côte d'ivoire, abidjan, afrique, africa, langues, languages
:revdate: {docdate}
:revnumber: 1.0
:copyright: Boyd Kelly
:orgname: coastsystems.net
:orguri: https://coastsytems.net
:date: $(date -I)
:sectnums:
:sectnumlevels: 1
:pdf-themesdir: _resources/themes
:pdf-theme: fontlist
:pdf-fontsdir: _resources/fonts
:lang: fr

Il y a plus de 1700 « familles » de polices (fonts) disponible chez Google.
À {docdate} il n'y a que $number familles qui gèrent correctement les langues mandingues, y compris le bambara et le dioula.
Les polices ci dessus{empty}footnote:[À http://coastsystems.net/fr/docs/fontlist[coastsystems.net] cette liste est reverifié quotidiennement pour les nouveautés!];
sont les seuls en effet qui rendent correctement les glyphs: ɛ Ɛ; ɔ Ɔ; ɲ Ɲ; ŋ Ŋ
Voici la liste complète avec échantillon d'écriture:

== Polices (fontes)

EOL

# Loop through each font in the list
echo "[start=1]" >>$adoc
while IFS= read -r font; do
  # Extract the font name without the extension
  font_name=$(basename "$font" .ttf)
  role_name=${font_name//_/-}
  urlfont=$(echo "$font_name" | tr '_' '+') && urlfont=${urlfont%-*}

  heading_name=${font_name%-*}
  heading_name=${heading_name//_/ }
  dyu=$(yq -oj '.[] | .dyu' $proverbs | shuf -n 1 | sed 's/^"\(.*\)"$/\1/')

  # Add the text with the role corresponding to the font
  #echo ". $heading_name: +" >>$adoc
  printf '. %s%s?preview.text=%s&preview.text_type=custom#standard-styles[%s,window=_blank] +\n' $base "$urlfont" $text "$heading_name" >>$adoc
  echo "[.$role_name]#$sample# +" >>$adoc
  echo "[.$role_name]#$dyu#" >>"$adoc"
  # 	#printf "%s%s?preview.text=%s&preview.text_type=custom#standard-styles,\n" "$base" "$urlfont" "$text" >> $linktest

done < <(sort "$FONT_LIST")
asciidoctor-pdf -D ./static/download/ $adoc
rm $adoc
