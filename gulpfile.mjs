import gulp from 'gulp';
import glob from 'glob';
import shell from 'gulp-shell';
import debug from 'gulp-debug';
import replace from 'gulp-replace';
import { execSync } from 'child_process';

gulp.task('cleanDest', shell.task(['sh cleandest.sh']));

gulp.task('fuse', shell.task(['node build-data.js', 'node build-index.js']));

gulp.task('buildComponents', (done) => {
  const languages = ['fr', 'en'];
  for (const lang of languages) {
    execSync(
      `node adoctocomp.cjs _include/${lang}/resources/ src/lib/components/${lang}/resources/`,
      { stdio: 'inherit' }
    );
  }
  done(); // Simply call done() to signal completion
});

gulp.task('fix-dom', () => {
  const langs = ['en', 'fr'];

  // Generate tasks for all matched .svelte files
  const tasks = langs.flatMap((lang) =>
    glob
      .sync(`src/lib/components/${lang}/resources/*.svelte`)
      .map((file) => `node ./mod-dom.js ${file}`)
  );

  return shell.task(tasks)();
});

gulp.task('buildResources', (done) => {
  const languages = ['fr', 'en'];

  for (const lang of languages) {
    // Step 1: Build the components
    execSync(
      `node adoctocomp.cjs _include/${lang}/resources/ src/lib/components/${lang}/resources/`,
      { stdio: 'inherit' }
    );

    // Step 2: Fix the DOM for each created file
    const svelteFiles = glob.sync(`src/lib/components/${lang}/resources/*.svelte`);
    svelteFiles.forEach((file) => {
      execSync(`node ./mod-dom.js ${file}`, { stdio: 'inherit' });
    });
  }

  done(); // Signal task completion
});

gulp.task(
  'buildDocs',
  shell.task([
    // 'src/assets/metadata/plugins.sh',
    //'node adoctohtml.cjs src/routes/fr/docs/cours/baoule src/routes/fr/docs/cours/baoule/',
    // 'node adoctocomp.cjs src/routes/fr/docs/cours/kodi/kodi.adoc src/routes/fr/docs/cours/kodi/',
    // 'node adoctocomp.cjs src/routes/fr/docs/cours/julakankaran/julakankaran.adoc src/routes/fr/docs/cours/julakankaran/',
    // 'node adoctocomp.cjs src/routes/fr/docs/code_ortho/code_ortho.adoc src/routes/fr/docs/code_ortho/',
    // 'node adoctocomp.cjs src/routes/fr/docs/anki/anki.adoc src/routes/fr/docs/anki/',
    // 'node adoctocomp.cjs src/routes/fr/docs/mandenkan-bibles/mandenkan-bibles.adoc src/routes/fr/docs/mandenkan-bibles/',
    // 'node adoctocomp.cjs src/routes/en/docs/bambara-manual/bambara-manual.adoc  src/routes/en/docs/bambara-manual/',
    // 'node adoctocomp.cjs src/routes/en/docs/w_jl/w_jl.adoc  src/routes/en/docs/w_jl/'
    // 'node adoctocomp.cjs src/routes/fr/docs/w_jl/w_jl.adoc  src/routes/fr/docs/w_jl/'
    // 'node adoctocomp.cjs src/routes/en/about/about.adoc  src/routes/en/about/',
    // 'node adoctocomp.cjs src/routes/en/contact/contact.adoc  src/routes/en/contact/',
    // 'node adoctocomp.cjs src/routes/fr/about/about.adoc  src/routes/dyu/about/',
    // 'node adoctocomp.cjs src/routes/fr/about/about.adoc  src/routes/bm/about/',
    // 'node adoctocomp.cjs src/routes/fr/about/about.adoc  src/routes/fr/about/',
    // 'node adoctocomp.cjs src/routes/fr/contact/contact.adoc  src/routes/fr/contact/'
  ])
);

//lets not do this for now.  some +page.adoc get clobbered.   and maybe they should be done by po4a anyways
gulp.task('copyLangs', function (done) {
  const languages = ['fr', 'dyu', 'bm'];

  //const copyTasks = languages.map(
  //  (lang) =>
  //    function copyLang() {
  //      return gulp.src(`src/routes/en/**/*.{js,svelte}`).pipe(gulp.dest(`src/routes/${lang}`));
  //    }

  const copyTasks = languages.map(
    (lang) =>
      function copyLang() {
        return (
          gulp
            //    .src(['src/routes/en/**/*.{js,svelte}', 'src/routes/en/docs/**/+page.adoc'])
            //copy adoc files without replacing embedded language symbols
            .src(['src/routes/en/docs/**/+page.adoc'])
            .pipe(debug({ title: `Copying to ${lang}:` }))
            .pipe(gulp.dest(`src/routes/${lang}/docs`))
          //            .pipe(gulp.dest(`src/routes/${lang}`))
        );
      }
  );

  return gulp.series(...copyTasks)(done);
});

// Task to replace language references for 'fr', 'dyu', 'bm'
gulp.task('replaceLangs', function (done) {
  const languages = ['fr', 'dyu', 'bm'];

  const replaceTasks = languages.map(
    (lang) =>
      function replaceLang() {
        return gulp
          .src(['src/routes/en/**/+page.{js,svelte}', 'src/routes/en/**/+layout.{js,svelte}'], {
            base: 'src/routes/en'
          }) // Preserve directory structure
          .pipe(debug({ title: `Copying to ${lang}:` }))
          .pipe(replace(`/en/`, `/${lang}/`)) // Replace `/en/` in file paths
          .pipe(replace(/'en'/, `'${lang}'`)) // Replace 'en' in content (ensure this won't cause unwanted replacements)
          .pipe(gulp.dest(`src/routes/${lang}`));
      }
  );

  // above previoulsly copying all js and svelte files.  Its only now doing page and layout, but this is for safe measure
  // and will let us not commit the Content.svelte files generated in udhr
  return gulp.series(
    ...replaceTasks,
    shell.task(['/bin/bash ./work/udhr/mk_udhr.svelte.sh'])
  )(done);
});

// Define a Gulp task to run po4a
gulp.task(
  'translate',
  shell.task([
    '/bin/bash po4a fr.cfg',
    '/bin/bash po4a dyu.cfg',
    '/bin/bash po4a bm.cfg',
    '/bin/bash po4a bci.cfg'
  ])
);

// Define a build task that depends on 'replaceLangs' and 'po4a'
gulp.task(
  'build',
  gulp.series('fuse', 'replaceLangs', 'translate', 'buildResources', 'buildDocs', 'cleanDest')
);

// Default task (you can customize this)
gulp.task('default', gulp.series('build'));
