# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2024-01-21 15:55+0000\n"
"PO-Revision-Date: 2024-01-21 15:58+0000\n"
"Last-Translator: Boyd Kelly <bkelly@coastsystems.net>\n"
"Language-Team: \n"
"Language: bm\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.2\n"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "saga"
msgstr "saga"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "a"
msgstr "a"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sheep"
msgstr "mouton"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hat"
msgstr "sac"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "baara"
msgstr "baara"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɑ"
msgstr "ɑ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "aa"
msgstr "aa"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "work"
msgstr "travail"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dark (longer a)"
msgstr "(a allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sanji"
msgstr "sanji"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɑ̃"
msgstr "ɑ̃"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "an"
msgstr "an"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rain"
msgstr "pluie"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "like lawn"
msgstr "sans"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "baga"
msgstr "baga"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "b"
msgstr "b"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "porridge"
msgstr "bouillie"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "big"
msgstr "bol"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "cɛ"
msgstr "cɛ"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "tʃ"
msgstr "tʃ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "c"
msgstr "c"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "man"
msgstr "cɛ"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "match"
msgstr "match"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "disi"
msgstr "disi"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "e"
msgstr "e"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "d"
msgstr "d"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "chest"
msgstr "poitrine"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dot"
msgstr "dos"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bese"
msgstr "bese"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "machete"
msgstr "machète"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "make"
msgstr "été"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "feere"
msgstr "feere"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "e:"
msgstr "e:"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ee"
msgstr "ee"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sell"
msgstr "vendre"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out e)"
msgstr "(e allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sen"
msgstr "sen"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sang [Approximate]"
msgstr "brin"

#. type: Hash Value: locale
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr "bm"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "foot"
msgstr "pied"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛ̃"
msgstr "ɛ̃"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kɛnɛya"
msgstr "kɛnɛya"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛ"
msgstr "ɛ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "health"
msgstr "santé"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "met"
msgstr "mets"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mɛɛn"
msgstr "mɛɛn"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛɛ"
msgstr "ɛɛ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "last"
msgstr "durer"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out short e)"
msgstr "(e allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bɛn"
msgstr "bɛn"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛn"
msgstr "ɛn"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "agree"
msgstr "entente"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(nasalized short e)"
msgstr "(e allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "foro"
msgstr "foro"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "f"
msgstr "f"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "field"
msgstr "champ"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "fox"
msgstr "fer"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mɔgɔ"
msgstr "mɔgɔ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "g"
msgstr "g"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "person"
msgstr "personne"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "get"
msgstr "gant"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gbɛlɛ or gwɛlɛ"
msgstr "gbɛlɛ or gwɛlɛ"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɡ͡b or ɡʷ"
msgstr "ɡ͡b or ɡʷ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gw or gb"
msgstr "gw ou gb"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hard"
msgstr "difficile"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gb or gw"
msgstr "gb ou gw"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hakili"
msgstr "hakili"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "h"
msgstr "h"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "intelligence"
msgstr "intelligence"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hello"
msgstr "hello.footnote:[anglais, aspiré]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "misi"
msgstr "misi"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "i"
msgstr "i"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "cow"
msgstr "boeuf"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "free"
msgstr "si"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "miiri"
msgstr "miiri"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ii"
msgstr "ii"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "thought"
msgstr "pensée"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(lengthened i)"
msgstr "(i allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "min"
msgstr "min"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ĩ"
msgstr "ĩ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "in"
msgstr "in"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "drink"
msgstr "boire"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nasilized i like seen [Approximate]"
msgstr "i nasalisé [à ne pas confondre avec 'brin' /ɛ̃/]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ji"
msgstr "ji"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɟ or dʒ"
msgstr "ɟ or dʒ"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "j"
msgstr "j"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "water"
msgstr "eau"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "Abidjan"
msgstr "Abidjan"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kitabu"
msgstr "kitabu"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "k"
msgstr "k"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "livre"
msgstr "livre"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kiosque"
msgstr "kiosque"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "lolo"
msgstr "lolo"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "l"
msgstr "l"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "star"
msgstr "étoile"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "late"
msgstr "lame"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "maro"
msgstr "maro"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "m"
msgstr "m"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rice"
msgstr "riz"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mat"
msgstr "mère"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nɔnɔ"
msgstr "nɔnɔ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "n"
msgstr "n"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "milk"
msgstr "milk"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "note"
msgstr "nom"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɲɔ"
msgstr "ɲɔ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɲ"
msgstr "ɲ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mil"
msgstr "mil"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "oignon"
msgstr "oignon"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ŋɔni"
msgstr "ŋɔni"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ŋ"
msgstr "ŋ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "thorn"
msgstr "épine"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ten glasses (ng) [Approximate]"
msgstr "parking [Approximative]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bolo"
msgstr "bolo"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "o"
msgstr "o"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hand"
msgstr "bras"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "no"
msgstr "no"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "looru"
msgstr "looru"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "oo"
msgstr "oo"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "five"
msgstr "cinq"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out o)"
msgstr "(o allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bon"
msgstr "bon"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "õ"
msgstr "õ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "on"
msgstr "on"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "house"
msgstr "maison"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nom [Approximate]"
msgstr "nom[Approximative]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sɔrɔ"
msgstr "sɔrɔ"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔ"
msgstr "ɔ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "find"
msgstr "trouver"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "not"
msgstr "nom"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "fɔɔnɔ"
msgstr "fɔɔnɔ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔɔ"
msgstr "ɔɔ"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vomit"
msgstr "vomir"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out ɔ)"
msgstr "( ɔ allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dɔn"
msgstr "dɔn"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔn"
msgstr "ɔn"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "danse"
msgstr "danser"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "like 'ahn' [Approximate]"
msgstr "an/mon[Approximative]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pan"
msgstr "pan"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "p"
msgstr "p"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "jump"
msgstr "sauter"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pen"
msgstr "pont"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bɔrɔ"
msgstr "bɔrɔ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "r"
msgstr "r"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sac"
msgstr "sac"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pero [Spanish]"
msgstr "pero [espagnol]"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sara"
msgstr "sara"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "s"
msgstr "s"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pay"
msgstr "payer"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sauce"
msgstr "sauce"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "toto"
msgstr "toto"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "t"
msgstr "t"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rat"
msgstr "rat"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "table"
msgstr "table"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kura"
msgstr "kura"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "u"
msgstr "u"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "new"
msgstr "nouveau"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "boot"
msgstr "loup"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "suuri"
msgstr "suuri"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "uu"
msgstr "uu"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "lower"
msgstr "baisser"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(lenthened u)"
msgstr "(u allongé)"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kunkolo"
msgstr "kunkolo"

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ũ"
msgstr "ũ"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "un"
msgstr "un"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "head"
msgstr "tête"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(nasal u)"
msgstr "(u nasalisé) pas comme en français"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vote [borrowed from French]"
msgstr "votekɛ [emprunt du français]"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "v"
msgstr "v"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vote"
msgstr "vote"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vol"
msgstr "vol"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "wari"
msgstr "wari"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "w"
msgstr "w"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "money"
msgstr "argent"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "watch"
msgstr "doigt"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "yan"
msgstr "yan"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "y"
msgstr "y"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "here"
msgstr "ici"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "yes"
msgstr "papaye"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "zɔnzɔn"
msgstr "zɔnzɔn"

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "z"
msgstr "z"

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "shrimp"
msgstr "crevette"

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "zoo"
msgstr "zoo"
