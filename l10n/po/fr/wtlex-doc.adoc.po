# French translations for Blog package
# Copyright (C) 2024 Boyd Kelly
# This file is distributed under the same license as the Blog package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-02-23 19:32+0000\n"
"PO-Revision-Date: 2024-08-17 14:10+0000\n"
"Last-Translator: Boyd Kelly <bkelly@coastsystems.net>\n"
"Language-Team: none\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.4.4\n"

#. type: Title =
#: src/routes/en/docs/wtlex-doc/+page.adoc:1
#, no-wrap
msgid "Lexique WT mandenkan"
msgstr ""

#. type: Title ==
#: src/routes/en/docs/wtlex-doc/+page.adoc:18
#, no-wrap
msgid "Préface"
msgstr ""

#. type: Plain text
#: src/routes/en/docs/wtlex-doc/+page.adoc:25
msgid ""
"Ce document est un outil d'apprentissage de Jula pour le champ de Côte "
"d'Ivoire.  Il comprend un vocabulaire et expressions théocratiques en Jula "
"de Burkina Faso.  Quelques expressions et mots équivalents sont inclus en "
"Bambara, et parfois aussi des variantes 'ivoiriennes' aussi.  Ceci est un "
"travail en cours.  Toutes les suggestions et ou corrections sont les "
"bienvenus."
msgstr ""

#. type: Plain text
#: src/routes/en/docs/wtlex-doc/+page.adoc:29
msgid ""
"Cartes anki pour téléchargement Index de recherche hyperliens entres mots"
msgstr ""

#. type: Block title
#: src/routes/en/docs/wtlex-doc/+page.adoc:30
#, fuzzy, no-wrap
#| msgid "Application"
msgid "Abreviations"
msgstr "Demande"

#. type: Table
#: src/routes/en/docs/wtlex-doc/+page.adoc:34
#, no-wrap
msgid "2+a|\n"
msgstr ""

#. type: Positional ($1) AttributeList argument for macro 'image'
#: src/routes/en/docs/wtlex-doc/+page.adoc:34
#, no-wrap
msgid "Jula wordcloud"
msgstr ""

#. type: Hash Value: wtlex page-image
#: src/routes/en/docs/wtlex-doc/+page.adoc:34 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "w100.svg"
msgstr "w100.svg"

#. type: Table
#: src/routes/en/docs/wtlex-doc/+page.adoc:59
msgid ""
"|pd|parti du discours |pr|pronom |pr. réfl.| pronom réfléchi |s.|singlulier |"
"pl.|pluriel |adj.|adjectif |n.|nom |v.|verbe |vt|verbe transitif |vi|verbe "
"intransitif |conn. verb.|connectif verbal |postp.|postposition |mq. asp.|"
"marque d'aspect |mq. include.|marque de l'injonctif |conn synt. compl.|"
"connectif du systagme complétif |mq. en. adj.|marque d'énoncé adjectival |"
"par. emph.|particule d'emphase |mq. inact.|marque d'inactuel |conn. distrib.|"
"connectif du syntagme distributif |part. phr.|particule phrastique |pr. "
"emph.|pronom emphatique |cop.|copule |fig.|figurée |empl. idiom.|emploi "
"idiomatque"
msgstr ""

#. type: Title ==
#: src/routes/en/docs/wtlex-doc/+page.adoc:61
#, no-wrap
msgid "Jula Français"
msgstr ""
