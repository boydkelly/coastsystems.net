# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-08-17 17:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:1
#, no-wrap
msgid "\"Silverblue Tip - Run commands anywhere\""
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:12
msgid ""
"Do you find yourself regularly running utilities on your host system as well "
"as in your toolbox? Just put this in your ~/.bashrc:"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:19
#, no-wrap
msgid ""
"for bin in fzf nvim code ranger anki meld poedit;\n"
"do\n"
"type -P $bin >/dev/null 2>&1 || alias $bin=\"toolbox run $bin\" <1>\n"
"done\n"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2020-11-09-sb-bashrc.adoc:22
msgid "add [ -c toolbox-name ] to run in a non default toolbox"
msgstr ""
