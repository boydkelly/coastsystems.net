# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-02-25 12:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:1
#, no-wrap
msgid "Light up Neo4j and Neodash in a pod with butane and ignition"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:17
msgid ""
"Run an instance of Neo4j with neodash in a pod in a Fedora Coreos container "
"on Google Cloud."
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:18
#, no-wrap
msgid "Requirements"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:21
msgid "GCP account with Google cloud compute engine."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:22
msgid "Static IP for the machine in your VPC network."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:23
msgid "Gcloud cli on local machine."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:24
msgid "local neo4j instance including dashboard saved to database."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:25
msgid "ssh keypair to install (public key) on image."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:26
msgid "butane"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:27
msgid "matches"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:28
msgid "These instructions assume the user is on a linux box."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:30
msgid "No ssl is configured here."
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:31
#, no-wrap
msgid "Start on Google cloud web"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:34
msgid "Set up firewall rules for the neo4j browser and neodash"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:35
msgid "Under VPC Network/firewall Click 'Create firewall rule'"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:36
msgid "Provide name for example: allow-neo4j"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:37
msgid "Under Targets, select 'Specified Target Tags'"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:38
msgid "Provide a target tag of your liking for example: neo4j"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:39
msgid "Specify TCP ports: 7474, 7687, 5005"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:40
msgid "Set up static ip to assign to the machine at build time."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:41
msgid "Under VPC Network/IP addresses"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:42
msgid "Select Reserve External Static Addresses"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:43
msgid "Select zone and other options."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:45
msgid ""
"the IP will be attached to the machine you will create in a minute or two."
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:46
#, no-wrap
msgid "Continue on your machine"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:49
msgid "Install https://cloud.google.com/sdk/docs/install[gcloud cli]."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:50
msgid "Make sure you can run: `gcloud compute instances list`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:51
msgid "Install https://coreos.github.io/butane/getting-started/[butane]"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:52
msgid "Create the following neo4j.fcc file and adapt to your needs."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:53
msgid "Neo4j section"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:54
msgid ""
"At a bare minimum you will need to supply a public key for the core user."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:57
#, no-wrap
msgid ""
"You will not be able to log into the machine without it.\n"
"The private key is also practical (for the core and/or neo4j user) if you want to pull data from gitlab etc."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:59
msgid ""
"The http and bolt listen address should be the *internal* ip of your cloud "
"vm."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:60
msgid "Neodash section"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:61
msgid "The *external* static ip of your vm MUST be indicated."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:62
msgid "You MUST change the password for the database and the dashboard name."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:63
msgid "You MUST indicate the external IP address for the standaloneHost value."
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:149
#, no-wrap
msgid ""
"#filename: neo4j.fcc\n"
"variant: fcos\n"
"version: 1.4.0\n"
"passwd:\n"
"  users:\n"
"    - name: core\n"
"      ssh_authorized_keys:\n"
"        - ssh-ed25519 CCCBC3NzaC1lZDI1NTE5CCCBILfBrfr+iLNCXjQhcWKDFPLTOO5MPpPp6+GxmR8m5zxY keyname\n"
"    - name: neo4j\n"
"      ssh_authorized_keys:\n"
"        - ssh-ed25519 CCCBC3NzaC1lZDI1NTE5CCCBILfBrfr+iLNCXjQhcWKDFPLTOO5MPpPp6+GxmR8m5zxY keyname\n"
"      uid: 7474\n"
"storage:\n"
"  files:\n"
"    - path: /etc/hostname\n"
"      mode: 0644\n"
"      contents:\n"
"        inline: fcos-neo4j\n"
"    - path: /home/core/.ssh/id_ed25519\n"
"      mode: 0600\n"
"      user:\n"
"        name: core\n"
"      group:\n"
"        name: core\n"
"      contents:\n"
"        inline: |\n"
"          -----BEGIN OPENSSH PRIVATE KEY-----\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          OO5MPpPp6+GxmR8m5zxYCCCBBmJrZWxseQECAwQFBgc=\n"
"          -----END OPENSSH PRIVATE KEY-----\n"
"    - path: /home/core/.ssh/id_ed25519.pub\n"
"      mode: 0644\n"
"      user:\n"
"        name: core\n"
"      group:\n"
"        name: core\n"
"      contents:\n"
"        inline: |\n"
"          ssh-ed25519 CCCBC3NzaC1lZDI1NTE5CCCBILfBrfr+iLNCXjQhcWKDFPLTOO5MPpPp6+GxmR8m5zxY keyname\n"
"    - path: /home/neo4j/.ssh/id_ed25519\n"
"      mode: 0600\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
"      contents:\n"
"        inline: |\n"
"          -----BEGIN OPENSSH PRIVATE KEY-----\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          QyNTUxOQAAACC3wa36/oizQl40IXFigxTy0zjuTD6T6evhsZkfJuc8WCCCBJAPDFZXDwxW\n"
"          OO5MPpPp6+GxmR8m5zxYCCCBBmJrZWxseQECAwQFBgc=\n"
"          -----END OPENSSH PRIVATE KEY-----\n"
"    - path: /home/neo4j/.ssh/id_ed25519.pub\n"
"      mode: 0644\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
"      contents:\n"
"        inline: |\n"
"          ssh-ed25519 CCCBC3NzaC1lZDI1NTE5CCCBILfBrfr+iLNCXjQhcWKDFPLTOO5MPpPp6+GxmR8m5zxY keyname\n"
"    - path: /var/lib/systemd/linger/neo4j\n"
"      mode: 0644\n"
"    - path: /home/neo4j/get-go.sh\n"
"      mode: 0744\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
"      contents:\n"
"        inline: |\n"
"          #!/usr/bin/bash\n"
"          [ -d $HOME/neo4j/data/databases/neo4j/|| { echo \"Looks like there is no neo4j database.  Exiting...\"; exit 1; }\n"
"          podman pod create --name neo \\\n"
"              --userns=keep-id \\\n"
"              -p 7474:7474 \\\n"
"              -p 7687:7687 \\\n"
"              -p 5005:5005\n"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:162
#, no-wrap
msgid ""
"          podman run -d \\\n"
"              --pod neo \\\n"
"              --name neo4j \\\n"
"              --security-opt label=disable \\\n"
"              -v $HOME/neo4j/data:/var/lib/neo4j/data \\\n"
"              -v $HOME/neo4j/logs:/var/lib/neo4j/logs \\\n"
"              -v $HOME/neo4j/import:/var/lib/neo4j/import \\\n"
"              -v $HOME/neo4j/plugins:/var/lib/neo4j/plugins \\\n"
"              --env NEO4J_dbms_connector_bolt_enabled=true \\\n"
"              --env NEO4J_dbms_connector_http_advertised__address=10.240.0.17:7474 \\\n"
"              --env NEO4J_dbms_connector_bolt_advertised__address=10.240.0.17:7687 \\\n"
"              docker.io/library/neo4j:5.3.0-community\n"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:201
#, no-wrap
msgid ""
"          podman run -d \\\n"
"              --pod neo \\\n"
"              --name neodash \\\n"
"              --security-opt label=disable \\\n"
"              -e ssoEnabled=false \\\n"
"              -e standalone=true \\\n"
"              -e standaloneProtocol=\"bolt\" \\\n"
"              -e standaloneHost=\"IP_ADDRESS\" \\\n"
"              -e standalonePort=\"7687\" \\\n"
"              -e standaloneDatabase=\"neo4j\" \\\n"
"              -e standaloneUsername=\"neo4j\" \\\n"
"              -e standalonePassword=\"YOUR_PASSWORD_HERE\" \\\n"
"              -e standaloneDashboardName=\"REPLACE_THIS_WITH_YOUR_DASHBOARD_NAME\" \\\n"
"              -e standaloneDashboardDatabase=\"neo4j\" \\\n"
"              docker.io/nielsdejong/neodash\n"
"          pushd $HOME/.config/systemd/user || exit\n"
"          podman generate systemd -n -f neo\n"
"          popd\n"
"          systemctl --user enable pod-neo\n"
"  directories:\n"
"    - path: /home/neo4j/.config\n"
"      mode: 0755\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
"    - path: /home/neo4j/.config/systemd\n"
"      mode: 0755\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
"    - path: /home/neo4j/.config/systemd/user\n"
"      mode: 0755\n"
"      user:\n"
"        name: neo4j\n"
"      group:\n"
"        name: neo4j\n"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:204
msgid ""
"Edit the following file as ignite.sh and save in the same directory as neo4j."
"fcc above."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:205
msgid ""
"Pay particular attention to the TAGS; these MUST correspond to the tag "
"assigned to the firewall rule allowing access to the machine on TCP ports "
"7474 and 5005."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:206
msgid "The IP_ADDRESS is your static IP as configured in Google Cloud."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:207
msgid "Pick your ZONE and VM_NAME"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:219
#, no-wrap
msgid ""
"#!/bin/bash\n"
"#filename: ignite.sh\n"
"STREAM=stable\n"
"VM_NAME=fcos-neo4j\n"
"CONFIG=neo4j.ign\n"
"TAGS=neo4j\n"
"IP_ADDRESS=35.211.65.234\n"
"ZONE=us-east1-b\n"
"butane -p -r neo4j.fcc > neo4j.ign\n"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:229
#, no-wrap
msgid ""
"gcloud compute instances create --metadata-from-file \"user-data=${CONFIG}\" \\\n"
"  --image-project \"fedora-coreos-cloud\" \\\n"
"  --image-family \"fedora-coreos-${STREAM}\" \"${VM_NAME}\" \\\n"
"  --zone ${ZONE} \\\n"
"  --tags ${TAGS} \\\n"
"  --address ${IP_ADDRESS} \\\n"
"  --no-service-account \\\n"
"  --no-scopes \\\n"
"  --preemptible\n"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:232
msgid ""
"preemptible allows Google to shut down your vm anytime it sees fit. Remove "
"this or create a cron job to restart it when Google shuts it off."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:234
msgid "Install butane (`dnf install butane`)"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:235
msgid "Light it up! `./ignite.sh`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:237
msgid ""
"If all goes to plan and you had the corresponding private key on your "
"computer, you should be able to ssh into your machine."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:239
msgid ""
"Its now up to you to get your neo4j database (including the saved neodash "
"dashboard) on the box."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:241
msgid "stop the database"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:242
msgid "`rsync -avupzr /var/lib/neo4j neo4j@IP_ADDRESS:~/` (or scp)"
msgstr ""

#. type: Title ==
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:243
#, no-wrap
msgid "On your new vm"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:246
msgid "Pull in the container images"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:247
msgid "`podman pull docker.io/library/neo4j:5.3.0-community`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:248
msgid "`podman pull docker.io/nielsdejong/neodash`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:250
msgid "Let's do this:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:251
msgid "`./get-go.sh`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:253
msgid "This will set up and start both neo4j and neodash."
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:255
msgid "Check running state with `podman pod ps`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:257
msgid "You can start and stop with:"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:259
msgid "`systemctl --user status|start|stop pod-neo`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:261
msgid "`podman pod start|stop neo`"
msgstr ""

#. type: Plain text
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:263
msgid "Connect to the database and Neodash:"
msgstr ""

#. type: delimited block -
#: src/routes/en/blog/2022-11-15-neo-pod.adoc:268
#, no-wrap
msgid ""
"`http://IP_ADDRESS:7474` (neo4j browser) +\n"
"`http://IP_ADDRESS:5005` (neodash)\n"
msgstr ""
