# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-08-17 17:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "saga"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "a"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sheep"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hat"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "baara"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɑ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "aa"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "work"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dark (longer a)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sanji"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɑ̃"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "an"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rain"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "like lawn"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "baga"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "b"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "porridge"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "big"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "cɛ"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "tʃ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "c"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "man"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "match"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "disi"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "e"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "d"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "chest"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dot"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bese"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "machete"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "make"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "feere"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "e:"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ee"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sell"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out e)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sen"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sang [Approximate]"
msgstr ""

#. type: Hash Value: locale
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "en"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "foot"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛ̃"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kɛnɛya"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "health"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "met"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mɛɛn"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛɛ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "last"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out short e)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bɛn"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɛn"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "agree"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(nasalized short e)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "foro"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "f"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "field"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "fox"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mɔgɔ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "g"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "person"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "get"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gbɛlɛ or gwɛlɛ"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɡ͡b or ɡʷ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gw or gb"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hard"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "gb or gw"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hakili"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "h"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "intelligence"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hello"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "misi"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "i"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "cow"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "free"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "miiri"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ii"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "thought"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(lengthened i)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "min"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ĩ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "in"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "drink"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nasilized i like seen [Approximate]"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ji"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɟ or dʒ"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "j"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "water"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "Abidjan"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kitabu"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "k"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "livre"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kiosque"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "lolo"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "l"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "star"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "late"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "maro"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "m"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rice"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mat"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nɔnɔ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "n"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "milk"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "note"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɲɔ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɲ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "mil"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "oignon"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ŋɔni"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ŋ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "thorn"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ten glasses (ng) [Approximate]"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bolo"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "o"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "hand"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "no"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "looru"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "oo"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "five"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out o)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bon"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "õ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "on"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "house"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "nom [Approximate]"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sɔrɔ"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "find"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "not"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "fɔɔnɔ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔɔ"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vomit"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(drawn out ɔ)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "dɔn"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ɔn"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "danse"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "like 'ahn' [Approximate]"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pan"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "p"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "jump"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pen"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "bɔrɔ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "r"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sac"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pero [Spanish]"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sara"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "s"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "pay"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "sauce"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "toto"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "t"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "rat"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "table"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kura"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "u"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "new"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "boot"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "suuri"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "uu"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "lower"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(lenthened u)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "kunkolo"
msgstr ""

#. type: Hash Value: ipa
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "ũ"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "un"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "head"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "(nasal u)"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vote [borrowed from French]"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "v"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vote"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "vol"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "wari"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "w"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "money"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "watch"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "yan"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "y"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "here"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "yes"
msgstr ""

#. type: Hash Value: example
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "zɔnzɔn"
msgstr ""

#. type: Hash Value: letter
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "z"
msgstr ""

#. type: Hash Value: meaning
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "shrimp"
msgstr ""

#. type: Hash Value: pronounce
#: src/routes/en/docs/alpha-dyu/alpha-dyu.yaml:1
#, no-wrap
msgid "zoo"
msgstr ""
