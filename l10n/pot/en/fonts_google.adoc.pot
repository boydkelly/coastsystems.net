# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-03-22 14:06-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Target for macro image
#: src/routes/en/blog/2020-10-21-fonts.adoc:13
#: src/_include/en/resources/fonts_google.adoc:9
#, no-wrap
msgid "polices.webp"
msgstr ""

#. type: Title =
#: src/_include/en/resources/fonts_google.adoc:1
#, no-wrap
msgid "Google web fonts for manding languages "
msgstr ""

#. type: Positional ($1) AttributeList argument for macro 'image'
#: src/_include/en/resources/fonts_google.adoc:9
#, no-wrap
msgid "polices web"
msgstr ""

#. type: Plain text
#: src/_include/en/resources/fonts_google.adoc:13
msgid ""
"If your are an African web developer go to link:https://fonts.google.com/?"
"preview."
"text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview."
"text_type=custom[Google Fonts] ` to check if your favorite web font contains "
"the appropriate unicode characters to display your language."
msgstr ""
