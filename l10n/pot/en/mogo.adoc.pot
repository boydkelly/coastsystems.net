# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-08-17 17:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/blog/mogo.adoc:1
#, no-wrap
msgid "Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye"
msgstr ""

#. type: Table
#: src/routes/en/blog/mogo.adoc:16
#, no-wrap
msgid ""
"| Mɔgɔ be yen mɔgɔ kɛ mɔgɔ ye.\n"
"| A man makes a man.\n"
msgstr ""
