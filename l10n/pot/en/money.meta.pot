# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-07-02 13:34+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Hash Value: wtlex author
#: src/routes/en/docs/animals/meta.yaml:1 src/routes/en/docs/money/meta.yaml:1
#: src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "Boyd Kelly"
msgstr ""

#. type: Hash Value: category
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "Julakan"
msgstr ""

#. type: Hash Value: description
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "Lean to count and use the West African CFA in the dioula language"
msgstr ""

#. type: Hash Value: keywords
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "dioula, dyu, jula, money, counting, FCFA"
msgstr ""

#. type: Hash Value: alphabet page-image
#: src/routes/en/docs/money/meta.yaml:1 src/lib/i18n/locales/en.yaml:1
#, no-wrap
msgid "cfa.webp"
msgstr ""

#. type: Hash Value: pageRevdate
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "2020-05-07"
msgstr ""

#. type: Hash Value: tags
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "money, counting"
msgstr ""

#. type: Hash Value: title
#: src/routes/en/docs/money/meta.yaml:1
#, no-wrap
msgid "Money and Counting in Dyula"
msgstr ""
