# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2024-08-19 13:42+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/_include/en/resources/rfi.adoc:1
#, no-wrap
msgid "Radio France International Mandenkan"
msgstr ""

#. type: Positional ($1) AttributeList argument for macro 'image'
#: src/_include/en/resources/rfi.adoc:9
#, no-wrap
msgid "RFI"
msgstr ""

#. type: Target for macro image
#: src/_include/en/resources/rfi.adoc:9
#, no-wrap
msgid "rfi_mandenkan.webp"
msgstr ""

#. type: Table
#: src/_include/en/resources/rfi.adoc:17
#, no-wrap
msgid ""
"2+|Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.\n"
"Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.\n"
"10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.\n"
"|Web site: link:https://rfi.fr/ma[rfi]|\n"
msgstr ""
