# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd Kelly
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2025-02-24 15:44+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#: src/routes/en/docs/w_jl/+page.adoc:1
#, no-wrap
msgid "Morphology of the Jula language in the Watchtower"
msgstr ""

#. type: Plain text
#: src/routes/en/docs/w_jl/+page.adoc:13
msgid "image:{page-image}[Alt text,300]"
msgstr ""

#. type: Block title
#: src/routes/en/docs/w_jl/+page.adoc:15
#, no-wrap
msgid "See also:"
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:18
msgid ""
"link:/en/docs/w_jl_w100/[Compiliation of the 100 most common Jula words in "
"the Watchtower]"
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:19
msgid ""
"link:/en/docs/kodi-w100/[The 100 most common words in Ivory Coast Jula "
"(Kodi)]"
msgstr ""

#. type: Block title
#: src/routes/en/docs/w_jl/+page.adoc:22
#, no-wrap
msgid "These figures may change for several reasons:"
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:25
msgid "New editions are added to the inventory of words"
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:26
msgid ""
"Errors may occur with French words that must be extracted from the Jula text."
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:27
msgid "Proper names in French and Jula present difficulies."
msgstr ""

#. type: delimited block =
#: src/routes/en/docs/w_jl/+page.adoc:28
msgid "The consideration of the plural may change"
msgstr ""
