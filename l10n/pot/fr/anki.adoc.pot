# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2023-07-16 14:19+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/fr/modules/ROOT/pages/2018-05-18-anki.adoc:4
#: content/fr/modules/ROOT/pages/2018-07-30-alpha-jula.adoc:5
#: content/fr/modules/ROOT/pages/ngalonci.adoc:6
#: content/fr/modules/ROOT/pages/2021-07-14-anki.adoc:3
#: docs/fr/modules/ROOT/pages/anki.adoc:5
#, no-wrap
msgid "fr"
msgstr ""

#. type: Title =
#: docs/fr/modules/ROOT/pages/anki.adoc:1
#, no-wrap
msgid "Qu'est-ce que Anki  ?"
msgstr ""

#. type: Attribute :keywords:
#: docs/fr/modules/ROOT/pages/anki.adoc:4
#, no-wrap
msgid "anki, dioula, apprendre"
msgstr ""

#. type: Title ==
#: docs/fr/modules/ROOT/pages/anki.adoc:9
#, no-wrap
msgid "Qu'est-ce que c'est https://ankiweb.net[Anki]?"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:12
msgid ""
"C'est une application qui facilite la mémorisation. Utilisé par des "
"étudiants universitaires dans le monde entier pour leurs études en médecine, "
"géographie, et langues, c'est un outil efficace, gratuit, et sans "
"publicité.  N'ayez pas peur! C'est facile à utiliser et disponible sous "
"Windows, Android, Apple, et Linux. Cela pourrait vous aider à mémoriser le "
"vocabulaire dioula."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:14
msgid ""
"Anki n'est qu'un des outils pratiques (parmi plusieurs) pour faciliter "
"l'apprentissage d'une langue. Mais il faut bien sûr plus que mémoriser des "
"mots pour devenir confortable dans une autre langue. Mandenkan.com est là "
"pour vous soutenir dans l'aventure..."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:16
msgid "Pour installer Anki avec le vocabulaire kodi:"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:18
msgid ""
"Allez d'abord sur https://ankiweb.net pour télécharger ou installer "
"l'application sur votre appareil Windows ou Mac."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:19
msgid ""
"Sur Android, allez sur le Google Play store, et chercher 'Anki'. https://"
"play.google.com/store/apps/details?id=com.ichi2.anki&hl=en&gl=US[Installez "
"l'application 'Ankidroid']."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:20
msgid ""
"Cliquez sur xref:docs:kodi:attachment$kodi.apkg[télécharger fichier de "
"vocabulaire Kódì]; Cliquez sur le fichier et accepter de l'importer."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:21
msgid ""
"Alternativement, vous pouvez aller dans l'application Anki, et choisir "
"'Importer...' à partir des menus, pour naviguer et sélectionner le fichier."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:23
msgid "Pour utiliser l'application:"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:25
msgid "Ouvrez l'application. Cliquez sur le paquet de questions 'Kódì…'."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:27
msgid "On vous propose un mot soit en dioula ou en français au hasard."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:28
msgid ""
"Soyez honnête avec vous même en indiquant en bas de l'écran la facilité de "
"votre réponse."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:30
msgid ""
"Vous pouvez cliquer sur ENCORE (réponse non rappelée), DIFFICILE, CORRECT, "
"ou bien FAÇILE."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:37
msgid ""
"Avec l'usage quotidien vous allez percevoir la magie de Anki, et la raison "
"pour laquelle que tant d'étudiants et professionnels l'utilisent.  Anki va "
"'apprendre' combien de temps cela vous prend pour mémoriser.  Et quels mots "
"présentent plus de difficulté.  Donc le rythme d'apprentissage serait "
"différent pour chacun.  Anki va ensuite ajuster la fréquence de rappels de "
"mots pour vous aider à ne pas les oublier.  Et Anki va retirer des mots "
"appris et les remplacer par de nouveaux mots à apprendre en l'utilisant."
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:39
msgid "Pour d'autres informations plus en détails voir:"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:41
msgid "https://www.ankiweb.net[Le siite web Anki]"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:43
msgid "https://fr.wikipedia.org/wiki/Anki[Anki sur Wikipedia]"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:45
msgid ""
"Le document: https://cours-de-japonais.com/anki/[Anki: Le logiciel Miracle]"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:47
msgid "Tutoriel Anki détaillé en français:"
msgstr ""

#. type: Plain text
#: docs/fr/modules/ROOT/pages/anki.adoc:51
msgid "Explication de la théorie de la courbe de l'oublie (pour les dures !):"
msgstr ""
