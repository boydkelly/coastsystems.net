#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

magick $1 -crop 1536x512+0+0 output_top.jpg
magick $1 -crop 1536x512+0+1024 output_bottom.jpg
magick $1 -crop 1536x512+0+256 output_middle.jpg
