#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

#[[ -z $1 ]] && exit
#image=$1

pushd static/images || exit

for image in alphabet.svg fontlist.svg cfa.webp animals.webp; do
  echo "$image"
  magick "$image" -resize 1200x675 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".x.jpg
  magick "$image" -resize 1200x630 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".og.png
done
