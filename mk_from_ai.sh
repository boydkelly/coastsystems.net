#!/usr/bin/bash
set -o errexit
set -o pipefail

[[ -z $1 ]] && {
  echo "need and image"
  exit
}
image=$(basename "$1")

set -o nounset

pushd static/images || exit

echo "$image"

case "${image##*.}" in
jpg | jpeg)
  echo "This is a JPEG image."
  ;;
png)
  echo "This is a PNG image."
  magick "$image" -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".webp
  magick "$image" -resize 1200x675 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".x.jpg
  magick "$image" -resize 1200x630 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".og.png
  ;;
svg)
  echo "This is an SVG image."
  ;;
webp)
  echo "This is a WEBP image."
  magick "$image" -resize 1200x675 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".x.jpg
  magick "$image" -resize 1200x630 -quality 75 -sampling-factor 4:2:0 -strip "${image%.*}".og.png
  ;;
*)
  echo "Unknown image format."
  ;;
esac
