import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';

if (process.argv.length !== 3) {
  console.error('Usage: node modify-dom.js path/to/filename.html');
  process.exit(1);
}
const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const partialHtml = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(partialHtml, { decodeEntities: false }, false);

$('table').each((_, table) => {
  const $table = $(table);
  if ($table.find('tbody').length === 0) {
    const tbody = $('<tbody></tbody>');
    $table.children('tr').each((_, tr) => {
      tbody.append(tr);
    });
    $table.append(tbody);
  }
});

$('iframe:not([title])').attr('title', 'unknown');

// Manually adjust the casing of <Tooltip> tags to ensure they are preserved
const cleanedHtml = $.html()
  .replace(/<tooltip>/g, '<Tooltip>')
  .replace(/<\/tooltip>/g, '</Tooltip>')
  .replace(/<tile /g, '<Tile ')
  .replace(/<\/tile>/g, '</Tile>');

// const cleanedHtml = '<script></script>\n' + modifiedHtml;

// Save the modified HTML to the original file
// const modifiedHtml = $.html();
fs.writeFileSync(absoluteFilePath, cleanedHtml);
