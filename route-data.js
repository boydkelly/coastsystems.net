import fs from 'fs';
import path from 'path';
import { processAsciiDoc } from './src/lib/js/asciidoc.js'; // Adjust path if necessary

const BASE_PATHS = {
  fr: 'src/routes/fr',
  en: 'src/routes/en',
  bm: 'src/routes/bm',
  dyu: 'src/routes/dyu'
};

const OUTPUT_DIR = 'src/lib';

// Extract metadata from meta.json if present
const extractMetadataFromMetaFile = (dirPath) => {
  const metaFile = path.join(dirPath, 'meta.json');
  if (fs.existsSync(metaFile)) {
    try {
      const metaData = JSON.parse(fs.readFileSync(metaFile, 'utf8'));
      return {
        category: metaData?.category || null,
        description: metaData?.description || null,
        title: metaData?.title || null,
        author: metaData?.author || null,
        keywords: metaData?.keywords || null,
        orgname: metaData?.orgname || null
      };
    } catch (error) {
      console.error(`Error reading meta.json in ${dirPath}:`, error);
    }
  }
  return null;
};

// Extract metadata from AsciiDoc files
const extractMetadataFromAdoc = (filePath) => {
  try {
    const adocContent = fs.readFileSync(filePath, 'utf8');
    const { metadata } = processAsciiDoc(adocContent);
    return {
      category: metadata?.category || null,
      description: metadata?.description || null
    };
  } catch (error) {
    console.error(`Error processing AsciiDoc file: ${filePath}`, error);
    return { category: null, description: null };
  }
};

// Recursively scan directories and collect metadata
const scanDirectory = (dir, lang, section, routePrefix = '') => {
  const entries = fs.readdirSync(dir, { withFileTypes: true });
  const routes = [];

  const metaData = extractMetadataFromMetaFile(dir);
  const route = `/${lang}/${section}${routePrefix}`;

  if (metaData) {
    if (!metaData.category) {
      console.warn(`Warning: Missing category in meta.json at ${dir}`);
    }
    routes.push({ route, section, ...metaData });
  }

  for (const entry of entries) {
    const fullPath = path.join(dir, entry.name);

    if (entry.isDirectory()) {
      routes.push(...scanDirectory(fullPath, lang, section, `${routePrefix}/${entry.name}`));
    } else if (entry.name.endsWith('.adoc')) {
      const { category, description } = extractMetadataFromAdoc(fullPath);
      if (!category) {
        console.warn(`Warning: Missing category in AsciiDoc file ${fullPath}`);
      }
      routes.push({
        route: `${route}/${entry.name.replace(/\.adoc$/, '')}`,
        section,
        category: category || metaData?.category,
        description: description || metaData?.description
      });
    }
  }

  return routes;
};

// Generate index files
const generateIndexFiles = () => {
  for (const lang of Object.keys(BASE_PATHS)) {
    for (const section of ['docs', 'blog']) {
      const dirPath = path.join(BASE_PATHS[lang], section);
      if (fs.existsSync(dirPath)) {
        const data = scanDirectory(dirPath, lang, section);
        const outputFile = path.join(OUTPUT_DIR, `${lang}-${section}-data.json`);
        fs.writeFileSync(outputFile, JSON.stringify(data, null, 2));
        console.log(`Generated: ${outputFile}`);
      }
    }
  }
};

// Run script
generateIndexFiles();
