set -o errexit
set -o nounset
set -o pipefail
#yq -pj -oj '.[] |= (.id = .commit)' ./lazy-lock.json >plugins.json
#yq -pj -oj '[.] | .[] |= (.id = .commit)' ./lazy-lock.json >plugins.json

#yq -pj -oj '.[] |= (.id = .commit) | [.]' ./lazy-lock.json >plugins.json

input="$HOME/.config/nvim/lazy-lock.json"
output=$HOME/dev/svelte/coastsystems.net/src/assets/metadata/plugins.json
~/.local/bin/yq -pj -oj '[to_entries[] | {"name": .key, "id": .value.commit} + .value]' "$input" >"$output"
git add "$output" && git commit -m "update plugins"
