export const prerender = true;
export const trailingSlash = 'always'; // needed for gitlab
import '$lib/i18n'; // Import to initialize. Important :)
import { waitLocale } from 'svelte-i18n';

export const load = async () => {
  await waitLocale();
};

// To get the current locale value
//let currentLocale;
// locale.subscribe(value => {
//    currentLocale = value;
//  })();
