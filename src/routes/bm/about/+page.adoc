= Qui suis-je ?
:author: Boyd Kelly
:date: 2018-05-21
:description: À propos de Coast Busines Technologies 
:lang: bm 
include::locale/attributes.adoc[]

En sommaire, j'ai fait du bénévolat extensif en afrique, tout en travaillant avec la technologie. J'ai supervisé departement de support technique pour le Bureau des Passports du Canada à l'ouest et ensuite travaillé dans le departement informatique d'une grande société de logiciel.

Au cours de route j'ai hébergé des sites web, géré les serveurs mail et DNS et enseigné les cours de Microsoft Exchange à l'Université de la columbie-britanique. Avec beaucoup d’expérience dans les technologies virtuelles sur Linux, Amazon et GCE, je peux travailler à distance avec pleine confiance.

Ce site web static est hébérgé sur Gitlab, créée par moi avec http://www.asciidoctor.org[Asciidoctor], https://svelte.dev[Sveltekit], et http://neovim.io[neovim] . 

++++
<iframe title="about" src="https://docs.google.com/forms/d/e/1FAIpQLSdw6yhla0-mmVrAWeLcHM2lBKHvKZre4uiiiGCjvaG30x22Qg/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0"></iframe>
++++
