= Type like a boss in Jula or Baoulé in Android!
:author: Boyd Kelly
:lang: en
:page-revdate: 2020-04-23
:description: How to install and use the Baoulé or Jula keyboard in Andoid
:page-tags: jula, tech
:tags: jula, tech
:keywords: jula, dioula, dyula, technology, android, baoulé
:category: Technology
include::{includedir}locale/attributes.adoc[]

image::clavier_android.webp[]

== « I ni sɔgɔma! Koow be di?  Bara ka ɲi wa? »

=== Can you type that on your phone keyboard?  Of course! 

=== A ka nɔgɔ!

The Google Gboard keyboard is available in more than 500 languages! 
While we are still waiting for it thought in Senoufo, Guere and Yacouba , at this time its available in Baouké and Jula. 
Since these keyboards are system utilities, they are available in all applications. 
So if you want to send an email, post a message on a social network, or write a letter, you can do it!

[NOTE]
====
You must have the GBoard keyboard installed on your device. 
Note however that Samsung uses its own (less powerful) keyboard by default. You may have to go to the Google Play Store, search for 'gboard' and install this on your device. 
Then you will need to activate this as the default keyboard.
====

=== If you already use Gboard, or if you have just installed it, now its a matter of activating the language input you wish.

[cols="2"]
|===
a|image::Screenshot_20200424-191237.webp[keyboard,400,,float="left",align="center"]
a|
* First go to Settings, System, Languages and inputs.
|===

[cols="2"]
|===
a|
* Notice the section *Keyboard and input modes*
* Select the option *Virtual Keyboard*
* Then select *Language*
  image::Screenshot_20200424-191500.webp[keyboard,400]
|===

[cols="2"]
|===
a|image::Screenshot_20200424-191614.webp[keyboard,400]
a|* Select *Add a keyboard*

 * *Find *Dioula* or *Baoulé*
|===

. From these options, you can choose a keyboard configuration, azerty, qwerty or with Jula, there is even an Nko option!

. If you do a long press on the space bar you will find the option to switch keyboards.

[TIP]
====
You can type in English with no issues with the Julakan or Baoulé keyboards. However the spell check is tied to the keyboard. So you will undoubtedly find advantageous to switch keyboards according to the language in which you are typing. One long press on the kbd::space and its done.
====

== Ayiwa.  An ka taa!
