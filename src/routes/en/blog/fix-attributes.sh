#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail


sed -i 's/include:.*attrib.*/include::locale\/attributes.adoc[]/' *.adoc
