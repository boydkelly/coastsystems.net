= Boyd Kelly on Linkedin
:date: 2018-05-05
:category: Technology
:lang: en
:link: https://www.linkedin.com/in/boydkelly
include::{includedir}locale/attributes.adoc[]

== Contact me via linkedin or the contact form below:

https://www.linkedin.com/in/boydkelly
