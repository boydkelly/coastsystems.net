import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core'
const processor = Processor()

export const load = (async () => {
  const file = await import(`./+page.adoc?raw` /* @vite-ignore */ );
  if (file.default) {
    const attributes = processor.load(file.default).getAttributes()
    return {
      metadata: attributes
    }
  }
  error(404, 'Not found');
});
