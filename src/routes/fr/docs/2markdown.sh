#!/usr/bin/bash
[[ -d $1 ]] || {
  echo "$1 Source directory not valid"
  exit 1
}

# export work_dir=/tmp/2markdown
# [[ -d "$work_dir" ]] && {
#   rm -fr "$work_dir"
# }
# mkdir "$work_dir"

export source_dir="$1"
export target_dir="$HOME/notes/obsidian/dioula/cours"
[[ -d $target_dir ]] || mkdir -p "$target_dir"

pushd "$source_dir" || exit
find . -type f -name "*.adoc" -exec sh -c 'mkdir -p "$target_dir/$(dirname "$1")"; downdoc -o "$target_dir/${1%.adoc}.md" "$1"' sh {} \;
popd || exit

#cp -pr "$work_dir"/* "$target_dir"
#convert xrefs to markdown links
find "$target_dir" -type f -name "*.md" -exec sed -i -r 's/xref::(.*)\.adoc(\[.*\])/\2\(\1\.md\)/' {} \;
find "$target_dir" -type d -name "locale" -exec rm -rf {} +
find "$target_dir" -type f -name "footer.md" -delete
#remove unconverted tables markup
find "$target_dir" -type f -name "*.md" -exec sed -i '/|====$/d; /a|$/d' {} \;
