import { error } from '@sveltejs/kit';
import { processAsciiDoc } from '$lib/js/asciidoc.js';

export const load = (async () => {
  const adocFile = await import(`./+page.adoc?raw` /* @vite-ignore */ );
  if (adocFile.default) {
    const { metadata: attributes } = processAsciiDoc(adocFile.default);
    return {
      metadata: attributes
    }
  }
  error(404, 'Not found');
});
