= Controle de connaissances
:author: Clément N'Goran
:date: 2025-02-28
:keywords: Baoulé, Language, Leçons
:tags: Julakan, Language, Leçons
:category: Baoulé
:description: SÉANCE 01
:page-author: Clément N'Goran
:page-image: blog
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

[abstract]
====
LIKE KLELƐ CƐN : 01/03/2025 +
LIKE SUAN FUƐ : Kelly BOYD & Mme YEDE +
LIKE KLEFUƐ: Clément N’GORAN +
====

Complète la phrase ou le groupe de mots avec le mot correct en Baoulé.
Choisis dans la banque de mots :

wan:: qui?
yi:: épouse?
nniɛ:: mien
nyɛ:: combien
liɛ:: possesif
wun:: pronom réfléchi  (corps, se)
isɔ:: Cela Ceux
yalɛ:: souffrances
sɔ:: Ceux
fa:: Donner

. Fa …man mi::
Fa nye man mi:::
Donner le mien ma part

. E …yɛlɛ klɔ::
E liɛ yɛlɛ klɔ:::
C'est notre village (Nous êtes village)

. Nga mun be ti nyɛ… ?::
Nga mun be ti nyɛ nyɛ ?:::
Ceux la sont combien?

. Min… liɛ ti Agba, be flɛ i Bleja.::
Min… liɛ ti Agba, be flɛ i Bleja.:::

. …liɛ yo n fɛ.::
isɔ liɛ yo n fɛ.:::
cela me fait plaisir

. Be …fuɛ mun be klun ti ufue.::
Be sò fuɛ mun be klun ti ufue.:::
Ce genre de personnes la sont gentille.

. Amun kun amun… ni nzan.::
Amun kun amun wun ni nzan.:::
Vous allez vous tuer avec l'alool.

. Kofi ni i nianman bla be… be wun.::
Kofi ni i nianman bla be… be wun.:::
Kofi et sa soeur se ressemble.

. Be di’a …gbekle sɛ bo.::
Be di’a yalɛ gbekle sɛ bo.:::

. Be… Kely o suan wawle.::
Be wan Kely o suan wawle.:::
On dit kely apprends le baoule

Traduire chaque phrase en français.
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________
. ____________________

ex.

I sɔ'n ti::
A cause de cela

Yao i abla'n ti ɔ wu1i' ɔ::
Yao est mort à cause de ses commérages

ti:: -à cause de

Nzu ti ɔ ?::
pourquoi ?:::
C'est à cause de quoi ?

Lele::
jusqu'à

Siɛnniɛ alie'n k lele Ouagadougou::
Maintenant le train va jusqu'à Ouagadougou
                                                                                    1
