#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

export sectnumlevels=2
export toc="true"
export includedir="include"

[[ -z "$1" ]] && {
  echo "need a file"
  exit 1
}
sed -n '/^=\s/q1' "$1" && {
  echo "this looks like it doesn't have a title"
  exit 1
}
export file=$1
echo processing "$file"
#delete blank lines
#sed -i '10,19{/^[[:space:]]*$/d}' "$1"

export title=$(sed -n 's/^=\ //p' "$1")

export author=$(awk -F': ' '/^:author:/{sub(/^:author: */, "", $0); print $0}' "$1")
export category=$(awk -F': ' '/^:category:/{sub(/^:category: */, "", $0); print $0}' "$1")
export tags=$(awk -F': ' '/^:tags:/{sub(/^:tags: */, "", $0); print $0}' "$1")
export keywords=$(awk -F': ' '/^:keywords:/{sub(/^:keywords: */, "", $0); print $0}' "$1")

[[ -z $author ]] && author="Clément N'Goran"
export date=$(awk '/^:date:/{print $2}' "$1" | uniq)
[[ -z "$date" ]] && date=$(date -Im)
export lang="fr"

[[ -z $category ]] && category="Baoulé"
[[ -z $tags ]] && tags="Julakan, Language, Leçons"
[[ -z $keywords ]] && keywords="Baoulé, Language, Leçons"

#start the asciidoctor attributes
sed -i '/^:author:.*/d' "$1" && sed -i "/^=\s/a :author: $author" "$1"
#[[ ! $(awk '/^:date:/{print $2}' "$1") ]] && sed -i "/^:author:/a :date: $date" "$1"
sleep 0.25
sed -i '/^:date:.*/d' "$1" && sed -i "/^:author:/a :date: $date" "$1"
sleep 0.25
sed -i '/^:keywords:.*/d' "$1" && sed -i "/^:date:/a :keywords: $keywords" "$1"
sleep 0.25
sed -i '/^:category:.*/d' "$1" && sed -i "/^:keywords:/a :category: $category" "$1"
sleep 0.25
sed -i '/^:tags:.*/d' "$1" && sed -i "/^:category:/a :tags: $tags" "$1"
sleep 0.25
sed -i '/^:sectnums:.*/d' "$1" && sed -i '/^:experimental:/a :sectnums:' "$1"
sleep 0.25
sed -i '/^:sectnumlevels:.*/d' "$1" && sed -i "/^:sectnums:/a :sectnumlevels: $sectnumlevels" "$1"

if [ ! $lang = "en" ]; then
  sed -n '/^:lang:\s/q1' "$1" && sed -i "/^:sectnumlevels:/a :lang: $lang" "$1"
  sed -i '/^:page-lang/d' "$1" && sed -i '/^:lang:/a :page-lang: {lang}' "$1"
  sleep 0.25
  #  sed -i '/:includedir:.*/d' "$1" && sed -i "/^:lang:/i :includedir: content/$lang/$includedir" "$1"
  sed -i '/:includedir:.*/d' "$1"
  sleep 0.25
  sed -i '/include::locale/d' "$1" && sed -i '/^:page-lang:/a include::{includedir}locale/attributes.adoc[]' "$1"
  sleep 0.25
  sed -i "/^include::locale/a #;" "$1" && sed -i 's/^#;//g' "$1"
fi

#sed -i "/^\=\ .*/a #;" "$1"
#sed -i '18 a #;' "$1" && sed -i 's/^#;//g' "$1"
#

#!/usr/bin/bash
#[[ -z $1 ]] && { echo "need a file" ; exit 1 ; }
#sed -n '/^---\s/q0' $1 || { echo "this looks like it already has a header" ; exit 1 ; }
#this returns 0 when its NOT found.
#sed -r -n '/^---(\s|$)/q1' "$1" || { echo " This looks like it already has a header" ; exit 1 ;  }

#git commit --quiet $1 -m "$1"
