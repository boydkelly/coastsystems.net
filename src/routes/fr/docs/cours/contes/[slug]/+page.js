import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core';
const processor = Processor();
export const load = (async ({ params }) => {
  const filename = await import(`../${params.slug}.adoc?raw`);

  if (filename.default) {
    const attributes = processor.load(filename.default).getAttributes();
    const lang = attributes.lang
    // console.log ('From blog/[slug]/page.js: ', lang +" " + params.slug)
    const html = processor.convert(filename.default, {
      standalone: false,
      'safe': 'unsafe',
      'attributes': {
        'showtitle': true,
        includedir: lang ? `_include/${lang}/` : '_include/',
        'imagesdir': '/images/',
        'allow-uri-read': '',
        'skip-front-matter': true,
        icons: 'font'
      },
    });

    return {
      content: html,
      metadata: attributes,
    }
  }
  error(404, 'Not found');
});

