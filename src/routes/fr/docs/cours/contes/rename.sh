#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
#!/bin/bash

# Loop through all the files with the pattern filename.xx.adoc
for file in *.??.adoc; do
  # Extract the part before the first dot (filename)
  base="${file%%.*}"
  # Extract the part after the last dot (adoc)
  ext="${file##*.}"

  # Rename the file by removing the middle section
  mv "$file" "${base}.${ext}"
done
