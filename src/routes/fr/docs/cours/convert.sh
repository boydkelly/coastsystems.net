#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
downdoc "$1" -o - | pandoc -f markdown -t gfm -o "${1%%.adoc}.md"
./header.sh "$1"
