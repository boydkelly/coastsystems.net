import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core'
const processor = Processor()

export const load = (async () => {
  const adocFile = await import(`./+page.adoc?raw` /* @vite-ignore */ );
  if (adocFile.default) {
    const attributes = processor.load(adocFile.default).getAttributes()
    return {
      metadata: attributes
    }
  }
  error(404, 'Not found');
});
