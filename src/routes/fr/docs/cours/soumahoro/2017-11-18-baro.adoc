= Baro
:author: Soumahoro
:date: 2017-11-18
:keywords: Julakan, Language, Leçons
:category: julakan
:tags: Julakan, Language, Leçons
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]
include::{includedir}locale/attributes.adoc[]

== Exercise

Kuman nu yɛlɛman julakan na.

[horizontal]
Karim:: I ni tere Bakary. Hakɛ to, kibaro juman bɛ i y so kɔnɔ?
Bakary:: Hɛrɛ dɔrɔn bɛ so kɔnɔn. Somɔgɔ bɛ ka kɛnɛ.
Karim:: I facɛ do?
Bakary:: A ka kɛnɛ. Nka, a tagara Abidjan kunu.
Karim:: Ala ɲuman segi.
Bakary:: Amina. Ala ye tere nɔgɔya.
Karim:: I dɔgɔmuso tɔgɔ ye di?
Bakary:: A tɔgɔ ye minata. A tara lakoriso la.
Karim:: Minata ye den ɲuman ye. A bɛ mɔgɔw bonya. A bɛ nan kɛ muso ɲuman ye.
Bakary: Ala o kɛ!
