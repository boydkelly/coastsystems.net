= SÉANCE 2021-04-04
:author: Youssouf DIARRASSOUBA
:date: 2021-04-04
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== Texte : Jago

Fɔlɔfɔlɔ, jago tun ye julaw dɔrɔn ta ye, nka sisan bɛɛ kɛla jagokɛla ye.
Jago ye feerefenw feere ye.
i b’i ya warikun ta ka feerefenw san jango i bɛ tɔnɔ sɔrɔ.
Jago ye sɔrɔ ani fɔn ye.
N’i ka minan feere kaban, tuman dɔ la, i bɛ tɔnɔ sɔrɔ, tuman dɔ i tɛ foyi sɔrɔ, hali jagokun tɛ bɔ, i bɛ ben.
Bɛɛ tɛ se ka jago kɛ k’a masɔrɔ bɛɛ tɛ waribaara ɲalɔn.
Jago ye timinandiyako ye.
N’i timinan ka di ani n’i muɲunin bɛ, i bɛ se ka jago kɛ ka nafa sɔrɔ.
I bɛ ben fɛn dɔw la i b’o kun, i bɛ wili fana dɔw la.
Tɔnɔba tɛ feerefɛnw na.
I bɛ tɔnɔ fitinin minw sɔrɔ i b’o mara ɲɔgɔn kan ka a caya, walassa k’o don fɛn wɛrɛw la k’o feere.
Ni tɔnɔ bugula, i bɛ se ka dɔ fara jagokun kan.
Jagokɛla ka kan ka nisɔdiya sannikɛlaw kɔrɔ.
A ka kan k’i basigi baara la.

.Vocabulaire
[width="100%",cols="3",frame="none",opts="none",stripes="even"]
|====
a|a:: le
ani:: et
baara:: travail
ban:: finir
basigi:: être stable
ben:: tomber
bugula:: augmenter+acc
bɔ:: sortir
bɛ:: aux.
bɛɛ:: tous
caya:: multiplier
don:: mettre
dɔ:: certain, autre
dɔ:: en, quelque chose
dɔrɔn:: seul
dɔw:: certaines
fara:: ajouter
feere:: vendre
feerefɛnw:: choses à vendre, articles
fitinin:: petit
foyi:: rien
fɔlɔ:: premier
fɔlɔfɔlɔ:: avant, jadis
fɔn:: perdre
fɛn:: chose
hali:: même
i:: tu, toi
a|jago:: commerce
jagokun:: capital
jagokɛla:: commerçant
julaw:: Dioulas
ka kan ka:: devoir
ka:: aux.
ka:: et, pour
ka:: pour
kan:: sur
karayogow:: marchandises
kun:: supporter, endurer
kɔrɔ:: auprès de
kɛ:: faire
kɛla:: devenir+acc
kɛ… ye:: devenir
k’a masɔrɔ:: parce que
la:: dans
laban:: finir
mara:: garder
minan:: marchandise
minw:: lesquels, que
muɲunin:: être patient+marque passé
nafa:: intérêt, avantage
nata:: ambition
ni:: avec
ni:: si
nisɔdiya:: être de bonne humeur
a|ni… ye:: avec
nka:: mais
o:: cela
san:: acheter
sanni:: action d’acheter
sannikɛlaw:: acheteurs, clients
se:: pouvoir
sisan:: maintenant
sɔrɔ:: trouver, gagner
ta:: pour
ta:: prendre
timinan::  ?
timinandiyako:: une affaire de courage, de degourdie
tuma:: moment
tun:: marque imparfait
tɔnɔ:: bénéfice
tɛ:: négation
walima:: ou bien
waribaara:: travail d’argent
warikun:: capital
wili:: lever
wɛrɛw:: autres
ye:: est, être
ye:: postposition
ye… ye:: identité
ɲalɔn:: connaitre
ɲɔgɔn:: pron. réciproque
|====

== Contextes d’emploi de « Jigi » 

Descendre qqn ou quelque chose…, decu de…., en vouloir à…., prendre fin…

. Descendre qqn ou quelque chose
* Suguni (furoncle) Funnun(enflure) jigi 
* Doni ma jigi ban.

. Être déçu de…., en vouloir à
* N tɛ jigi mɔgɔ la, mɔgɔ kana jigi n na.

. Prendre fin
* Selikɛnɛ jigi la
* Baara ma jigi

. Accoucher
* Muso jigi la

. lajigi (faire décendre) 

=== Exercice: Donnez 3 exemples pour chaque contexte

.Réponses
[%collapsible]
====
. descendre qquelque chose
+
* n be jigi ɲa fɛ
* sumuni bena jigi n'i be fura ta
+
jigi être déçu

* a tɛ jigi mɔgɔ la. a be jigi ala la.

la la la verbe, aux passé en
* n tun la la i jigi la. i ka n janfa
* kana jigi O la.

lajɛn le groupement

. prendre fin
+
* nɔgɔnye jigi la
* ne ni dɔtɔrɔ ya nɔgɔnye ye ma jigi folo
* a ma kuma dabila folo   
* n'i jigila, i be ne wɛlɛ

. faire descendre
+
* jɔn bena o ya doniw lajigi?
* paranticɛ bena o ya doniw lajigi sisan 
* i be ji min ka flakise lajigi kayan
====

Denmisenw tun tɛ bɔ ni su ko la



