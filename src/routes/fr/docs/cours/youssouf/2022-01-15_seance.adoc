= L'artiste de la semaine : TIKEN JAH FAKOLY 
:author: Youssouf DIARRASSOUBA
:date: 2022-01-30
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== COURS DU JOUR : PAROLES DE CHANSON

video::WXKykvbhzf8[youtube]

KALANKƐLON:: 15/01/2022
KALANDEN:: Passokona SYLLA
KALANFA:: Youssouf DIARRASSOUBA

[verse]
____
DƆNKILILALA : TIKEN JAH FAKOLY
DƆNKILI TƆGƆ : SUNGURUBA
Ni n be taga la lɔgɔfɛ la,
boro be su la n na yɔrɔ bɛɛ la
N tolo fa la kuman caman na
N ka min kɛ, n ma min kɛ, ko ne le nɔ
Ni Allah le ye den di la
Masa le fana be nafolo di la an ma
Allah le ye lɔnin di la wo
Ole fana be furu di laaaaa

Refrain
O ko ne ma sunguruba
Allah ma furu diya ne la
O b’a fɔ ne man sunguruba
Allah ma furu diya ne la (2 fois)

Ni n be taga la lɔgɔfɛ la,
boro be su la n na yɔrɔ bɛɛ la
N tolo fa la kuman caman na
N ka min kɛ, n ma min kɛ, ko ne le nɔ
Ni Allah le ye den di la
Masa le fana be nafolo di la an ma
Allah le ye lɔnin di la wo
Ole fana be furu di laaaaa

Refrain
O ko ne ma sunguruba
Allah ma furu diya ne la
O b’a fɔ ne man sunguruba
Allah ma furu diya ne la (2 fois)

Ni Allah ka furulon se
Foyi foyi te a sa, a be kɛ
Bɛɛ ta ko ye Allah bolo
I kana ye ko furu lɔgɔ te n na

Ni Allah ka ko min lon se dununya
Nko foyi te a sa be kɛ
Bɛɛ ta ko ye Allah bolo
I kana ye ko furu lɔgɔ te n na

A kana jigi ne la
Nko i kana ne minan mɔgɔ kan man
Allah le man kiti furu la
A kana jigi ne la (2 fois)

Refrain
O ko ne ma sunguruba
Allah ma furu diya ne la
O b’a fɔ ne man sunguruba
Allah ma furu diya ne la (2 fois)
____

