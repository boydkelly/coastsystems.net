= SÉANCE 26 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-09-08
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LÉÇON 26 : KALANSEN MUGAN NI WƆƆRƆNAN

=== RUBRIQUE DIALOGUE

* Baro tan ni looru/Thème 15 :  bɛ the gɛrɛn na

Bi ye sibiri ye.
Coulibali bɛ the gɛrɛn na ni a teriw ye darɛsalam.
O bɛ to ka the min sibiri bɛɛ (sibiri o sibiri), ka politiki barow kɛ.
Tuman caman, o ya baro bɛ boli jamanan kunafoniw kan.

* A ni sɔgɔma n balenmanw !
* Heeee Moussa a bɛ di? A kɛla faman ye dêh. I tun bɛ mini?
* Coulibali, n tun wayase la (N tun tɛ Bouaké tere fila nin na). N taga la bɔ n badencɛ ye broukro
* O tun ka di wa?
* ɔn-hɔn, badenya foli dɔrɔn le ka n lase a fɛ
* Ayiwa, i danse. Annugu fana bɛ yan
* O ka nyi. Jɔn ne bena the wili bi ?
* Ile kɛ. A bɛ i nyana jɔn bena the wili ka a di i ma?
* A ka jugu dêh. Basi tɛ, n bena bi ta wili
* Ile ye i ya nanganangakumanw dabila ka an ya the wili. Ne ko oh, vote (kalafili) bena kɛ lon juman ?
* Vote (kalafili) jumɛn ?
* Ahi, ile ma a lɔn ko alugu Kɔdiwarikaw ka kan ka a ya Peresidan (masacɛ) sugudan nyinan wa ?
* N ka faamu sisan. Vote bena kɛ oktɔburu kalo la nka ne sen tɛ vote ko la. Politikimɔgɔw tɛ jamanandenw jate la
* I kana o fɔ dɛh. Ni i ma vote, i bena nimisa sabu i ka kan ka sɔn tɔw ta Peresidan sugandinin ya kokɛtaw bɛɛ ma
* I te sabari wa. Ne ya vote bena yɛlɛmanni jumɛn ne don kow la ?
* Ni bɛɛ ko a bɛ kuman ten, a ma nyi dɛ. Jamandenw kelen kelen ya vote le bɛ fara ɲɔgɔn kan ka Peresidan sugandi
* I jo le nka ne nya tɛ politikimɔgɔw la min bɛ se ka an bɔ nɔgɔ la
* O tuman na ile ma la o si la wa ?
* Ne ma la o si la, hali min bɛ an kuna nin, ale ma foyi kɛ an ye
* Ni o le, polikimɔgɔ gbɛrɛ sugandi ola kow bɛ yɛlɛman
* O bɛɛ ye kelen ye. Ni o tɛ fanga la, o bɛ mɔgɔw nɛgɛ nɛgɛ. Ni o ka fanga sɔrɔ, o bɛ nyinan o layiriw kɔ
* O ye can ye. Jamandenw ka kan ka o cɛsiri ka kow yɛlɛman. Ni o tɛ, Afiriki jamananw tɛna bɔ nɔgɔ la dêh
* A bɛ ile nyana ko yan ye faragbɛla le ye wa?
* Ile ni ne tɛ se ka bɛn oh. An fila hakilinanw tɛ kelen ye. The peremiye mɔn na wa?
* A to la dɔɔni. N bena sukaro kɛ a la sisan sisan
* The mɛn na ta la dɛh
* Ile le ma an bila kuman caman na wa ?
* A ka nyi ten sa! Ile fana ya kuman ka ca kojugu.

=== LA FORMULE GRAMMATICALE DU JOUR

L’usage de « telen » (terme issue d’une déformation phonétique de « tere ») pour exprimer l’idée de « passer la journée… »

Prés. Affir. (Je passe la journée)         Pres. Nég. (je ne passe pas la journée)

* N bɛ telen                               - N tɛ telen
* I bɛ telen                               - I tɛ telen
* A bɛ telen                               - A tɛ telen
* An bɛ telen                              - An tɛ telen
* A bɛ telen                               - A tɛ telen
* O bɛ telen                               - O tɛ telen

Accompli                                Non accompli

* N telenna dugu kɔnɔ                      - N ma telen
* I telenna dugu kɔnɔ                      - I ma telen
* A telenna dugu kɔnɔ                      - A ma telen
* An telenna dugu kɔnɔ                     - An ma telen
* A telenna dugu kɔnɔ                      - A ma telen
* O telenna dugu kɔnɔ                      - O ma telen

✓ Passer la nuit

* Moussa bɛ si (shi) a
* Kunu, Coulibali si (shi) la kɛnɛ ma
* Youssouf si (shi) la baarakɛyɔɔrɔ la
* A ka tele fila si (shi) dɔtɔrɔso la

=== DIVERS

Discussions autour des éventuelles préoccupations de l’apprenant à propos des précédents cours ou de situations de communication qu’il souhaite mieux comprendre. 
Toutes les questions sont les bienvenues.

