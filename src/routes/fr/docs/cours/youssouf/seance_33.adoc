= SÉANCE 33 COURS DE DIOULA
:author: Yousouf Diarrasouba 
:date: 2020-10-17
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:pdf-themesdir: ./resources/themes
:pdf-fontsdir: ./resources/fonts
:pdf-theme: mandenkan 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LÉÇON 33 : KALANSEN BI SABANAN NI SABANAN

=== CORRECTION DE L’EXERCICE 2 DU PRECEDENT COURS

=== LECTURE ET COMPREHENSION DU DIALOGUE

[abstract]
Contexte : échange entre Coulibali et KADIDJA : Coulibaly de passage devant chez KADIDJA décide de passer prendre de ses nouvelles.
Alors s’engage une conversation dont le contenu est dense en termes de courtoisie et de fraternité.

[%hardbreaks]
I ni wula
Nse, hɛrɛ telenna wa ?
Tɔɔrɔ si tɛ n na, i ka kɛnɛ wa?
Tɔɔrɔ si tɛ n na, sigiyɔrɔ bɛ yen
I ni ce
Dɔ d’an ma wula fɛ
Juguman tɛ, n tɛmɛtɔ le, n ko n bɛ lɔ ka n ɲɛ la i kan.
Aaa, i ni ce foli la, an fana bɛ yan, baasi si tɛ yan. N bɔtɔ le dugu kɔnɔ, n ma na joona, n ɲa gbannin bɛ sulafana tobili la.
ɛɛɛ, gbatigicɛ ma taga yɔrɔ si bi wa ?
tele fila nin na, a tɛ taga baara la sabu a fari ma di a la. a taga la tagama-tagama nka dɔɔnin a bena nan
Ala y’a ɲuman segi
Amiina
Denmisɛnw do ?
O taga la kalankɛyɔrɔ la. O tɛ na mɛn ka na k’a masɔrɔ sɛnkɛri sela, O jigintuman sela
Ayiwa n bɛ sira ɲini n fana bena taga n taw kunbɛn. I ka denmisɛnw lɔn, n’o jigila o tɛ sin ka na so kɔnɔ, o b’a kɛ tolonkɛ ye sira fɛ
Ayiwa sira bɛ yen. k’an b’o fo
O bɛn’a mɛn, k’an bɛn lon wɛrɛ

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Ala:: Dieu
a:: il, lui
a:: le
aaa:: ah !
aaa:: interjection
an:: nous
baara:: travail
baasi:: mal
bi:: aujourd’hui
bɔtɔ:: sortant
bɛ:: aux
bɛ:: aux. présent
bɛ:: est
bɛ:: être
denmisɛnw:: enfants
di:: bon
di:: donner
don ?:: et ?
dugu:: ville
dɔ:: en, quelque chose
dɔɔnin:: un peu
fana:: aussi, également
fari:: corps
fla:: deux
fo:: saluer
foli:: salutation
fɛ:: postposition
gba:: repas
gbannin:: chauffer
gbatigicɛ:: chef de famille
i ni ce:: merci
i:: toi
jigi la:: descendre+acc
jigin:: descendre
juguman:: méchant
ka kan ka:: falloir, devoir
a|
ka:: aux
kalankɛyɔrɔ:: classe
kan:: sur
ko:: dire
kunbɛn:: rencontrer
kɔnin:: en tout cas
kɔnɔ:: dans
kɛ:: faire
k’a masɔrɔ:: parce que
k’an bɛn:: au revoir
la:: au
la:: dans
la:: en, dans
la:: mettre, poser
la:: pour, dans
lo:: c’est
lon:: jour, date
lɔ:: arreter
lɔn:: connaitre
ma:: à
ma:: négation passé
man:: nég. Devant adjectif
mɔn:: cuire
mɛn:: durer
mɛn:: entendre
n taw:: les miens, les miennes
n:: je
na:: dans
na:: futur
na:: venir
ne:: je, moi
ni:: si
nin:: ceci, caa
nka:: mais
o:: ils
o:: ça
a|
sabu:: parce que
sani:: avant
segi:: retourner
sela:: arriver+acc
si:: aucun
sin:: aller droit, se diriger
sira:: route
so::  maison
sulafana:: repas du soir, souper
sɔɔnin:: bientôt
sɛnkɛri:: cinq heures
taa:: partir
taama:: marcher
taga la:: partir+acc.
taga:: partir
tele:: jour
tobili:: préparation
tolonkɛ:: amusement, jeu
tuma:: moment
tɛ:: nég
tɛ:: ne…pa
tɛ:: postposition
tɛmɛtɔ:: passant
u:: ils
wula:: soir
wɛrɛ:: autre
yan:: ici
ye:: aux
ye:: postposition
yen:: là
yɔrɔ:: endroit, lieu
yɔɔrɔ si:: nulle part
ɲini:: demander, chercher
ɲuman:: bon
ɲɛ:: œil
|====

