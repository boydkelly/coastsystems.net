= SÉANCE 34 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-10-24
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LÉÇON 34 : KALANSEN BI SABANAN NI NAANINAN

=== REVISION DE CERTAINS POINTS DES LECONS PRECEDENTES

* [*] Exercice 1 : Répondre à ces questions

. Coulibali bɛ bɔ mini ?
. Mandenkan bɛ sɔrɔ jamanan jumɛn na ?
. Bouake televizɔnbon bɛ kin jumɛn na?
. Bouake kin jumɛn cɛ ka nyi katimi tɔw kan?
. Siya jumɛn le ka ca Bouaké ?
. Kalo joli bɛ san kelen kɔnɔ?
. Lɔgɔkun joli bɛ kalo kɔnɔ ?
. Bolokanden joli bɛ adamanden na ?
. Adamaden ka kan ka domuni kɛ sinyɛn joli tere kɔnɔ ?
. Domunifenw la, mun ka di Coulibali ye?
. Farafinw ka ca Canada wa?
. I ya lakofifa siginin bɛ dugu jumɛn na?

* [*] Exercice 2 : Traduire ces phrases (comprendre certains emplois de « ye »)

. A kɛ la baara ye
. A kɛ la sigiɲɔgɔnya ye
. A kɛ la hɔrɔn ye
. A ma kɛ kɔnɔnangbo ye
. A kɛ la cɛ ye
. A kɛ la mɔgɔya ye
. A ma kɛ baganya ye
. A kɛ la dugawuden ye
. A ma kɛ dangaden ye

. Bere kɛ la sa ye
. Den kɛ la mɔgɔba ye
. Coulibali kɛ la cɛndala ye
. Jɔn ka fisa ni jɔn ye ?
. Mɔgɔ ma fisa ni mɔgɔ ni mɔgɔ ye
. An bɛɛ ye Ala denw ye
. Mɔgɔ kelen fa tɛ Ala ye

[%collapsible]
====
C'est devenu du boulot
C'est devenu entre voisins
Il est devenu intégre
Il n'est pas devenu égoiste
Il est devenu un homme
C'est devenu humanisme
Ce n'est pas devenu cause d'animosité
Le bâton est devenu serpent
L'enfant est devenu un grand type
Nous sommes tous les enfants de Dieu
?
====

=== LECTURE ET COMPREHENSION

Kalanso kɔnɔ (Dans la salle de classe)

[width="100%",cols="1",frame="none",options="none",stripes="even"]
|====
|KALIFA ye kalanden ye. a ya kalanso bɛ a ya so kɛrɛ fɛ. 
Lon bɛɛ kalifa bɛ taga kalanso kɔnɔ.
Sibirilon ni karilon, a bɛ to so kɔnɔ k’a yɛrɛ lafɔɲɔ.

KALIFA ye kalanden ɲuman ye. A hakili ka di. A bɛ baara kɛ. a b’a ya karamɔgɔ bonya. karamɔgɔ
b’a fɔ: "KALIFA ! I wili, i sigi, nan tabulo la, kalifa! lakɛrɛ ta ka lɔgɔkun kɔnɔ lonw sɛbɛ". 
KALIFA b’a sɛbɛ: "Tɛnɛn, Tarata, Araba, Alamisa, Juma, Sibiri, Kari". 
Karamɔgɔ b’a fɔ : "o nyan na KALIFA. 
Ayiwa i bɛ miiri la mun ma? kalo joli bɛ san kɔnɔ? A sɛbɛ".

KALIFA : "n tɛ miiri la foyi ma, kalo tan ni fla bɛ san kɔnɔ. 
Zyanwuye kalo, Feburuye kalo, Marisi kalo, Avirili kalo, Mɛ kalo, Zuɛn kalo, Zuye kalo, Uti kalo, Sɛbutanburu kalo, ɔkutɔburu kalo, Novanburu kalo ani Desanburu kalo”.

Karamɔgɔ bɛ KALIFA ɲininga tugun: "KALIFA, ne ko: yala saminya joli bɛ Kɔdiwari? "
KALIFA : "Saminya fla bɛ Kɔdiwari: Saminyaba ani saminya dennin"

KALIFA bɛ a somɔgɔw dɛmɛ ka baara kɛ.
A bɛ lɛtɛrɛ sɛbɛ a yɛrɛ ye. KALIFA ko ka di bɛɛ ye.
|====

==== N’BALENMANCƐ, NYININGALI NINUGU JABI

. Kalifa ye jɔn (mun) ye ?
. Kalifa ya lakoliso yɔrɔ ka jan a ya so la wa?
. Muna Kalifa ko ka di bɛɛ ye?
. Kalifa bɛ taga lakoliso la lon bɛɛ wa?
. Ile hakili la, muna Kalifa ye kalanden nyuman ye?
. Kalifa bɛ sɛbɛli kɛ ni mun ye?


[%collapsible]
====
. Kalifa ye kalenden ye
. ɔ́n-ɔ̀n, Kalifa ya lokoliso yɔrɔ ka surun a ya so la.
. 
. Kalifa bɛ taga lakoliso la Tɛnɛn, Tarata, Araba, Alamisa, Juma  
. Kalifa ye kalanden ɲuman ye sabu, a hakili ka di, ni a b'a karaɔgɔ bonya.
. Kalifa be sɛbɛli kɛ ni lakɛrɛ ye.
====

.VOCABULAIRE
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Alamisa:: Jeudi
Araba:: Mercredi
Avirili kalo:: Avril
Desanburu kalo:: Décembre
Feburuye kalo:: Février
Juma:: Vendredi
Kalifa:: prénom masculin
Kari:: Dimanche
Kɔdiwari:: Côte d’Ivoire
Marisi kalo:: Mars
Mɛ Kalo:: Mai
Novanburu kalo:: Novembre
Sibiri:: Samedi
Sɛbutanburu kalo:: septembre
Tarata:: Mardi
Tɛnɛn:: Lundi
Uti kalo:: Août
Zanwuye kalo:: Janvier
Zuye kalo:: Juillet
Zuɛnkalo:: Juin
a:: il, le
baara:: travail
baro:: dialogue
bonya:: respecter
bɛ:: aux.présent
bɛ:: être
bɛɛ:: tous
dennin:: petite
di:: bon
dɛmɛ:: aider
fla:: deux
a|
fɔ:: dire
fɛ:: à, postposition
hakili:: esprit, mémoire
i:: toi
joli:: combien
ka:: aux., être, pour
kalanden:: élève, étudiant
kalansen:: lecon
kalanso:: classe
kalifa:: confier
kalo:: mois
karamɔgɔ:: maître, professeur
karilon:: dimanche
ko:: affaire
kɔnɔ:: en, dans
kɛ:: faire
kɛrɛ:: côté
la:: au
lafɔɲɔ:: reposer
lakɛrɛ:: craie
lon:: jour
lonw:: jours
lɔgɔkun:: semaine
lɛtɛrɛw:: lettres
ma:: à
miiri:: penser
mun:: quoi
na:: venir
ne:: moi, je
ni:: et
o:: ça, cela
a|
saminya:: saison de pluie, hivernage
saminyaba:: grande saison de pluie
n:: année
sibirilon:: samedi
sigi:: asseoir
so:: maison
somɔgɔw:: parents
sɛbɛ:: écrire
ta:: prendre
taa:: partir
tabulo:: tableau
to:: rester
tugun:: encore, plus, une fois de plus
wili:: lever
wolonfla:: sept
wolonflanan:: septième
ya:: possessif
yala:: placer en debut de phrase pour une question
ye:: être, postposition, pour,à
ye...ye:: être, identité
yɛrɛ:: même
ŋɛna:: a été bien
ɔkutɔburu kalo:: Octobre
ɲininga:: demander, questionner
ɲuman:: bon
|====
