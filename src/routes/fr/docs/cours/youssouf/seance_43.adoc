= SÉANCE 43 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-12-26
:keywords: Julakan, Language, Leçons
:category: Julakan
:tags: Julakan, Language, Leçons
:page-noindex: 
:lang: fr
:page-lang: {lang}
include::{includedir}locale/attributes.adoc[]

== LÉÇON 43 : KALANSEN BI NAANI NI SABA

=== BARO : KIBAROYA

An balema cɛman n’a musoman
An sila hɛrɛ la, Ala ye tere nɔgɔya.
Ayiwa, kibaroya min bɛ yen k’a lase a ma, o ye ko bolokɛninden kelen tɛ se ka bɛlɛkisɛ ta ; 

ko flanan kisɛden kelen tɛ se ka yɔrɔ flan ka gbɛ; 

ko kɔnɔw jɛnin le bɛ biii fɔ ; 

ko mɔgɔ kelen tɛ se ka faso baara kɛ ;

ko an ye an bolo di ɲɔgɔn ma ; mɔgɔ ma bɔ mɔgɔ la, siya ma bɔ siya la ;

ko an ye kɛlɛkɔrɔw ban an ni ɲɔgɔn cɛ ; 

ko an ye don da kelen fɛ ka jɛ ka baara kɛ walasa faso ye taga ɲɛ ka kunnawolo ;

ko an ye an ya kanw fɔ k’o sɛbɛcogo kalan.

N’o kɛra, jamana bɛ diya, faasodenw tɛ na kɔngɔ, minnɔgɔ tɛ n’an minan fana, ayiwa an bɛɛ bena langafiya sɔrɔ.

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Ala:: Dieu
a:: pron. 3e pers. sing
an:: nous
aw:: vous
ayiwa:: alors, eh bien
baara:: travail
balema:: parent
ban:: finir
biii:: idéophone, bruit
bolo:: main
bɔ:: enlever, sortir
bɛ:: être
bɛɛ:: tous
cɛ:: entre
cɛman:: mâle, viril
da:: porte
diya:: agréable
don:: entrer
fana:: aussi
faso:: patrie
fasobaara:: travail de la patrie
fasodenw:: citoyens, peuple
flan:: balayer
flanan:: balai
fɔ:: dire
a|
fɛ:: par
gbɛ:: rendre propre
hɛrɛ:: la paix
jamana:: pays
jɛ:: s’unir
jɛnin:: unis, regroupés
ka:: aux.
ka:: connectif
ka:: pour
kalan:: lire
kelen:: un, une
kibaroya:: nouvelle
kisɛden:: brindille
ko:: dire
kunnawolo:: honorer
kɔngɔ:: avoir faim
kɔnɔw:: oiseaux
kɔrɔw:: anciennes
kɛ:: faire
kɛlɛ:: guerre, querelle
kɛra:: faire+acc
la:: dans
langafiya:: repos, tranquilité, paix
lase:: annoncer
le:: présentatif
a|
ma:: nég
ma:: postposition
min:: qui
mina:: attrapper
minnɔgɔ:: soif
musoman:: femelle
mɔgɔ:: personne
na:: marque futur
ni:: et, si
o:: ça, cela
o:: les
se:: pouvoir
sila:: passer la nuit+acc
siya:: race, ethnie
sɛbɛ:: écrire
taa:: partir
telenna:: passer la journée +acc
tɛ:: nég.
walasa:: fin que
ye:: aux
ye:: être
yen:: là
yɔrɔ:: endroit
ŋɛ:: devant
ɲɔgɔn:: les uns, les autres
|====

=== POINT DE GRAMMAIRE : LES PROCÉDÉS DE DESIGNATION DES MOTS

=== Mots composés à partir d’un adjectif +le suffixe d’abstrait

Nɔgɔ+ya:: facilité, simplicité
Gbɛlɛ+ya:: cherté, difficulté, pauvreté
Jugu+ya:: méchancete
Ɲuman+ya:: bonté, générosité
Bon+ya:: la grandeur
Kunfin+ya:: ignorance

==== EXERCICE

* Certaines personnes aiment la facilité
* La méchanceté est un mauvais comportement
* Dieu recommande la bonté
* L’ignorance est une maladie
* La grandeur de Dieu ne peut être expliquée

.Réponses
[%collapsible]
====
. Nɔgɔya ka di mɔgɔ dɔw ye.
. Juguya ye jogo jugu ye.
. Ɲumanya ka di Ala ye.
. Kunfinya ye bana ye.
. Adamadenw tɛ Ala ya bonya lɔn.
** Adamadenw tɛ se ka Al ya bonya ɲafɔ
====

=== Mots composes a partir d’un nom+le suffixe de l’abstrait

Denya:: l’enfance
Denmisɛnya:: l’enfance
Mɔgɔya:: la civilité (l’humanisme)
Musoya:: la féminité ou le fait d’être une femme
Cɛya:: la masculinité ou le fait d’être un homme
Dioulaya:: le fait d’être Dioula
Farafinya:: africanité (le comportement ou l’attitude africaine)

==== EXERCICE

* L’enfance est agréable
* Ce monsieur à une attitude de blanc (européen)
* En Afrique, le fait d’être une femme n’est pas facile
* Ce monsieur est très humain

.Réponses
[%collapsible]
====
. Denya ka di
. Tubabu jogo / hakili bɛ cɛ nin na.
. Musoya ka gwɛlɛ farafina.
. Mɔgɔya be cɛ nin na kojugo.
====

==== Former des verbes de processus à partir d’un adjectif + le suffixe de l’abstrait

bonya:: grossir 
gbiliya:: alourdir
gboya:: rendre désagréable
janya:: grandir
nɔgɔya:: faciliter
misɛnya:: émietter
surunya:: raccourcir
juguya:: rendre méchant
kalaya:: chauffer
dɔgɔya:: diminuer
diya:: rendre agréable (doux)
fisa:: préférer, rendre meilleur

==== Proposez quatre phrases avec ces mots

.Réponses
[%collapsible]
====
. i be se ka kalan nɔgɔya n bolo. 
** a bena feerefɛn sɔngɔ jigi n ye.
** a bena dɔ bɔ sɔngɔ la ne ye
. an bena bon kalaya
. adamadenw ka kan ka o balemaw bonya
. jagokɛlaw tɛ sara
. sanji be lɔgɔ gboya
====

=== NYININGALI NINUGU JAABI

Je vais te poser un ensmble de questions (10 au moins) que tu devras repondre de facon pragmatique.

