#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

sectnumlevels=2
toc="true"
includedir="{includedir}"

[[ -z "$1" ]] && { echo "need a file" ; exit 1 ; }
sed -n '/^=\s/q1' "$1" && { echo "this looks like it doesn't have a title" ; exit 1 ; }
file=$1
#delete blank lines
#sed -i '10,19{/^[[:space:]]*$/d}' "$1" 

title=`sed -n /^\=\ .*/p "$1"` 
author=`awk /author:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
categories=`awk /categories:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
tags=`awk /tags:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`

[[ -z $author ]] && author="Boyd Kelly"
date=`awk '/date:/{print $2}' "$1" | uniq` 
[[ -z "$date" ]] && date=`date -Im`
type="blog"
lang="fr"
draft="true"
[[ -z $categories ]] && categories="[]"
[[ -z $tags ]] && tags="[]"

#sed -i 's/^:lang:/:page-lang:/g' $1
sed -n '/^:include::locale\s/q1' "$1" &&  echo  ok found it  
#sed -i "s/^:include::locale/:include::$includedir\/locale/g" "$1"
sed -i "s/include::locale/include::$includedir\/locale/g" $1

#if [ ! $lang = "en" ]; then
#  sed -n '/^:lang:\s/q1' "$1" &&  sed -i "/^:sectnumlevels:/a :lang: $lang" "$1" 
#sleep 0.25 
#  sed -i '/:includedir:.*/d' "$1" && sed -i "/^:lang:/i :includedir: content/$lang/$includedir" "$1"
#sleep 0.25 
#  sed -i '/include::locale/d' "$1" &&  sed -i '/^:lang:/a include::locale/attributes.adoc[]' "$1" 
#sleep 0.25 
#  sed -i "/^include::locale/a #;" "$1" && sed -i 's/^#;//g' "$1"
#fi

#sed -i "/^\=\ .*/a #;" "$1"
#sed -i '18 a #;' "$1" && sed -i 's/^#;//g' "$1"
# 

#!/usr/bin/bash
#[[ -z $1 ]] && { echo "need a file" ; exit 1 ; }
#sed -n '/^---\s/q0' $1 || { echo "this looks like it already has a header" ; exit 1 ; }
#this returns 0 when its NOT found.   
#sed -r -n '/^---(\s|$)/q1' "$1" || { echo " This looks like it already has a header" ; exit 1 ;  }
