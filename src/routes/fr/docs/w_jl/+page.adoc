= Morphology of the Jula language in the Watchtower
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2022-01-14
:page-image: w_JL_w100.svg
:description: Morphology study of the Jula language in the Watchtower magazine
:keywords: watchtower, jula, language
:page-tags: watchtower, jula, language
:category: Language
:lang: en

image:{page-image}[Alt text,auto,200]

[NOTE]
.Voir aussi:
====
* link:/fr/docs/w_jl_w100[La compilation des 100 mots Jula les plus fréquents utilisés dans la Tour de Garde]
* link:/en/docs/kodi-w100/[The 100 most common words in Ivory Coast Jula (Kodi)]
====

[WARNING]
.Les chiffres ci-dessous sont sujets à changer pour plusieurs raisons:
====
. Les nouvelles revues sont ajoutés (ou retirés) à l'inventaire
. Les erreurs peuvent se produire avec les mot français qui doivent être retirés du texte jula.
* Les noms propres français et jula sont difficiles à gérer.
. L'usage du pluriel reste à déterminer
====

include::{includedir}w_JL_include.adoc[]

