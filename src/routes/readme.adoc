= Notes

The home page for all sections will be en/.   
There will be no 'index' page for docs/ or blog/.
There should be links to open and docs/[doc] file or any blog[slug] file

the [doc] and [slug] directories will require 4 files

+page.svelte:  receives the data
+page.js:  converts the adoc file from .. and hands to +page.svelte

+layout.svelte:  provides the layout with header meta data
+layout.js fetches the meta data from .. and hands it to +layout.svelte
