import adapter from '@sveltejs/adapter-static';
import sveltePreprocess from 'svelte-preprocess';
import { optimizeImports } from 'carbon-preprocess-svelte';
import { mdsvex } from 'mdsvex';
import { processAsciiDoc } from './src/lib/js/asciidoc.js';
import fs from 'fs';
import path from 'path';

// Convert all `.adoc` files from specified directories
const contentDirs = [
  'src/routes/en/blog',
  'src/routes/fr/blog',
  'src/routes/fr/docs/cours/youssouf',
  'src/routes/fr/docs/cours/contes',
  'src/routes/fr/docs/cours/soumahoro',
  'src/routes/fr/docs/cours/baoule'
];

for (const dir of contentDirs) {
  const contentDir = path.resolve(dir);
  if (fs.existsSync(contentDir)) {
    const files = fs.readdirSync(contentDir).filter((f) => f.endsWith('.adoc'));

    for (const file of files) {
      const filePath = path.join(contentDir, file);
      //      console.log('File:', file);
      const { content: html } = processAsciiDoc(fs.readFileSync(filePath, 'utf-8'));

      fs.writeFileSync(filePath.replace('.adoc', '.html'), html, 'utf-8');
    }
  }
}

// function svmdsvex() {
//   return {
//     mdsvex({
// 			extensions: ['.md']
// 		})
//   }
// }

function svasciidoc() {
  return {
    markup: async ({ content, filename }) => {
      if (
        filename.endsWith('.adoc') &&
        !filename.includes('/_include/') &&
        !filename.includes('/locale/')
      ) {
        const { content: processedHtml } = processAsciiDoc(content);
        return { code: processedHtml };
      }
    }
  };
}

const config = {
  extensions: ['.svelte', '.adoc', '.md', '.html'],
  preprocess: [
    sveltePreprocess(),
    mdsvex({
      extensions: ['.md']
    }),
    svasciidoc(),
    optimizeImports()
  ],
  kit: {
    adapter: adapter({
      pages: 'build',
      assets: 'build',
      fallback: undefined,
      precompress: false,
      strict: true
    }),
    prerender: {
      handleHttpError: 'warn',
      handleMissingId: 'warn',
      entries: [
        '/',
        '/en/',
        '/en/contact/',
        '/en/docs/w_jl/',
        '/en/docs/wtlex/',
        '/en/docs/wtlex-doc/',
        '/en/docs/freq/',
        '/en/docs/carbon/',
        '/fr/',
        '/fr/about/',
        '/fr/docs/',
        '/fr/contact/',
        '/fr/docs/w_jl/',
        '/en/docs/freq/',
        '/fr/docs/wtlex/',
        '/fr/docs/cours/test/',
        '/fr/docs/mandenkan-bibles/',
        '/fr/docs/freq/',
        '/fr/docs/carbon/',
        '/bci/',
        '/bci/docs/udhr/',
        '/bm/',
        '/bm/about',
        '/bm/docs/kodi-w100/',
        '/bm/docs/fontlist/',
        '/bm/blog/',
        '/bm/docs',
        '/dyu/',
        '/dyu/about',
        '/dyu/docs',
        '/dyu/docs/kodi-w100/',
        '/dyu/docs/fontlist/',
        '/dyu/blog/'
      ]
    }
  }
};

export default config;
