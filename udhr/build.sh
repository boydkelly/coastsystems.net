#!/usr/bin/bash
# anylyse udhr in four languages extract longest words extract
# and create json files for use in word cloud and web pages
# set -o errexit
# set -o nounset
# set -o pipefail

project=udhr
build=build
target=../src/assets/metadata/

[[ -d build ]] || mkdir build
for x in tmp yaml adoc json; do export "${x}=$build/${project}.$x"; done

function nowhite() {
  sed -i 's/[ \t]*$//' $1
}

function analyze() {
  export lang=$1
  [[ -d $build/$lang ]] || mkdir -p $build/$lang/images
  #images="$lang/images"
  #file in path in all 3 langs
  filepath="../src/routes/$lang/docs/udhr.adoc"
  file=${filepath##*/}
  base=${file%.*}

  all=$build/$lang/${base}.${lang}.all.txt
  unique=$build/$lang/${base}.${lang}.unique.txt
  sort stop.all.txt "stop.$lang.txt" >$build/stop.txt
  #nowhite stop.txt
  #tail -2 stop.txt
  groups=5 # Set the number of groups you want to test
  asciidoctor -a includedir=../../../ -a skip-front-matter "$filepath" -o - | lynx -stdin -dump |
    sed -e "s/[^[:alpha:]’]/ /g; s/[[:space:]]/\n/g" | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] |
    sed '/^$/d' | tee >(sort | awk '{print $0}' >"$all") | sort | uniq -c |
    awk 'NR==FNR{stop[$1]; next} !($2 in stop)' $build/stop.txt - |
    awk -v groups="$groups" '{print NR, length($2), "group-" (NR-1) % groups + 1, $0}' >"$unique"

  total_words=$(wc -l <"$all") && export total_words
  unique_words=$(sort <"$unique" | uniq | wc -l) && export unique_words

  random_words="($(shuf -n 10 <"$unique" | awk '{print $5}' | tr '\n' ' '))"
  json_random_words=$(printf '%s\n' "${random_words[@]}" | jq -R . | jq -s .) && export json_random_words
  #  echo "${random_words[@]}"

  longest_words="($(sort -rn -k2 <"$unique" | awk '{print $5}' | head -10))"
  json_longest_words=$(printf '%s\n' "${longest_words[@]}" | jq -R . | jq -s .) && export json_longest_words
  #  echo "${longest_words[@]}"

  yq -i -oj '.udhr.[env(lang)] |= . +{"id": env(lang)}' "$json"

  yq -i -oj '.udhr.[env(lang)] += .random[]' "$json"
  yq -i -oj '.[].[env(lang)].random |= env(json_random_words)' "$json"

  yq -i -oj '.udhr.[env(lang)] += .longest[]' "$json"
  yq -i -oj '.[].[env(lang)].longest |= env(json_longest_words)' "$json"
  #
  yq -i -oj '.[].[env(lang)] |= . +{"total": env(total_words)}' "$json"
  yq -i -oj '.[].[env(lang)] |= . +{"unique": env(unique_words)}' "$json"

  yq -i -oj '.udhr.[env(lang)] |= . +{"all": null}' "$json"

  while read -r -a columns; do
    export id="${columns[0]}"     # The first column is the ID
    export length="${columns[1]}" # The second column is the word length
    export group="${columns[2]}"  # The second column is the word length
    export count="${columns[3]}"  # The third column is the count
    export word="${columns[4]}"   # The fourth column is the word
    yq -i -oj '.udhr.[env(lang)].all |= . +[{"id": env(id),
    "length": env(length),
    "group": env(group),
    "count": env(count),
    "word": env(word)}]' "$json"
  done <"$unique"
  #python3 ./udhr.py $lang $base
}

: >"$json"
yq -n -oj '.udhr = {"en": null, "fr": null, "dyu": null, "bm": null}' >"$json"
#now that we know we need null we could probably do this too.
#yq -n -oj '.udhr: null  >udhr.json
for lang in en bm dyu fr; do
  echo processing: $lang
  # nowhite stop.txt
  analyze $lang
done

echo $json
cp -v "$json" "$target"
