[#IV]
= {nbsp}
:sectnums:
:partnums:
:part: IV
:!sect:
:!exercise:
:!chapter-number:

==  {nbsp}

===  {nbsp}

Les verbes transitifs peuvent être employés à la voix active ou à la voix passive. 
Dans la leçon III-0, seule la voix active a été traitée, nous allons maintenant étudier la voix passive. 
Le principe fondamental est le suivant : un verbe transitif employé sans objet est à la voix passive ; Exemples :

Màlo bé dómu:: le riz se mange
Nìn tɛ́ kɛ́:: Ceci ne sé fait
Sògo má tìgɛ:: La viande n'as pas été coupé

Dans ces trois exemples les trois verbes transitifs dómu, kɛ́, tìgɛ sont employées sans objet: le sens dé l'enoncé est passif. 
Dans le cas du passé affirmatif, la forme passive né se forme pas avec "ka" (ye) qui nous l'avons vu, est réservé à l'emploi actif des verbes transitifs - mais avec le suffixe -la (-ra, -na) exemple :

Sògo tìgɛla:: la viande a été coupée
Màlo dómula:: le riz a été mangé

Les verbes mixtes peuvent fonctionner comme des verbes transitifs ou intransitifs; exemples. : dòn (entrer, enfiler…)

Musa dònna:: Moussa est entré
Musa bé dòn:: Moussa entre
Musa má dòn:: Moussa n'est pas entré
Musa kà dèreke dòn:: Moussa a enfilé la chemise
Dèreke dònna:: La chemise a été enfilée
Dèreke má dòn:: La chemise n'a pas été enfilée

Dans les trois premiers exemples, dòn est employé comme táa ou nà, son sens est actif. 
Dans la quatrième phrase, il est employé comme un verbe transitif, avec un objet, et son sens est actif;
dans les deux derniers exemples, dòn est employé sans objet: seul, le sens de dèreke (chemise) permet de savoir que la phrase est passive;
dans un exemple comme à bé dòn, suivant que à est animé (Moussa…) ou inanimé (chemise), la phrase signifiera:
il entre ou ça s'enfile.

Les verbes mixtes sont extrêmement fréquents en dioula: bɔ (sortir, enlever…), pán (sauter, enjamber…), wúli (se lever, bouillir..), etc…     

"tun" est un élément grammatical dont la présence dans un énoncé transforme l'aspect de la marque d'énoncé (lò, bé ou marques d'énoncés adjectivaux que nous verrons en VII-0-1); exemples:

à táara:: il est parti
à tùn táara:: il était parti
à bé yàn:: il est ici 
à tùn bé yàn:: il était ici
à má Musa yé:: l n'a pas vu Moussa
à tùn má Musa yé:: i il n'avait pas vu Moussa
wàri té:: ce n'est pas de l'argent
wári tùn té:: ce n'était pas de l'argent

Dans certains énoncés complexes, que nous n'étudierons pas ici, "tun" transforme un futur en conditionnel. 
Sa valeur sémantique est donc l'inactualité, passé ou future (mode inactuel).

"kà" est un élément connectif permettant de coordonner deux ou plusieurs énoncés; exemples:

[ordered]
à táara Abidjan:: il est parti à Abidjan
à kà móbili san:: il a acheté une voiture
à táara Abidjan kà móbili sàn:::  il est parti à Abidjan et a acheté une voiture 

[ordered]
à kà à yé:: il l' a vu
à kà à fò:: il l'a salué
à kà à yé ka à fò::: il l'a vu et l'a salué 

[NOTE]
====
[loweralpha]
. kà relie seulement des énoncés qui ont le même sujet,
. toutes les marques verbales, (aspect, mode…) sont supprimées après kà: le verbe est alors à sa forme nue, 
. après les verbes nà et táa, kà est élidé, sauf lorsque ces verbes sont suivis de circonstants; exemples:
+
à táara à fò:: il est allé le saluer 
à táara Abidjan kà à fò:: il est allé à Abidjan le saluer
====

"kó" est un élément grammatical très fréquent en dioula: ce n'est pas, bien qu'on le traduise dans certains cas par "dire", un verbe, puisque "kó" n'accepte aucune des marques verbales; voici quelques exemples d'emploi :

à kó dì?:: que dit-il?
à kó à bé nà:: il dit qu'il vient
Musa b' a fɔ́ kó a nàna:: Moussa dit (va dire) qu'il est venu

"dì" est un élément interrogatif, comme mùn, mín…; son emploi est plus restreint que les interrogatifs vus précédemment; exemples:

à kó dì?:: que dit-il?
à kɛ́la di?:: comment ça s'est passé?
à nána cógo di ?:: comment est-il venu?

"fóyi" (rien s'emploie uniquement dans une phrase négative; exemples:

à má fóyi kɛ́:: il n'a rien fait
Musa má fóyi sàn:: Moussa n'a rien acheté.
fóyi tɛ́:: ce n'est rien

"dɔ́" est une marque de l'indéfini; exemples:

mùso nàna:: la femme est venue
mùso dɔ́ nàna:: une femme est venue
à kà móbili sàn:: il a acheté la voiture
à ka móbili dɔ́ sàn:: il a acheté une voiture 

Lorsque dɔ́ est employé sans déterminé, c'est-à-dire isolément, il correspond à peu près au "en" français; exemples:

à ma dɔ́ yé:: il n'en a pas vu
dɔ́ bé Abidjan:: il y en a à Abidjan

dɔ́ prend la marque du pluriel ; exemples:

mùso dɔ́w nàna:: des femmes sont venues
à kà móbili dɔ́w sàn:: il a achété des voitures
dɔ́w bé yàn:: certains sont ici

Enfin, dɔ́w… dɔ́w correspond à l'expression certains… d'autres; exemples:

dɔ́w táara Abidjan, dɔ́w táara Bouaké:: certains sont partis à Abidjan, d'autres à Bouaké. 

kúnù (hier) est un nom fonctionnant comme circonstant, dont on verra le fonctionnement en <<VI-0-2>>.
Remarquons seulement qu'il peut se placer soit au début, soit à la fin de l'énoncé; exemples :

kúnú, à táara:: hier, il est parti
à táara kúnù:: il est parti hier
 
án et áw correspondent respectivement aux première et deuxième personnes du pluriel; exemples.:

án táara:: nous sommes partis
án dénw táara:: nos enfants sont partis
áw táara:: vous êtes partis
áw dénw má táa:: vos enfants ne sont pas partis án peut être également prononcé ánw.

áw peut étre également prononcé á (différent de à, pronom de la troisième personne du singulier).

Le système pronominal se présente de la manière suivante:

.Système pronominal
[width="100%",cols="4",frame="topbot",opts="none",stripes="even"]
|====
|1er sing
|ń (je)
|né/né le (moi je )
|né lò/né le lò (c'est moi qui)

|2e
|í
|íle/é
|íle lò

|3e
|à
|àle
|àle lò

|1ère  pl.
|án/ánw
|ánwle
|ánwle lò

|2e
|áw/á
|áwle
|áwle lò

|3e
|ò
|òle
|òle lò
|====

De nombreuses variantes sont possibles, suivant les régions et suivant les personnes: ò/ù, ánwle/ánnugu, òle/olugu/òlu…

Il existe en dioula deux types de syntagmes complétifs: dans le premier type, les deux éléments se suivent sans connectif, exemples:

Musa bámuso:: la mère de Moussa
sàga kán:: le cou du mouton
à dén:: son enfant (l'enfant de lui)

Dans le deuxième type, les deux éléments sont reliés par une particule de connection "tá": .

Musa tá mùru:: le couteau de Moussa
ntá wári:: mon argent (l'argent de moi)

Dans les deux types, l'ordre est complétant-complété (l'ordre est inverse en français); 
le connectif "tá" est employé lorsque la relation entre les deux termes est d'ordre contractuelle, aliénable (achat, prêt…): 
lorsque la relation est naturelle, inaliénable (par exemple entre moi et mon fils, moi et ma téte, etc…), les deux termes sont conjoints. 
"tá" peut être employé sans le deuxième terme du syntagme complétif; exemples:

à tá lò:: c'est le sien
Musa tá lò:: c'est celui de Moussa
Musa tá bé yàn:: celui de Moussa est ici
ń tá tɛ́:: ce n'est pas le mien

NOTE: sauf dans ce dernier emploi, on trouvera ká et yá comme variantes dialectales de "tá".

-ba et -nin sont des suffixes à valeur respectivement augmentative et diminutive; exemples:

mùru:: couteau
mùruba::: grand couteau
mùrunin::: petit couteau

-ba et -nin peuvent avoir des connotations mélioratives ou péjoratives; exemples:

mùsoba:: femme de mauvaise vie
  
yèn (là, là-bas) est l'un des mots les plus fréquents du dioula. Il peut signifier:

. là-bas
+
Mamadu táara yèn:: Mamadou est parti là-bas

. la, ici, "il y a"
+
bàranda bé yèn:: il y a des bananes
dɔ́ bé yèn:: il y en a
à bé yèn:: il y en a

NOTE: Dans les exemples où "yèn" signifie "il y a", il est prononcé très rapidement et la voyelle est très peu audible: à bé y',   dɔ́ bé y', etc…

[glossary]
== Vocabulaire 

.Vocabulaire {part} 1
[glossary.anki]
tún:: (marque d'inactualité)
tá:: (connectif)
ɲíni:: chercher
kàsi:: pleurer
yé:: voir
túnu:: perdre
wári:: argent

.Vocabulaire {part} 2
[glossary.anki]
kà:: (connectif verbal)
án:: nous, nos…
áw:: vous, vos
-ba:: (augmentatif)
-nin:: (diminutif)
kúnù:: hier
tìgɛ:: couper
séli:: prier, prière, fête
séliba:: Tabaski
sàga:: mouton
kán:: cou
sé:: arriver (à), pouvoir
fò:: saluer

.Vocabulaire {part} 3
[glossary.anki]
kó:: dire, que
dì:: comment
-ie:: (marque pronominale)
fóyi:: rien
báara:: travail
sɔ̀rɔ:: trouver, obtenir
bɔ́:: sortir
yèn:: là, là-bas
