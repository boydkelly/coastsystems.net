a
à
ábada
àle
àni
áwoka
àyiwa
báabu
baara
báara
báarakalan
báarakɛla
báarasɔrɔbaliya
báasi
bádenya
bàli
bálikuya
bálo
bámuso
bàn
bánna
bàtakiw
bàto
be
bé
bèn
bena
béna
bènkánni
bènni
bèpes
bɛ
bɛɛ
bɛɛkanya
bɛn
bɛnkan
bɛnkélenma
bɛnna
bɛrɛbɛrɛ
bìla
bòli
bolo
bólo
bólofɛn
bólokolonya
bólokɔnɔmɔgɔ
bònya
bɔ
bɔra
bɔsi
bɔyɔrɔ
bɔyɔrɔko
cɛ
cɛlasígi
cɛn
cɛnna
cɛnni
cɛw
cogo
cógo
cógoya
dàamu
dáfa
dàgara
dàn
dànmátɛmɛ
dantigɛlikan
dàntigɛlikan
dén
dénbaya
dénbayaw
dénmisɛnya
dénw
dɛmɛ
dɛsɛ
dɛsɛbagatɔ
dí
díina
díinako
díinaw
dògotu
dòn
dɔ
dɔgbɛ
dɔgbɛrɛ
dɔɔnin
dùgulénya
dùgumála
dunya
dúnya
dúnyalatigɛ
dùsu
fá
fàcɛ
fàgamaden
fàgantanden
fàgantánya
fàlen
fána
fànfɛla
fànga
fàngajúgu
fàniya
fàra
fàrafinko
fàragbɛko
fàso
fɛ
fɛɛrɔbɔ
fɛn
fɛrɛ
fìla
fìriyatɔ
fó
fòrobabáaraw
fóyi
fɔ
fɔli
fɔlɔ
fɔra
fúrakɛli’
fúru
fúrukɔfɛden
fúrukɔnɔden
fúruɲɔgɔn
fúrusa
gánsi
gbansan
gbɛlɛya
gbɛlɛyaw
gbɛn
gbɛrɛ
gbɛrɛw
gùndo
gundow
hádamaden
hadamadenw
hádamadenw
hádamadenya
hákɛ
hákɛw
hákɛya
hákɛya’
hákɛyabaw
hákɛyako
hakɛyaw
hákɛyaw
hákili
hákilila
hákilima
hákilitigi
haminankoba
hɔrɔ
hɔrɔn
hɔrɔnya
hɔrɔnyako
húkumu
i
í
jàlaki
jàma
jàmakúlu
jàmalájɛkow
jàmana
jàmanadénya
jàmanaw
jàngáta
jànkáro
jànto
jàte
jàtebáliya
jɛkulu
jɛkúlu’
jɛn
jó
jógoɲumanya
jɔn
jɔnféere
jɔnjɔn
jɔnna
jɔnya
jù
júgu
ká
k’à
kà
kàlafíli
kàlan
kàlanni
kàlifa
kan
kán
kana
kàna
kánma
kánya
kányaɲɔgɔnmaya
kàsi
kàso
kelen
kélen
kélén
kɛ
kɛcogo
kɛɛ
kɛnɛ
kɛnin
kɛra
kɛrɛnkɛrɛnnin
kɛrɛnkɛrɛnninya
kɛtuma
kíima
kiti
kíti
ko
kó
kófɔnin
kójugu
kóɲa
kóɲaw
ków
kɔ
kɔnɔ
kɔnɔmaya
kɔnɔna
kɔrɔ
kɔrɔsi
kɔrɔta
kɔsɛmɛ
kúma
kùma
kúmaɲabila
kùn
kùnko
kùnmasúuli
kùnnafóniw
kùntaga
kúra
l
la
lá
là
ládamu
láfàsa
làganfíya
làganfiyalan
làjába
lájɛn
lákanda
lákandali
làkika
làkíka
lákisi
lámadamuni
lànbe
lánin
lánkɛnɛmaya
làɲíni
lára
lásàbati
lásìgi
last
látanga
látànga
látàngali
làtangali
le
lè
lèebu
links
lò
lónanya
lɔ
lɔn
lɔnbaliya
lɔnko
lɔnkow
lɔɔkɔnin
lɔyɔrɔ
lùjura
má
mà
màgbɛ
màgbɛn
màko
mán
mànton
màntonw
màra
màrabólo
màrada
másalaw
míiriya
min
mín
mina
mína
mìna
mìnaliko
minw
mɔ
mɔgɔ
mɔgɔw
mɔgɔya
mumɛ
mùmɛ
múruti
múso
mùso
mùsow
n
na
ná
n’à
nà
nàfa
nàfasɔrɔ
nàfolotígiya
n’àfɔ
námara
nan
nàna
násɔn
ni
ní
níjuguya
nin
nìn
nìnnugu
nìnugu
nìyɔrɔ
nɔ
nɔgɔya
nɔnɔ
ɲá
ɲà
ɲàli
ɲàmakalako
ɲa’na
ɲánabɔli
ɲánagbɛw
ɲánamaya
ɲàngíli
ɲánimabori
ɲásininin
ɲásuma
ɲɛ
ɲíni
ɲɔgon
ɲɔgɔn
ɲɔgɔnbonya
ɲɔgɔnfaamuya
ɲɔgɔnlajɛrɛba
ɲɔgɔnye
ɲuman
ɲúman
ŋ
ŋàniya
o
ó
ò
òlugu
òlúgu
pólitikiko
references
sàbati
sàbatíli
sàgo
sánfɛkalan
sàra
sáratikan
sàriya
sàriyaba
sàriyaso
sàriyaw
se
sé
sègi
séko
sékow
séli
sèn
sépe
séra
sɛndika
si
sí
sífaya
sìgi
sìgida
sìgidénya
sìgili
síniɲasigi
síra
síra’
siran
síran
sìri
sìrili
síyaw
síyawoloma
sìyɔrɔ
sókɔnɔ
sɔ
sɔn
sɔrɔ
sɔsɔ
sɔsɔli
súgandi
súgandili
súgu
súguya
sùtara
syáman
tá
tága
tágama
tànga
tàngali
táya
te
té
tèle
tériya
tɛ
tɛkinikikálan
tigɛ
tìribinali
tìribináli
tó
tɔ
tɔgɔ
tɔn
tɔnba
tɔnden
tɔɲɔ
tɔɔrɔ
tɔrɔ
tɔw
tùma
ù
wà
wágati
wájibi
wájibiya
wálà
wálasa
wálew
wáleyaw
wálima
wàsa
wólo
wólomali
wóte
ya
yá
yálasa
ye
yé
yèn
yɛrɛ
yɛrɛbakun
yɛrɛmabíla
yɛrɛmahɔrɔnyanin
yìra
yíriwa
yíriwali
yɔrɔ
