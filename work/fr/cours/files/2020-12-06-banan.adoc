= Bana dugu kɔnɔ
:author: Soumahoro
:date: 2020-12-06
:description: "Cours de dioula Bana dugu kɔnɔ"
:sectnums:
:sectnumlevels: 2
:keywords: jula, dyula, dioula, kodiwari, cours, langue, afrique, côte d'ivoire
:category: Julakan
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

== Baro

.Bana dugu kɔnɔ

[horizontal]
Salim::
I ni sɔgɔma!

Binta::
N'se. hɛrɛ sila ?

Salim::
Hɛrɛ dɔrɔn.

Binta::
I ka kɛnɛ wa ?

Salim::
Ɔh-hɔn, n'ka kɛnɛ kosɔbɛ.

Binta::
O ye hɛrɛba ye.
Dɔ di sɔgɔma.

Salim::
Juguman tɛ yen.
N nan na i non fɛ.
An bɛ taa ɲɔgɔnye kɛ dɔtɔrɔso la.

Binta::
Mu le bɛ ye tuguni ?
Dugu ɲamɔgɔw ko ko bana caya la dugu kɔnɔ.

Binta::
Akɔni ! Sumaya, ɲan dimi, furu dimi, ni denmisɛnw ya kɔnɔ bori caya la.

Salim::
Ɔn-hɔn, i ka faamu.
Dɔtɔrɔw bɛna dɔ fɔ an ɲanan.

Binta::
I lɔ dɔɔni.
N'bena n'cɛ wele.

== Vocabulaire

.Vocabulaire
[width="80%",cols="2",frame="topbot",options="none",stripes="even"]
|====
a|
hɛrɛ:: paix
hɛrɛba:: grande paix
ɲɔgɔnye:: réunion
tuguni:: encore
Madu ka ya dereke feere tuguni.
ɲamɔgɔ:: chef
ɲamɔgɔw:: chefs
banan:: maladie
banan tɛ mɔgɔ teri ye.
a|
caya la:: devenu nombreux.
akoni!:: en tout cas!
faamu:: comprendre
faamuli:: compréhension
n ma i ya kuma faamu.
dɔ fɔ:: dire quelque chose. i ka dɔ fɔ wa?
lɔ:: attendre
dɔɔni:: un peu
|====
