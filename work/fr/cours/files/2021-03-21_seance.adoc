= SÉANCE 2021-03-21 
:author: DIARRASSOUBA Youssouf
:date: 2021-03-21
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== QUELQUES FORMES D'EXPRESSIONS AVEC "HAKILI LA"

=== Forme au présent (objet + to + sujet+ hakili la)

* N ka kuman min fɔ i ye (nyana), a to i hakili la (i kɔnɔ)
* N b’a fɛ a bɛ to i hakili la   (je veux que tu gardes en mémoire)
* A ma to a hakili la (a kɔnɔ)
* Mɔgɔkɔrɔbaw ya kuman ka kan ka to denminsɛnw hakili la
* Kalankɔnɔkumanw tɛ to kalanden caman hakili la
* N’i b’a fɛ a bɛ to i hakili la, i ka kan ka segi a kan tuman caman
* A tɔgɔ ma to n hakili la (a tɔgɔ ma to n kɔnɔ)

Variante individuelle (Objet+ to+ sujet + kɔnɔ)

* A ka kuman min fɔ, a to la n kɔnɔ
* A tɔgɔ to la n kɔnɔ
* Zagamɛn tobicogo ma to Coulibali kɔnɔ  (kelly ne se rapelle pas comment faire le zagamen)

=== Forme au passé (objet + to + la+ sujet + hakili la)

* A ka kuman min fɔ n ye (nyana), a bɔla n hakili la
* N b’a fɛ I bɛ haminankow bɛɛ bɔ I hakili la, a ma nyi i ma
* A ma to a hakili la (il n'a pas mémorisé)
* Foyi ma to a hakili la (il ne s'est rapeller de rien)

NOTE: Il faut bien voir les verbes to et bɔ qui marque la différence entre retenir/mémoriser et oublier/renoncer

Formes simples (avec "to")

* A to la so kɔnɔ (il est resté à la maison)
* A to la san pedro (il est resté à san pedro)
* N ya mobili to la Abidjan (Ma voiture est restée à A bidjan)

Formes imagées

* A to la kasara la (il est resté dans l’accident/il n’a pas survecu à l’accident)
* A to la banan na (Il a n’a pas survecu à la maladie/La maladie l’a emporté)
* Madou to la kɛlɛ la (Madou est mort au combat)

Formes simples (avec "bɔ")

* A bɔ la bi (Il est sorti aujourd’hui)
* A bɔ la kɛnɛ ma
* Coulibali bɔ la a ya so kɔnɔ
* Sunguru nin ma bɔ kunu wula fɛ
 
. Formes imagées (avec "bɔ")

* A sen bɔ la kɛlɛ la
* A boro bɔ la cɛnko la
* Musa ka a bolo bɔ kuman caman
* Denminsɛn ka kan ka a da bɔ mɔgɔkɔrɔbaw ya kuman na (ne dois pas participer à la discussion des personnes agées)
* N sen bɔ kuma cama la  

Antonymes (objet + bɔ+ sujet+ hakili la)

* A bɔ i hakili la
* Kunu kumankolonw bɛɛ bɔ i hakili la
* I kana i haminankow mara, o bɔ i hakili la (ne gère pas tes soucies, sort les de ta tête)

* da  = s'exprimer
* sen = se déplacer
* bolo = acquérir

i kana a to i hakili la =  à bɔ i hakili la

kunu kumankolonw bɛɛ bɔla n hakili la (n bɔla n kɔnɔ) 

ne hakili la = n ɲanya
