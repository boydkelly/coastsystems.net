= GBABUGU KƆNƆ
:author: Youssouf DIARRASSOUBA
:date: 2021-09-12
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

////
o lon na = la dernière fois

o yɔrɔ nin bɛɛ = en meme temps en séance tenante

minkɛ = lorsque

a tila la  +ensuite  (elle a fini et ensuite...)
nanfenw tɔ   (reste des condiments)
hakɛ(ya) lɔn
da = jate
kunawari (wari a ta disposition)
connaitre aussi la quantité de leurs marchandises.

kafoli ye ka   (consiste à ajouter les choses ou l'argent ls uns sur les autres)

kafoli ye wari fara ɲɔgɔn kan

si tu ajoutes un sur deux ça fait trois.


pour enlever dans la quantité des choses que tu possèdes.

ka dɔ bɔ o la.  (pour enlever ) ou bien pour enlever dans l'argent que tu possèdes.

warida = somme d'argent
kunawari = l'argent que tu possèdes.

ɲogon da la = mettre ensemble.  k'o buguya pour les faire croître

den kelen kenena   chaque enfant obtiendra 4 mangues.
////

== SƐBƐKƆNƆKUMANW FAAMUYALI 

=== GBABUGU KƆNƆ

Fatu ye muso kisɛ ye.
A furula a sanji looru ye ɲinan ye.
Kabini a tun ka dɔgɔ, a ka tobili dege a bamuso fɛ.
Bi, a bɛ se tobili la kosɔbɛ, a bɛ to tobi, a bɛ basi kɛ, a bɛ zagamɛ tobi.
Lon o lon, Fatu bɛ gba kɛ k’a d’a cɛ n’a denw ma, nka a bɛ sɔsɔ ni sogo ni sisɛfan ni kɔmdetɛri ni makɔrɔnin kɛ k’o feere sinema da la Tɛrɛsivili.

N tun bɛ Fatu ya so o lon na, a taga la lɔgɔ la ka taga sogo san, ka jɛgɛ san, ka nanfɛn caman san ani malo.
A nan na don gbabugu kɔnɔ.

* N ko Fatu i ni lɔgɔ
* A ka n jaabi ko nse.
* Fatu i bena mun tobi bi?

* Eee, bi wa?
N bena tigadɛgɛnan ani malo tobi.
O le ka di n ya denbaya ye.
O yɔrɔ nin bɛɛ, Fatu ka tasuma mɛnɛ.
A ka daga fla sigi walasa a ye teliya.
Daga kelen ye nandaga ye, daga flanan ye malodaga ye.

A sigila ka muru ta ka nanfɛnw tigɛ tigɛ, ka jɛgɛw tigɛ tigɛ.

Daga kelen kɔnɔji wili la, a ka malo ko k’a k’a la k’a datugu.

A ka tulu kɛ daga tɔ kelen kɔnɔ. Tulu gban na minkɛ, a ka jɛgɛ yiran.

A ban na jɛgɛ yiran na, a ka tigadɛgɛ ni nanfɛnw kɛ tulu la ka nan yiran.
A tila la ka nan yiran k’a jidon.

Nan wili la minkɛ, a ka jɛgɛ ni nanfɛnw tɔ k’a la. Yanni lɛri fla cɛ, Fatu ya gba sinna
ka mɔn.

.Traduction
[%collapsible]
====
Fatu est une femmme courageuse (vaillante).
Elle est marié 5 ans cette année.
Depuis qu'elle est peitite, elle a appris à cuisiner auprès de sa mere.
Aujourd'hui, elle arrive à cuisiner beaucoup. Elle prépare le to, fait le couscous, prépare le riz gras.
Chaque jour, Fatu fait la cuisine pour donner à son mari et ses enfants, mais elle fait le haricots avec poulet et pommes de terre et macaroni, pour vendre devant la porte de Treicheville.

J'étais chez Fatu la dernière fois, elle est allé au marché acheter la viande, du poisson, beacoupe de condiments et du riz.
Elle est venue rentrer dans la maison.

* J'ai dit Fatu bon marché
* Elle a répondu nse.
* Fatu qu'est-ce que tu vas préparer? 

Eh, aujourd'hui?
Je vais préparer la sauce arachide avec du riz.
Cela plaît à la famille.
Aussitôt, (en même temps) Fatu a allumé le feu.
Elle a pris deux marmites pour que cela aille vite.
Un marmite pour la sauce, le deuxième pour le riz.

Elle s'est assise, et a pris le couteau pour découper les condiments et le poisson. 

Quand l'eau dans un des marmites a bouillie, elle a pris le riz pour le mettre dedans et l'a fermé. 

Elle a fait huile dans l'autre marmite. Lorsque l'huile a chauffé, elle a frit le poisson. 

Quand le poisson a fini de frire, elle a frit l'arachide et les condiments dans l'huile.

Elle a terminé la friture en ajoutant de l'eau.

Lorsque la sauce a bouilli, elle a ajouté le poisson et les autres condiments.  D'ici deux heurs le repas de Fatu est prêt.  
====

== VOCABULAIRE

.Vocabulaire
[width="100%",cols="3",frame="topbot",opts="none",stripes="even"]
|====
a|
Fatu:: Fatou
a:: elle, ile, le
a:: il, le, la
a:: pron. 3e pers. sing
ani:: et
bamuso:: mère
banna:: finir+acc
basi:: couscous
bi:: aujourd’hui
bɛ:: aux.
bɛɛ:: tout
caman:: beaucoup
cɛ:: entre
cɛ:: mari, époux
daga:: marmite
datugu:: couvrir, fermer
dege:: apprendre
denbaya:: famille
denw:: enfants
di:: bon, agréable
di:: donner
don:: entrer
dɔgɔ:: petit
eee:: hee !
feere:: vendre
fla:: deux
flanan:: deuxième
furula:: marier+acc
fɛ:: chez
gba:: cuisine, repas
gbabugu:: cuisine
gban na:: chauffer+acc
jaabi:: répondre
jidon:: mettre eau
jɛgɛ:: poisson
ka:: aux.
ka:: être
ka:: pour, et
kabini:: depuis
kelen:: une
a|
kisɛ:: hardie
ko:: dire
ko:: laver
kosɔbɛ:: très bien
kɔmdetɛri:: pomme de terre
kɔnɔ:: dans
kɔnɔji:: contenu d’eau
kɛ:: faire
kɛ:: mettre
k’a:: ka (aux.)+a (la)
la:: au
la:: dans
la:: dedans
le:: c’est
lon o lon:: chaque jour
lon:: jour
looru:: cinq
lɔgɔ:: marché
lɛri:: heure
ma:: postposition
makɔrɔnin:: macaroni
malo:: riz
malodaga:: marmite pour le riz
minkɛ:: lorsque
mun:: quoi ?
muru:: couteau
muso:: femme
mɔn:: cuire
mɛnɛ:: allumer
n:: je, moi
na:: dans
na:: marque du futur
nana:: venir+acc
nandaga:: marmite contenant la sauce
nanfɛn:: condiment
nanfɛnw:: condiments, ingrédients
ni:: et
nse:: réponse d’une femme à une salutation
o yɔrɔnin bɛɛ:: sur place, immédiatement
o:: cela, ce
a|
san:: acheter
sanji:: année, eau de pluie
se:: pouvoir
sigi la:: s’asseoir+acc
sigi:: poser, mettre
sinna:: aller droit+acc
sisɛfan:: œuf
so:: maison
sogo:: viande
sɔsɔ:: haricot
ta:: connectif
ta:: prendre
taala:: partir+acc
tasuma:: feu
teliya:: être rapide, aller vite
tigadɛgɛ:: pâte d’arachide
tigadɛgɛnan:: sauce arachide
tigɛ tigɛ:: couper en plusieurs morceaux, découper
tigɛ:: couper
tilanna:: finir+acc
to:: toh, gâteau
tobi:: faire cuire, préparer
tobili:: cuisine, préparation
tulu:: huile
tun:: marque imparfait
tɔ:: reste, l’autre
tɔw:: autres
wa:: est-ce que ?
walasa:: afin que
wili la:: bouillir+acc
ya:: connectif
ya:: de
yanni:: avant
ye:: être
ye:: postposition
ye:: pour
yiran:: faire frire
yɔrɔ nin:: lieu
zaamɛ:: riz gras
ɲinan:: année en cours, cette année
|====

== EXERCICES

=== NYININGALI NINUGU JABI

. Fatu furu la a sanji joli bɛ bi?
. Muna fatu ye muso kisɛ ye?
. Fatu ka tobili dege wagati jumɛn
. Fatu ka tobili dege jɔn fɛ?
. Muna Fatu bɛ daga fila sigi tuman caman ka gba kɛ?
. Fatu bɛ mun feere Tɛrɛsivili sinema da la?
. Domuni jumɛn ka di Fatu ya denbaya ye?
. Ile nyana, Fatu ya gba bɛ mɛn ta kan wa?
. Ile bɛ domuni jumɛn kɛ kɛnɛ ma tuman caman?
. Cɛgbana caman bɛ se gba la, nka o ya gba ma di katimi musow ta kan. Tunyan wa wuya?


== POINT DE GRAMMAIRE

=== Jatebɔ (Le calcul)

Jate bɛ kɛ walima jate bɛ bɔ walasa ka se ka fɛnw hakɛ lɔn ka wari fana da lɔn.

Exemple: Jagokɛlaw bɛ jatebɔ tuman bɛɛ walasa ka o kunawari hakɛya lɔn walima k’o ya doni hakɛya fana lɔn

==== Kafoli (addition)

kafoli ye ka fɛnw walima wari fara ɲɔgɔn kan.

Exemple: N’i ka kelen fara fila kan o bɛ bɛn saba ma

==== Dɔbɔli (soustraction)

Dɔbɔli ye fɛn hakɛ min b’i bolo ka dɔ bɔ o la walima ka dɔ bɔ warida la.

Exemple: N’i ka saba bɔ looru la, a bɛ to fila

==== Sigiyɔrɔmanni (multiplication)

Sigiyɔrɔmanni ye ka fenw sigi sigi ɲɔgɔ n da la walasa k’o buguya

Exemple: mangoro den looru sigiyɔrɔman looru, o ye mangoro den mugan ni looru ye.

==== Tlanni (division)

Tlanni ye ka fenw walima wari tlan (nin)

Exemple: N’i ka mangoro den mugan tlan den looru cɛ, den kelen kelenna bɛɛ bɛ mangoro den maani sɔrɔ.
