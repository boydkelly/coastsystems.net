= SEANCE 2021-10-31
:author: Youssouf DIARRASSOUBA
:date: 2021-10-31
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== REVISION DE POINT DE GRAMMAIRE

CONSIGNES : Après lecture des exemples, construire pour chaque point évoqué dix (10) autres exemples

=== POINT 1 : Le suffixe "man" signifie : un état ou qui a la quaité de...

cɛman:: le mâle, (qui a la qualité de mâle)
musoman:: la femelle
nɛgɛman:: qui est en métal
nɔgɔman:: qui est facile
nɔnɔman:: qui contient du lait
juguman:: qui est méchant
fɛngɛman:: qui est léger
gbiliman:: qui est lourd

=== POINT 2 : "Ala ma" est une expression utilisée pour introduire un vœu, suivi de la forme conjuguée au passé du verbe transitif ou intransitif

"Ala ma an kodiya la ŋɔgɔn ye".:: Que Dieu fasse que nous nous aimons.
"Ala m’an kisi la"::  Que Dieu nous sauve

=== POINT 3 : "min " est un pronom relatif. On l’utilise comme en français.

"N bɛ muso min lɔn kɔsɔbɛ, o ye karamɔgɔ ye kalansoba la".:: La femme que je connais très bien, elle est enseignante à l’université.
" taga a fɛ, kibaroya min b’a fɛ a bɛna o fɔ i ye".:: Va chez elle, elle te dira la nouvelle qu’elle a.

== SUJET DE DEBAT (AN YA BI BARO)

=== Dɔrɔgimin (La consommation de la drogue)

. Dɔrɔgi ye mun ye ?
. Muna mɔgɔ dɔw bɛ dɔrɔgi min ?
. Ile degi la ka dɔrɔgi nɛnɛn wa ?
. Dɔrɔgimin bɛ mun lase adamaden ma?
. Nafa bɛ dɔrɔgimin na wa?
. Ile be se ka ladilikan jumɛn fɔ dɔrɔgiminaw ye?
. Jamana bɛ sɔrɔ dununya kɔnɔ dɔrɔgimin daganin bɛ min kɔnɔ wa?
. Ile nyana kafe farimanw ye dɔrɔgi sugu dɔ ye wa?
. Ile bɛ se ka jɛn ni dɔrɔgiminaw ye wa? (muna?)
. A ka nyi jɛkulu dɔw bɛ dɔrɔgiminaw dɛmɛ wa ?
. Dɔrɔgifeerelaw ka ca dugubaw la walima dugudeninw?
. I dege (deli) la ka dɔrɔgi dɔ tɔgɔ mɛn wa?
. Ile hakili la, dɔrɔgiminaw ka ca jamankulu jumɛn na?
. Dɔw ko ko dɔrɔgi hakɛya fitinin bɛ sɔrɔ fura dɔw kɔnɔ (la). Tunyan wa wuya ?
