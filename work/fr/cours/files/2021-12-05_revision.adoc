= REVISION 2021-12-05
:author: Youssouf DIARRASSOUBA
:date: 2021-12-05
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== RÉVISION GÉNÉRALE

=== COMMENT SE PRÉSENTER EN DIOULA ?

. Je m’appelle Coulibali.
. Mon nom de famille est BOYD
. Je suis originaire du Canada.
. Je suis le fils de.…………………et de…………………………………………………………………
. Mes parents vivent au Canada.
. Moi je vis en Côte d’Ivoire
. Je suis le frère de……..
. Il est avec moi ici à Bouaké
. Mon lieu de travail est situé à/au………………………………….
. Je n’ai pas de grand. J’ai une petite sœur. Elle s’appelle………………………………
. Elle est gentille et très belle.
. J’apprends le dioula depuis le mois de………………………………………………………
. Mon professeur s’appelle Youssouf DIARRASSOUBA
. Il est n’est pas gentil. On apprend la langue Dioula les dimanches


.Réponses
[%collapsible]
====
. n tɔgɔ ko Coulibali.
. n jamu ko Boyd.
. n be bɔ Canada.
. n ye thomas ni jacqueline dencɛ ye.
. n balimaw siginin be Canada.
. ne siginin be kɔdiwari.
. n ye yousouf badencɛ ye.
. a be ni ne ye bouaké.
. n ya baarakɛ yɔɔrɔ be yɔɔrɔ bɛɛ.
. kɔrɔ te yen n na. Dɔgɔmuso kelen be n bolo. A tɔgɔ ye Awa.
. a ka ɲy, a cɛ ka ɲi fana.
. kabini Zanviye kalo, ne be julakan kalan.
. N ya kalanfa tɔgɔ ko Yousouf DIARRASSOUBA.
. a ma ɲi. An be julakan degi, karilon o karilon.
====

== LES MOIS DE L’ANNÉE

[width="100%",cols="4",frame="none",options="none",stripes="even"]
|===
|zanviye kalo|avirili kalo|zuwe kalo|ɔkutɔburu kalo
|feviriye kalo|mɛn kalo|uti kalo|novanburu kalo
|marisi kalo|zwɛn kalo|sɛputanburu kalo|desanburu kalo
|===

== ÉTABLIR LE CONTACT AVEC UN INCONNU

. Bonjour frère
. Bonjour frère
. Comment vas-tu ?
. Je vais bien et toi ?
. Je vais bien
. Comment tu t’appelles ?
. Je m’appelle Youssouf et toi ?
. Moi je m’appelle Coulibali. C’est quoi ton nom de famille ?
. Mon nom de famille est DIARRA et toi ?
. Moi mon nom de famille est BOYD
. Tu es originaire de quel pays. Tu es de quel nationalité ?
. Je suis Canadien et toi ?
. Moi je suis ivoirien. Je vis à Daloa. Je viens très souvent à Abidjan en vacances
. Que cherches-tu ici ?
. Mon frère a une boutique dans ce quartier. Mais je ne sais pas où elle est située
. C’est quoi le nom de ton frère ?
. Amidou DIARRA
. Il est grand ?
. Oui, il est grand
. Sa boutique est derrière cette église
. Merci beaucoup frère
. De rien
. A bientôt
. A bientôt !

.Réponses
[%collapsible]
====
. i ni sɔgɔma n balimancɛ
. i ni sɔgɔma n balimancɛ
. i ka kene wa ?
. N ka kene, ile do ?
. n ka ka kene
. i tɔgɔ be di ?
. n tɔgɔ be Youssouf, ile do ?
. ne tɔgɔ be Coulibali. i jamu?
. n jamu be DIARRA. ile do ?
. n jamu ye BOYD.
. i be mun jamana be bɔ ? I ye mun jamana fasoden ye ?
. n be bɔ canada.
. n ye canada fasoden ye. Ile do ?
. ne ye jamana kodiwari fasoden y. n siginin be dalao.  je be to ka nan abidjan ka vakansi kɛ, tuma bɛɛ.
. i be mu ɲini yan ?
. boutiki be n balimancɛ bolo kin nin kɔnɔ.  Nga, n m'a yɔrɔ lon.
. i balimacɛ tɔgɔ be di ?
. a tɔgɔ dafanin be Amidou DIARRA
. a ka jan wa ?
. a ya butiki be eglisi kɔfe
. i ni cɛ kosɔbe balimacɛ
. folikun tɛ
. an be kɔfɛ
. an be kɔfɛ
====

quartier:: kin 
église:: legilisi 
beaucoup:: kosɔbɛ 
derrière:: kɔ fɛ

== EXPRIMER DES BESOINS, DES ENVIES, DES VOLONTES

Les structures pour exprimer ses besoins/envies/volontés sont :

Sujet + b’a fɛ ka…, 

Sujet + be…..fɛ ; …..lɔgɔ be + sujet + la/na

Lire les phrases ci-dessous et propose à ton tour cinq phrases du même type

. N b’a fɛ malo ni tennan domun bi
. Mariam b’a fɛ ka cɛkɛ ni jɛgɛ san sini
. Michèle t’a fɛ ka kalan kɛ sibirilon

. Ne denmuso t’a fɛ ka sunɔgɔ
. Cɛkɛdomunlɔgɔ be ne la
. Mobilisanlɔgɔ be n terimuso la
. Jiminlɔgɔ be kamelen nin na
. Baarakɛlɔgɔ te n tericɛ la
. Ne be coca cola fɛ
. Michèle be zagamɛn fɛ
. Ne tɛ nyɔmi n nɔnɔ fɛ
. N muso be mɔntɔrɔ kura fɛ

n b'a fɛ mobili gbɛman ni mobili finman san.

== ÉVOQUER DES POSSIBILITÉS

La structure pour exprimer les possibilités est :

Sujet + be se (te se) ka + objet + verbe

=== Lire les phrases ci-dessous et propose à ton tour cinq phrases du même type

. N be se ka malo ni tennan tobi
. Mariam be se ka baara kɛ su fɛ
. Ile be se ka mɔntɔrɔ lalaga wa?
. Jɔn be se ka n dɛmɛn?
. Coulibali te se ka nɛgɛso boli
. Ne te se ka nan su nin na
. Adamaden te se ka saya bali
. Cɛw te se ka kɔnɔn ta

== NOUVEAUX POINTS : ÉVOQUER LES DEVOIRS

La structure pour exprimer les possibilités est :

Sujet + ka (ma) kan ka + objet + verbe

. Cɛw ka kan ka o musow ladiya
. Denw ka kan ka o bangebagaw bonya
. Michèle ka kan ka a bamuso wele bi
. Adamadenw ka kan ka Allah bato
. Farafin na, denmusow ka kan ka suman tobi dege

. Muso ma kan ka a cɛ dɔgɔya
. Mɔgɔ ma kan ka a mɔgɔɲɔgɔn faga
. Hɔrɔn ma kan ka nin kɛ
. Silaman ma kan ka lɛsogo domun
. Dennyuman ma kan ka sonyali kɛ

ladiya:: prendre soin, combler
bonya:: respecter
sonyali:: vol
wele:: appeler
bato:: louer, prier
farafin na:: en afrique
suman:: nourriture
tobi:: preparer
dege:: apprendre
dɔgɔya:: manquer de respect à…
mɔgɔɲɔgɔn:: semblable
faga:: tuer
hɔrɔn:: noble
nin:: cela, ceci
silamanw:: musulmans
lɛsogo:: viande de porc
dennyuman:: enfant sage)
