= SÉANCE 16 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-06-30
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 16 : KALANSEN TAN NI WƆƆRƆNAN
=== BARO SABANAN/THEME 3 :  et son ami JAMES au restaurant

.Contexte de communication
[abstract]
Coulibali bɛ terela kɛ Aïcha ya domunifeereyɔɔrɔ lɔgɔkun laban caman na. Bi sibiri,
a taga la domuni kɛ ni a lɔnbagaw dɔ ye Aïcha fɛ.
Traduction : Coulibali prend son déjeuner dans le restaurant de Aicha très souvent
les week-end. Aujourd’hui samedi, il est allé manger chez Aïcha avec une de ses connaissances (proches).

****
* I ni tere AÏCHA
* Nse, i ni tere. Somɔgɔw do ?
* O ka kɛnɛ kosɔbɛ
* I bɛ nin jɔn ye bi ?
* N bɛ ni n tericɛ James ye bi. A b’a fɛ ka i ya gba nɛnɛ ka filɛ
* Ayiwa, a danse. A bena mun domun bi ?
* Ne bena maro ni tigadɛgɛnan domun
* I tericɛ do ? Ale bɛ mun fɛ ?
* Ne ni JAMES bena domuni kelen domun
* Foronto ka di i tericɛ ye wa ?
* Foronto ka di James ye katimi ne yɛrɛ kan
* Ola, n bena foronto caya a ya zagamɛ la. A bena a domun ni sogo ye walima jɛgɛ ?
* Ne bena n ta domun ni sogo ye nka n tericɛ ta kɛ jɛgɛ ye.
* N k’a mɛn. Ala ye a suman a kɔnɔ (Ala ye a ni hɛrɛ bɛn) !
* Amina. N bɛ nan kɔfɛ
****

Quelques minutes plus tard (wagati/miniti dɔɔni timinin kɔfɛ)

****
* , domuni diya la wa ?
* A diya la kojugu. N tericɛ sawa la (kɔntan na) katimi.
* A bena mun min ka domuni lajigi kanyan ?
* An bena lemuruji min
* Lemuruji ma gilase. Jabibiji gilasenin fana be yi dɛ ?
* Basi tɛ, an bena lemuruji gilasentan min ten
* Lemuruji filɛ nin ye. A kana bi ta sara
* Muna, i bena ben dɛ AÏCHA. Jago tɛ kɛ ten oh !
* N b’o kalaman, nka ile ye n ya sanikɛla bɛrɛ (kiliyan bɛrɛ) ye. I bɛ yan lɔgɔkun laban bɛɛ. N ka kan ka i ladiya
* I n ice AÏCHA
* Folikun tɛ. A bena segi yan lon jumɛn ?
* Sibiri nanta, ni Ala sɔn na
* Ayiwa, an bɛ (k’an bɛ) sibiri nanta
* An bɛ (k’an bɛ) AÏCHA.
****

=== BAARO NAANINAN/THEME 4 :  au kiosque à café du quartier

[abstract]
===
Contexte de communication:: Bɛ kafe min lon bɛɛ sɔgɔma Birahima ya kafefeereyɔɔrɔ (cɔsi) la. 
A bɛ to ka baaro kɛ ni cɔsi mɔgɔ tɔw ye ninsɔndiya la.

Traduction::  prend son café tous les matins au kiosque à café de Birahima.
Il a pour habitude de causer (discuter) avec toutes les autres personnes du kiosque dans la bonne humeur (ambiance).
===

* A ni sɔgɔma
* N’ba, i ni sɔgɔma  ?
* Hɛrɛ sila ?
* Hɛrɛ dɔrɔn
* A sawanin (kɔntannin) bɛ bi dɛ? mun ko bɛ yi?
* Foyi tɛ yi. Ni Ala ka si ni kɛnɛya d’i ma, i ka kan k’a konyuman lɔn
* O ye can (tunyan) ye. Can na, a ya ninsɔndiya le bɛ ne lase yan lon bɛɛ
* O dɔrɔn tɛ dɛ. N ya kafe duman le fana bɛ i lase yan
* I ma wuya fɔ. Ayiwa, i ya kafe duman dɔ di n ma
* Ka sukaro caya bi ta la wa ?
* Ɔn-ɔn, n tɛ sukaro caman fɛ tugun. N ya dɔtɔrɔcɛ ko sukaro caman ma nyi adamaden ma
* Muna, sukaro bɛ banan jumɛn lase adamaden ma?
* A bɛ sukarobanan ni banan caman gbɛrɛ di mɔgɔ ma
* Ne tun tɛ o kalaman. Bi kɔfɛ, ne bena dɔ bɔ n ya sukaro hakɛya la
* N’i ko kɛ kɔni, i ya banan minsɛninw bena dɔgɔya
* Heee , dɔtɔrɔya dabila, i ya kafe min.
