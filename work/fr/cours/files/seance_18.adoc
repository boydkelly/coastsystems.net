= SÉANCE 18 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-07-10
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 18 : KALANSEN TAN NI SEEGI 

=== BAARO WƆƆRƆNAN/THEME 6 : Le rôle de la femme chez les mandingues

.Contexte de communication
[abstract]
N’a tericɛ ya baro be boli muso lɔyɔrɔ le kan mandenkaw fɛ. Muso ni cɛ baarakɛcogo n’o ya baarakɛtaw ye kelen ye wa ?
Coulibali ɲana, muso ni cɛ be se ka baara kelen kɛ.
Nka Youssouf hakili la, muso ni cɛ ma kan ka baara kelen kɛ sabu o fila fanga tɛ kelen ye.

****
* I ni sɔgɔma
- N’ba YOUSSOUF, i ni sɔgɔma. Sɔmɔgɔw do?
* O ka kɛnɛ kosɔbɛ. i bɛ mun kɛ la bon kɔnɔ ?
- Sigi ka n kɔnɔn. N bena bɔ sisan sisan
* N b’i kɔnɔn na kɛnɛma yan
- I danse n tericɛ. Hakɛ to, n tun bɛ terela le tobi la
* Eeeeh, ile fana. Cɛ ma kan ka gba kɛ. O ye musobara le ye
- Ile ma foyi faamu duniɲa tagabolow la. Ile ma a lɔn ko cɛw le bɛ gba kɛ lotɛlibonw ni domunikɛyɔrɔbaw la wa ?
* Cɛ minw bɛ gba kɛ lotɛlibonw walima domunikɛyɔrɔbaw la, o tɛ cɛ bɛrɛbɛrɛ ye.
N’o tɛ anugu mandenkaw fɛ, cɛ tɛ gba kɛ abada. Cɛ ya baarakɛ dɔrɔn ka domuni ni lu musakaw ɲini k'a ya denbaya ladiya.
- O ye tiɲɛ ye nka bi dunuɲa gwɛlɛya la.
A kɛla jagboya ye muso bɛ a cɛ dɛmɛ ka lɔ lu kunkoyaw kɔrɔ
* A bɛ ile ɲana ko an bɛ Faransi walima Angileteri wa? Farafina, muso lɔyɔrɔ ye furu ni dencɛ dɔrɔn ne ye
- Ile ni fɔlɔfɔlɔmɔgɔw hakilinan ye kelen ye. Bi, muso ni cɛ bɛ ɲɔgɔn dɛmɛ sokɔnɔbaaraw la. Muso bɛ se ka a cɛ dɛmɛ ka bon sara, walima ka nansɔng dafa.
* O ye fagobagatɔkan ne ye. Mɔgɔ o mɔgɔ bɛ a muso ya wari domun, o ma kan ka kuman cɛ tɔw cɛman.
- A ka ɲi ten. A bɛ i n’a fɔ n bɛ n yɛrɛ sɛgɛ la gbansan. Ile hakilinan te se ka yɛlɛman
* Ne kelen tɛ dɛ ! Ile fana hakilinan ka kan ka yɛlɛma. An bɛ farafina nka a bɛ ile ɲana ko an bɛ faragbɛla le.
****

=== BARO WORONFILANAN :  A L’HOPITAL

.Contexte de communication
[abstract]
Ma kɛnɛ bi sɔgɔma. A taga la dɔtɔrɔso la ka taga a yɛrɛ furakɛ.
A ka a ya dimiw kun fɔ dɔtɔrɔcɛ nyana. Dɔtɔrɔcɛ ko ko sumaya le b’a la.
A ka furasansɛbɛ di Coulibali ma janko a bɛ furaw san ka a ya bana furakɛ.

****
* N’ba i ni sɔgɔma. Dɔ di sɔgɔma
* Dɔtɔrɔ, n fari ma di n na/n ma kɛnɛ
* I yɔrɔ jumɛn b’i dimi na?
* N kun ne bɛ n dimini kabini kunu. N fɔnɔn na fana ka tere ban
* Sigi n bena i filɛ (peu courant: n bena I fari sɛgɛsɛgɛ)
****

Quelques minutes après

****
* Sumaya le b’i la n badenmancɛ
* N ka sumaya fila ta kunu, a bɛ sɔrɔ bana gbɛrɛ le bɛ n na
* I ka fila jumɛn ta ?
* Farafinfila. Filaburu kunaman minw jakwajo furakɛ n ka ole ta
* Ile ma lɔn ko farafinfuraw ma nyi wa ? O be se ka banan gbɛrɛ lase i ma.
N bena tubabufura dama dama sɛbɛ ka a d’i ma. I ka kan k’o san furafereyɔrɔ (farimasini) la
* N ka mɛn. Bi kɔfɛ, n tɛna farafinfura ta tugun
* A la ye nɔgɔya kɛ. Ka tɔrɔ dɔgɔya
* Amina. I ni ce kosɔbɛ
****

