= SÉANCE 21 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-08-15
:page-noindex: 
:description: Explique ses mésaventures à ses amis de Bouaké
:sectnums:
:sectnumlevels: 2
:icon: font
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 21 : KALANSEN MUGAN NI KELENNAN

=== BARO TANNAN / THEME 11 :

[abstract]
.Contexte de communication
Coulibali explique ses mésaventures à ses amis de Bouaké

Coulibali kalo kelen le bɛ bi Abidjan. A ka Abidjan gundow ni a tagabolow faamun dɔɔni.
A ka a teriw wele Bouaké ka a ya Abidjansigi ɲanyira o la.
Youssouf ye a terigbɛlɛn dɔ ye.
A ka ale le wele fɔlɔ.

* I ni sɔgɔma Youssouf
** N’ba n tericɛ, kow bɛ di ?

* Heee n teri, koow ma nɔgɔ dɛ ?
** Cogo di ?

* Abidjansigi ma nɔgɔ few! Kabini n nan na, ne ni lagafiya faran na.
** Muna ?

* Fenw bɛɛ sɔngɔn ka ca. I ko bon o, faniw o, nanfenw o, o bɛɛ sɔngɔ ka ca.
** N teri, i ka kan ka sɔn nin bɛɛ ma. Abidjan ye duguba ye. Warisɔrɔ ka di nka I be wari sɔrɔ cogo min, I b’a domun ten ne.

* O ka can fɔ. N sibon dɔrɔn bɛ sara waga mugan ni looru.
** Aah o ma nɔgɔ. Nka i ya baara do? I bɛ nafa sɔrɔ la wa?

* Dɔɔni dɔɔni. A bena bɛn ni Ala sɔn na.
** Ala ye kow nɔgɔya i bolo.

* Amina. N bena san kelen kɛ ka a filɛ, ni kow ma ɲanabɔ, n bena kɔsegi Bouake.
** O fana ye hakilinan nyuman ye.

=== EXERCICE DE TRADUCTION

. L’homme qui m’enseigne le dioula s’appelle Youssouf.
. La femme qui est venue hier est Ambassadeur.
. Le monsieur qui a la voiture blanche est chez Coulibali ce matin.
. Tu dois faire un travail qui te permet de nourrir ta famille.
. La boutique qui est derrière la mosquée appartient à Karim.
. La maison dans laquelle il vit est petite.
. Le bureau dans lequel il travaille n’est pas grand.
. La question à laquelle je dois répondre n’est pas facile.
. L’homme à qui j’ai parlé lundi soir est anglais.
. La voiture dans laquelle il est arrivée est jolie.

.Réponses (à corriger!!!)
[%collapsible]
====
. mɔgɔ min be n kalan julakan. A tɔgɔ ko Youssouf.
. muso min nan na kunu, a ye lasigiden ye.
. mɔgɔ min be mobili gbɛman bolo be Coulibali fɛ bi sɔgɔma.
. i ka kan ka baara kɛ min be i ya denbaya balo.
. butiki min be misiri kɔfe be karim bolo.
. a be bon min kɔnɔ. a ka dɔgɔ.
. a be baara kɛ buro min kɔnɔ. a ma bon.
. n ka kan ka ɲininga min jaabi a ma nɔgɔ.
. mɔgɔ min n ka kuma kunu su fɛ be anglekan fɔ.
. kuma na kɛ ni fɛ tɛnɛlon.
. a nan na mobili min kɔnɔ. a cɛ ka ɲi.
====

i ka kan ka kuma fɔ, kuma min bɛ kele ban.:: 
Les paroles que tu dites doivent arrêter le conflit.
