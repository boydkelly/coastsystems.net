= SÉANCE 24 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-08-25
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 24 : KALANSEN MUGAN NI NAANINAN

=== RUBRIQUE DIALOGUE

* Baro tan ni sabanan : Coulibali n’a ya denbaya ka o ya yaala damina

[abstract]
Coulibali n’a ya denbaya bena o ya yaala damina Bouake lɔgɔfɛba la kasɔrɔ ka taga maki la.
O bena o ya tagaman bɛ kɛ o sen na.
Ola, o bena fen caman ye walima ka dugu kɔkankow faamu.

* En quittant l’hotel : O bɔtɔ lotɛlibon na

* Alexia, Markus, an be se ka taga sisan wa? Ne labɛnin bɛ kaban ?
* Annugu fana labɛnin bɛ. An ka kan ka bɔ ni minan jumɛn ye?
* Foyi, an bena sira bɛɛ kɛ sen na. Ni an ka doni ta, a bena an sɛgɛ
* An bena an ya negemanfenw bila mini ola?
* Manan ni saki bɛ feere dugu kɔnɔ
* An ka a faamu

* Arrivés au grand marché de Bouaké (O senin Bouaké lɔgɔfɛba la)

* Muna mɔgɔw b’an filɛ la ten ?
* Markus, ile fana, annugu tɛ yanmɔgɔ ye, an fari n’o ta fana te kelen ye. Jagboya le o bɛ an filɛ
* I jo le, n tun nyinan na pew ko an ye lonan/lonntan le ye
* Ile ko ten wa ? I bena to yan, ne ni i bamuso bena segi canada. I kɛla Kodiwarika ye sisan
* Ɔn-ɔn, n m’o fɔ dêh. Bɛɛ bɛ nyinan
* Ayiwa basi tɛ. An bena an ya sani damina nanfenlɔgɔ la
* Coulibali, a filɛ, gɔyɔ bɛ sɔrɔ yan fana
* Gɔyɔ dɔrɔn tɛ dɛ. Tamati, karɔti ni jaba ka ca yan
* Nka nin ye mun ye (sunbara, une épice africaine) ?
* Nin t’an fɛ Canada. An bena feerekɛla nyininga. I ni sɔgɔma n badenmanmuso. Hakɛto, nin ye mun ye?
* A danse, nin ye sunbara ye. A ka nyi lafiri la
* Lafiri ye mun ye ?
* Lafiri ye an ya farafin zagamɛn ye. A ka di mandenkaw ye kosɔbɛ
* Ayiwa, an bena dɔɔni san ka a nɛnɛn ka filɛ. An be se ka a kɛ nan na wa?
* Ɔn-hɔn, a ka nyi nan caman na. A bɛ se ka a kɛ wosoburunan na, gbanmugunan na waliman tigadɛgɛnan fana na
* Wosoburunan, gbanmugunan ni tigadɛgɛnan? Annugu ma degi ka o domun fɔlɔ
* A ka kan ka nantɔgɔw domun. O ka di kojugu
* An ka a mɛn. Subara siri kelen ye Joli ye ?
* A sɔngɔ ma ca. Mugan dɔrɔn ne
* An bena siri fila ta ola
* Mɔn/ɔn, Ala ye tere nɔgɔya
* Amina

La suite dans la prochaine leçon (Coulibali au maquis)

=== LA FORMULE GRAMMATICALE DU JOUR

Précisions : Les verbes discontinus et le positionnement de l’objet lors de leur emploi.
« L’objet direct » est généralement placé avant le verbe, contrairement à « l’objet indirect » qui apparait entre le premier segment et le second segment des verbes discontinues (verbes pronominaux)

* Moussa ka kow nyanyira a badencɛ la/ Moussa ka kow walawala a badence ye
* A ka wari caman di a dencɛ man
* A facɛ kuman na a dencɛ fɛ ka dɛsɛ
* A ka saga feere cɛ nin ma
* I kana jigi à la, a seko tɛ !
* A yɛlɛ la so kan
* Birahima ka sogo tlan/nin mɔgɔw la

=== DIVERS

Discussions autour des éventuelles préoccupations de l’apprenant à propos des précédents cours ou de situations de communication qu’il souhaite mieux comprendre.
Toutes les questions sont les bienvenues.

