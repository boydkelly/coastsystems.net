= SÉANCE 28 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-09-26
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LECON 28 : KALANSEN MUGAN NI SEEGINAN

=== BARO TAN NI SEEGINAN : TABALITIGI

DAOUDA ye tabalitigi ye.
Lon bɛɛ, a bɛ feere damina kabini sɔgɔma fɔ su fɛ. A t’a yɛrɛ lafɔnyɔ.
A bɛ sɛgɛ kosɔbɛ k’a masɔrɔ a bɛ fɛn caman feere.
Sukaro b’a fɛ, kɔgɔ b’a fɛ, bɔnbɔn fana b’a fɛ.
N’i ko tulu o, safuna o, i bɛ o bɛɛ sɔrɔ DAOUDA fɛ.
Hali tiga kɔgɔman b’a fɛ.
Dakosafuna ani bɔrɔsi i b’o sɔrɔ a fɛ.
A ya minanw sɔngɔ ka nɔgɔ.
Lɛtɛrɛ foroko ni fɛnmisɛnnin wɛrɛ caman b’a fɛ.
A ya tabali ka bon.
Mɔgɔ caman bɛ sanni kɛ DAOUDA fɛ..
Barokɛbagaw tɛ ban DAOUDA kɔrɔ.

DAOUDA tɛ kɔdiwarika ye.
A bɛ bɔ Mali la.
A n’a ya denbaya nan na Abidjan a bɛ sanji looru bɔ bi.
O bɛ to ka taga Mali la seliba la sinyɛn caman.

DAOUDA muso bɛ nanfew feere Adjamé Lɔgɔfɛ la.
A tɔgɔ ye ALIMAN.
A tɛ kan gbɛrɛ fɔ ni Bamanankan tɛ.
A cɛ bɛ to ka a degi tubabukan na dɔɔni dɔɔni.

.VOCABULAIRE
[width="100%",cols="3"]
|====
a|
Dawuda:: Daouda                              
a:: il, lui                                  
ani:: et                                        
baro:: causerie, dialogue_+kɛ:: faire+bagaw     
barokɛbagaw:: causeurs                          
bon:: grande, grosse                            
bɔrɔsi:: brosse                                 
bɛ:: aux. présent                            
bɛɛ:: tous                                   
bɛ…fɛ:: avoir                                
caman:: beaucoup                             
caman:: beaucoup                                
cɛgbana:: celibataire
dako safuna:: pâte dentifrice                   
damina:: commencer                           
feere:: vente, vendre                               
fɔ:: jusque                                     
a|
fɛ:: avoir                                   
fɛ:: chez
fɛ:: postposition                               
fɛn:: chose                                  
gbɛlɛ:: dur, cher                               
hali:: même                                  
i:: tu                                       
ka:: être                                       
kabini:: depuis                              
kalansen:: leçon                             
ko:: dire, demander                          
kosɔbɛ:: très bien                              
kɔgɔman:: salée                              
kɔrɔ:: auprès de                                
k’a masɔrɔ:: parce que                          
lafɔnyɔ:: reposer                               
loorunan:: cinquième                         
lɛtɛrɛforoko:: enveloppe                        
ma:: nég                                        
minanw:: marchandises                        
a|
misɛnnin:: petite                               
mɔgɔ:: gens                                     
ni:: si, et                                         
o:: ça emphase                                  
sanni:: achat                                   
seegi:: huit                                 
su:: nuit                                       
sukaro:: sucre                               
sɔgɔma:: matin                               
sɔngɔ:: prix                                    
sɔrɔ:: trouver                               
sɛgɛ:: fatiguer                                 
tabali:: table                                  
tabalitigi:: tablier, vendeur à l’étalage    
tiga:: arachide                              
tulu/turu:: huile                            
tɛ:: nég. Présent                               
wɛrɛ:: autre                                    
ye:: postposition                            
yɛrɛ:: même                                     
|====

== Nyiningali ninugu jabi

. DAOUDA bɛ feeere kɛ lon bɛɛ wa?
. Daouda bɛ tubabukan mɛn wa? ?
. Mun bɛ sɔrɔ DAOUDA fɛ?
. DAOUDA muso bɛ kan jumɛn fɔ?
. Kɔdiwari ye DAOUDA ya worojamanan ye wa?
. DAOUDA n’a ya denbaya bɛ to ka taga Mali la tuman jumɛn?
. DAOUDA ye mun ye?
. Muna mɔgɔ caman bɛ sanni kɛ DAOUDA fɛ?
. DAOUDA n’a ya denbaya nan na Abidjan a bɛ sanji joli bɔ bi?
. DAOUDA bɛ a yɛrɛ lafɔɲɔ lon jumɛn?
. DAOUDA bɛ a yɛrɛ lafɔɲɔ lon jumɛn?

== Segi kumankolo ninugu kan ka sɔrɔ ka o kɔrɔ fɔ an ye julakan na
. Baarakɛ ka di ni baarakɛbaliya ye.
. Wari ma di ni adamadenya ye.
. Nɔgɔya ka di bɛɛ ye ni gbɛlɛya ye.
. Flabuluna ka di ni malo ye
. Sumali nyuman ka ɲin ni sumalijugu ye.
. Sigiyɔrɔ saniyanin ka nyi ni sigiyɔrɔ nɔgɔnin ye.
. Kuman nyuman fɔ ka fisa ni kuman jugu fɔ ye.

* Repetition
* Substitution
* transformation

