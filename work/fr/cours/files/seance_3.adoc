= SÉANCE 3 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-05-05
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 3 : KALANSEN SABANAN
--
L’OBJECTIF DE CETTE LECON EST DE PARVENIR A MAITRISER LES EMPLOIS CONTEXTUELS DU VERBE "ÊTRE" EN DIOULA VEHICULAIRE DE COTE D’IVOIRE
--

Au cours de cette leçon, nous allons étudier le verbe « être » dans ces trois modes d’emploi en dioula de Côte d’Ivoire.
Il faut tout de suite souligner que le contexte détermine les usages de ce verbe dans les langues mandingues en général et dans le dioula véhiculaire de Côte d’Ivoire en particulier.

Veuillez trouver ci-dessous les différents contextes d’emploi :

== LOCALISATION

=== Dialogue introductif

[horizontal]
Youssouf:: I ni sɔgɔma Coulibali
Coulibali:: N’ba, i ni sɔgɔma
Youssouf:: Hɛrɛ bɛ?
Coulibali:: Hɛrɛ, i bɛ mini ?
Youssouf:: N bɛ so kɔnɔ
Coulibali:: I bɛ so kɔnɔ ni jɔn ye?
Youssouf:: N bɛ ni n ya sofɛricɛ ye so kɔnɔ. Ile do?
Coulibali:: Ne bɛ ni n ya wulu ye
Youssouf:: Ayiwa, k’an bɛ
Coulibali:: K’an bɛ Youssouf
 
« bɛ » dans un contexte de localisation traduit le verbe être en dioula de Côte d’Ivoire.
Sa forme negative est « tɛ »

=== Exemples

* N bɛ baarakɛyɔrɔ la
* Ali bɛ so kɔnɔ
* Moussa bɛ a ya butiki kɔnɔ
* Coulibali bɛ Bouaké
* Youssouf bɛ Abidjan
* A bɛ ni a muso ye Canada
* Daresalam limamu bɛ misiri kɔnɔ
* Sogofeerelaw bɛ lɔgɔfɛ la
* N bangebagaw bɛ Ameriki
* Youssouf ya baarakɛyɔrɔ bɛ Cocody

Lire les formes négatives de ces phrases

Pour les travaux pratiques, on aura besoin de connaitre quelques circonstants de lieu.
Il s’agit des différentes unités de la langue qui permettent de situer les éléments qui entourent le locuteur.

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|san fɛ|en haut
|dugu man|en bas, par terre
|kun na|au dessus, sur
|nya fɛ|devant, a l’avant
|kεrε fɛ|a coté de
|Kan|sur,
|la, na|a, en, dans, au
|===

=== Exercice de traduction

. Je suis chez mon professeur
. Où est le jouet de mon fils ?
. Qu’est qui est dans le sac ?
. Il n’y a rien dans ma poche
. Où se situe le marché du Plateau ?
. Nous sommes chez ma mère Aminata
. Ils sont tous chez le voisin de mon amie
. Yousouf et son ami Moussa sont en face de la boutique
. Je ne suis pas chez moi, je suis à l’hôpital
. Il est ici avec mon fils.
. Le chauffeur de mon père est dans la voiture
. Le repas est sur la table.


== LA PRESENTATION

=== Dialogue introductif

{zwsp}:: I ni sɔgɔma, n balimancɛ
{zwsp}:: N’ba i ni sɔgɔma. Hɛrɛ bɛ ?
{zwsp}:: Hɛrɛ, hakɛto I tɔgɔ ye di?
{zwsp}:: N tɔgɔ ye Youssouf, ile do?
{zwsp}:: Ne tɔgɔ ye Coulibali. Ile ye jɔn tericɛ ye?
{zwsp}:: Ne ye Moussa tericɛ ye. N bɛ bɔ Faransi, ile do?
{zwsp}:: Ne bɛ bɔ kɔdiwari. I baarakɛɲɔgɔnw bɛ Faransi walima Kɔdiwari ?
{zwsp}:: O bɛɛ bɛ Kɔdiwari yan


Dans le contexte de la présentation, le verbe être est matérialisé par la forme discontinue « ye….ye ». La forme négative est « tɛ….ye »

=== Exemples

. Ne ye Moussa badencɛ ye
. N ye Amidu dɔgɔcɛ ye
. Ali ye dɔtɔrɔ ye
. Youssouf ye kalanfa ye
. Drissa dencɛ ye sɛnɛkɛla ye
. Aminata ye gbakɛla ye
. Cɛ nin ye Assetou facɛ ye
. Barakissa ye Alidou muso ye
. Coulibali ye Youssouf ya kalanden ye
. Birahima ye Minisiri ya sofɛricɛ ye

Lire les formes négatives de ces phrases

=== Exercice de traduction

Phrases à traduire

. Qui est ce monsieur?
. Qui est ton père?
. Nous sommes les amis de ton fils Noam
. Je suis le professeur de Madou
. Ali n’est pas cultivateur, il est médecin
. Qui est derrière la voiture de Mariam ?
. Aujourd’hui est mardi
. Le fils de l’ami de Moussa s’appelle Ibrahim
. La fille de la tante de mon chauffeur est élève
. Le professeur de ma fille est français.

.Traductions
[%collapsible]
====
. Cɛ nin ye jɔn ye?
. I facɛ ye jɔn ye?
. An ye i dencɛ Noam teri cɛw ye.
. N ye Madu ya lakɔlifa ye.
. Ali tɛ sɛnɛkɛla ye, a ye dɔgɔtoɔrɔ ye
. Jɔn bɛ Mariam ya mobili kɔfe ?
. Bi ye taratalon ye.
. Musa tericɛ dencɛ tɔgɔ ye Ibrahim.
. N ya sofericɛ tɛnɛmuso denmuso ye kalenden ye.
. N denmuso ya lakɔlifa ye tubabumuso ye.
====

== QUALIFICATIF

=== Dialogue introductif

[horizontal]
{zwsp}:: I ni sɔgɔma, n balenmancɛ
{zwsp}:: N’ba i ni sɔgɔma. Hɛrɛ bɛ ?
{zwsp}:: Hɛrɛ, hakɛto I tɔgɔ ye di?
{zwsp}:: N tɔgɔ ye Youssouf, ile do?
{zwsp}:: Ne tɔgɔ ye Coulibali. Ile ye jɔn tericɛ ye?
{zwsp}:: Ne ye Moussa tericɛ ye. N bɛ bɔ Faransi, ile do?
{zwsp}:: Ne bɛ bɔ kɔdiwari. I baarakɛɲɔgɔnw bɛ Faransi walima Kɔdiwari ?


Dans le cadre de la description d’un objet, d’une personne ou de valeurs abstraites, le verbe être est marqué par « ka ».
Sa forme négative est « ma »

=== Exemples

. Ali muso dɔgɔcɛ ka bon
. N ya lakɔlifa ka jan
. Coulibali ka ɲi kosɔbɛ
. Fanta cɛ ka jugu kojugu
. Alimata ya gba ka di dêh
. Legilisi ka surun a ya so la
. Cɛ nin ya bon ka bon
. Mobili nin ka teli (teri)
. Moussa ya butiki ka dɔgɔ
. Lemurukumun ka kuna

Lire les versions négatives de ces phrases

Exercice de traduction

. Mon frère n’est pas grand
. Le fils de ma sœur est beau
. La cuisine de ma femme est bonne
. L’amie de mon frère est très gentille
. Cette mangue est amère

.Traduction
[%collapsible]
====
. N badencɛ ma bon
. N badenmuso dencɛ ka ɲi
. N muso gba ka di
. N badencɛ terimuso ka di kosɔbɛ
. Mangoro nin ka kumu??
====

Je me présente :

. Bonjour.
. Je m’appelle .
. Je viens du Canada.
. Je suis le père de…
. Mon père est à Abidjan.
. Mon lieu de travail est situé à Bouaké.
. Je suis beau et grand (de taille).
. L’Anglais est ma langue maternelle.

.Traduction
[%collapsible, none]
====
. A ni sɔgɔma
. N tɔgɔ ye Coulibali
. N be bɔ Canada.
. N ye ali facɛ ye.
. N facɛ be Abidjan
. N ya baarakɛyɔrɔ be buaké
. N ka ɲi ani n ka jan.
. Anglekan ye n ya kan ye
====

