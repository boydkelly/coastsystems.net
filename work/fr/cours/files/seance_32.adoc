= SÉANCE 32 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-10-11
:page-noindex: 
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 32 : KALANSEN BI SABA NI FILANAN

== RAPPEL POINTS DE GRAMMAIRE

[loweralpha]
. Correction de l’exercice de la leçon précédente
+
Toutes les réponses sont correctes.
Cependant, la traduction de la première phrase de l’exercice 1 n’est pas correcte.
On va revoir ce point ensemble.
. Petite révision des expressions avec le suffixe « tɔ »

* Sunɔgɔtɔ/ mɔgɔ caman sunɔgɔtɔ bɛ gɔrɔndɔ
* Bolitɔ/ den bolitɔ don na an kan ni kule ye
* Mɔgɔ ɲumankɛtɔ le bɛ na ni juguman ye tuma dɔw la
* Domunikɛtɔ = en mangeant.
* Kumakɛtɔ/Kumantɔ = en parlant.
* Baarakɛtɔ= en travaillant
* Yirantɔ = en faisant frire
* Barokɛtɔ = en causant/en echangeant

=== LES SPECIFICITES ET CONTEXTES D’EMPLOI DE « DI » EN DIOULA

==== Le mot « di » pour evoquer le goût ou le caractère de ce qui est plaisant à la vue ou au toucher

* Baranda ka di kojugu
* Zagamɛn ka di ni jɛgɛ ye
* Filimu nin ka di n muso ye/filimu ni ka di
* Lemuruji ka di katimi nyamankuji kan
* Sisɛsogo ka di ni salati ye

=== Exercice: Proposes 5 phrases du même type

==== Le mot « di » pour exprimer la simplicité ou la facilité à accomplir quelque chose

[qanda]
A sunɔgɔ ka di:: (il a une certaine facilité de s’endormir)
Cɛ nin kasi ka di kojugu:: (ce monsieur à une facilité de pleurer )
Coulibali nyinan ka di:: (Coulibaly oublie facilement)
N ya lakɔlifa nɛgɛ ma di:: (Il n’est pas facile de blaguer mon professeur)
Adama kunun ma di:: (il n’est pas facile de reveiller Adama)

[%colapsible]
. kabini a muso dencɛ wolola, a sunɔgɔ man di
. i jo le kelly nyinan ka ki kɔsɔbe dɛ !
. wari sɔrɔ ma di bouaké kɔnɔ

=== Exercice: Proposes 5 phrases du même type

Dans des contextes imagés (sens figurés)

[qanda]
A nisɔ ka di:: (il est de bonne humeur)
I sen ka di:: (Tu as la chance dans tes pieds. Tu tombes toujours au bon moment)
I kun ka di:: (Tu as la chance)
I bolo ka di:: (tu es adroit)
I da ka di:: (Tu sais parler ou tu as l’appétit)

[%colapsible]
====
. kabini a ka baara sɔrɔ, a nisɔ ka di dɛ !
. an bena lafiri ni sisɛ domu.  i sen ka di (i bɛn na i sen ma.)
. i ya bere sela kɔnɔnin ni ma, i bolo ka di.
. n be fɛ ka baaro kɛ n'i ye.   i da ka di kɔsɔbe.
. ...i kun ka di. 
====
