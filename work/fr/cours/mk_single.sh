#!/usr/bin/bash
# set -o errexit
# set -o nounset
# set -o pipefail

[[ -z "$1" ]] && { echo "You need to specify a file from nolinks"; exit 1; }

function validate {
  csvclean -n -v -t $1
  awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
  #delete missing field 1
#  sed -i /^,/d $1 
  #remove any defs with discontined verbs for now
#  sed -i /[...]/d $1
}

files=files
pages=pages
all="/home/bkelly/dev/jula/dyu-xdxf/mandenkan/dyu-uri.csv"
stop="stop.dyu.txt"

validate $all

filename="${1##*/}"
pagepath=$pages/${1##*/}
cp -rv $1 ./$pagepath
#remove the stopwords from dyu-codes; also remove white space and blank lines
cat ${all} | sed -e "$(sed 's:.*:s/^&,.*$//i:' $stop)" | sed 's/[ \t]*$//' | sed /^$/d > input.csv

#| sort -u | shuf | awk -v f=$udhrstats 'BEGIN {OFS=","} NR==1, NR==10 {print ". " $1  >> f}'
echo Processing $pagepath
while IFS="," read -r word id ;
do 
  #sed -i -r -e "/^image:/! s/(\b$word\b)/xref:fr@ROOT:lexique-dyu-show.adoc#$id\[\1\]/i" ./pages/$filename
  #sed -r -e "s/(\b$word\b)/xref:fr@ROOT:lexique-dyu-hide.adoc#$id\[\1\]/ip" ./files/$filename > ./pages/$filename
  #sed -i -r -e  "/^image:|^:.*|\b_.*[ɛɛ̀ɛ́]\b/! s/(^.*)(\b$word\b)/\1xref:fr@ROOT:lexique-dyu-show.adoc#$id\[$word]/i" ./pages/$filename;
  #sed -i -r -e  "/^image:|^:.*|\[_.*\]/! s/(^.*)(\b$word\b)/\1xref:fr@ROOT:lexique-dyu-show.adoc#$id\[$word]/i" ./pages/$filename;

  #sed -i -r -e  "/^image:|^:.*/! s/(?!$word)(^.*)(\b$word\b)/\1xref:fr@ROOT:lexique-dyu-show.adoc#$id\[$word]/i" ./pages/$filename;
  sed -i -r -e  "/^image:|^:.*/! s/( $word )/ xref:fr@docs:ROOT:lexique-dyu.adoc#_$id\[$word] /i" $pagepath;

done < input.csv
rm input.csv
git commit -a -m $0 
