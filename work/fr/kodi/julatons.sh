#!/usr/bin/bash
for x in `ls *.ly`; do
  lilypond -f svg -o ${x%.*}-tmp $x;
  lilypond -f png -o ${x%.*}-tmp $x;
  convert -crop 75x45+100+20 ${x%.*}-tmp.svg ${x%.*}.svg
  convert -crop 75x45+100+20 ${x%.*}-tmp.png ${x%.*}.png
  rm ${x%.*}-tmp.*
done
