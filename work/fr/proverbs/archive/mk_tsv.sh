#!/usr/bin/bash
#This may be outdated by the script in Obsidian, but may need the tsv here
#check this later
partial="/home/bkelly/dev/web-docs/docs/fr/modules/proverbs/partials/"
static_dev="/home/bkelly/dev/dev-build/supp_ui/"
static_main="/home/bkelly/dev/dyu-build/supp_ui/"
coast="/home/bkelly/dev/svelte/coastsystems.net/src/assets/metadata/"
slides="/home/bkelly/dev/jula/slides-dyu/"
lexicon="/home/bkelly/dev/jula/dyu-xdxf/mandenkan/"
project=proverbs
for x in tmp tsv md yml json; do export "${x}=${project}.$x"; done

while IFS= read -r -d '' file; do
  cat $file |
    sed -e '/^|/!d' | sed -e '/^|=/d' |
    sed -r -e 's/^\||\|$//' |
    sed -e 's/|/\t/' |
    sed -e '/^$/d' >>$tsv.~

  cat $file |
    sed -e '/^|/!d' | sed -e '/^|=/d' |
    sed -r -e 's/^\||\|$//' |
    sed -e '/^$/d' >>$md.~

done < <(find ./files -name "*.adoc" -print0)

sort -u -t $'\t' -k 2 $tsv.~ >$tsv
sort -t $'\t' -k 2 $tsv.~ | sed 's/\t/: /g' >$yml #sort -t $'\t' -k 2 $tsv.~ | awk '{print $0}' F='\t' OFS=': ' >$yml
sort -t $'\t' -k 2 $tsv.~ | jq -R -s -c 'split("\n") | map(split("\t")) | map({"dyu": .[0], "fr": .[1]})' >$json
#sort -t $'\t' -k 2  $tsv.~ | jq -R -c 'select(length == 2) | select(. != "\t\t") | split("\t") | select(.[0] != "null" and .[1] != "null") | {"dyu": .[0], "fr": .[1]}' > $json
# Count the records and print to the terminal
echo -e "------------ | ------------" >$md
sort -t '|' -k 1 $md.~ >>$md

rm ./*.~

# git commit -a -m proverbs
echo $(wc -l $tsv) proverbs
echo jason: $json
echo $(jq 'length' $json)

for x in $partial $static_dev $static_main $slides $lexicon; do cp -v $tsv $x$tsv; done
for x in $coast $lexicon; do cp -v $json $x$json; done

cp $md ~/notes/obsidian/dioula/lexique/
