#!/usr/bin/bash
# take care of smart quotes:
# \xC2\xA0 is nbsp
# the easy end of sentence puncutation 0 or more spaces (soft or nbsp) with a nbsp
sed -i 's/\([ |\xC2\xA0]*\)\!/\xC2\xA0\!/g' "$1"
sed -i 's/\([ |\xC2\xA0]*\)?/\xC2\xA0?/g' "$1"

# avoid asciidoc header meta (line starts with :)
sed -i '/^:\|::/!s/\([ |\xC2\xA0]*\:\)/\xC2\xA0:/g' "$1"
sed -i 's/\([ |\xC2\xA0]*\);/\xC2\xA0;/g' "$1"

# now the straigt quotes. but avoid asciidoc tables (line starts with [) 
sed -i '/^\[/!s/"\([^"]*\)"/“\xC2\xA0\1\xC2\xA0”/g' "$1" 

#opening smart quote followed by 0 or more spaces with french quote and nbsp 
sed -i 's/“ */«\xC2\xA0/g' "$1"
sed -i 's/”/\xC2\xA0»/g' "$1" 

# now fix existing french quotes with 0 or more soft or nbsp ; 
sed -i 's/«\([ | ]*\)/« /g' "$1"
sed -i 's/\([ |\xC2\xA0]*\)»/\xC2\xA0»/g' "$1"

#remove any spaces between the final puncutation and closing quote.  (french puncutation requirement)
sed -i 's/\([\!|\?]\)\([ |\xC2\xA0]\)»/\1»/g' "$1"

#this is not ok for dyula?????
#sed -i "s/'/’/g" "$1"

sed -i 's/oe/œ/g' "$1"

