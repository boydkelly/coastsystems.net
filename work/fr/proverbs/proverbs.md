# Proverbes


> Bɛɛ n’i diyaɲamɔgɔ lo.
> - A chacun a son partenaire aimé.

> Bɛɛ n’i karanmɔgɔ.
> - A chacun son maître.

> #Kɔnɔnin #kiri #lamaga lamaga #kojugu b’a ci.
> - A force de trop bouger l’œuf de l’oiseau, il finit par se casser.

> #Tɔri firikojugu b’a #bla #suma ra.
> - A force de trop jeter le crapaud, on le met à l’ombre.

> #Nin #muso #cɛ ka ɲi ! A #jogo ka ɲi, a cɛ #ɲininka #fɔlɔ dɛ !
> - Ah ! Cette femme est belle ! Elle a un bon caractère ! Pose la question à son mari d’abord.

> Makantaga tɛ #mɔgɔ #sɔnkɔrɔ bɔ i ra.
> - Aller à la Mecque n’enlève pas un ancien caractère.

> Ka sin #kɔɔ ra, o ka #fisa kɔda ra munumunu ye.
> - Aller directement au marigot est mieux qu tourner autour.

> Mɔgɔsaba teriya, #kelen b’a #kun fɛ.
> - Amitié de trois personnes, l’un restera seul.

> I dege mɔnni na, o ka #fisa ni #don go don jege #deli ye.
> - Apprendre à pêcher est mieux que prier pour des poissons.

> Dege dege le bɛ kɔnɔnnin lapan.
> - Apprendre peu à peu fait voler l'oiseau.

> #Kɛlɛ #kɔ ye #diya ye.
> - Après la querelle l’entente !

> A to ! A to ! N’i #ma to, n’a tonna i kɔrɔ, i #kelen b’a cɛ.
> - Arrête ! Arrête ! Si tu n’arrêtes pas, Quand tu en auras une montagne devant toi, tu le ramasseras seul.

> Biŋ #siri #baa ye, baa siri biŋ na, o be ɛ baa ka domuni.
> - Attacher l’herbe avec une chèvre, attacher une chèvre dans l’herbe, tout cela est la nourriture de la chèvre.

> #Sani i ye fitinan #mana #fiyentɔ ye, o #turu kɛ a ta #sɔsɔ ra, o #bena a #mako ɲa.
> - Au lieu d’allumer une lampe à huile pour un aveugle, mets cette huile dans son haricot, c’est mieux pour lui.

> #Sani i ye #kɔgɔ kɛ i nɛnkun kan, a kɛ i #kɛwale ra.
> - Au lieu de mettre du sel sur ta langue, mets le dans tes actes.

> #Sani i ye jɛgɛjalan kɛ ka su fifa, i tun ye o kɛ #nanji ye k’a #di a #ɲanaman ma.
> - Au lieu de souffler le mort avec le poisson séché, il aurait fallu lui en faire un bouillon de son vivant.

> Sigi ka mɛɛn, #tiɲɛ #lɔn jɔɔna.
> - Avec le temps la vérité sera connue.

> Sisɔrɔ le ka gwɛlɛn ka #tɛmɛ siyɔrɔsɔrɔ kan.
> - Avoir la vie est plus difficile que d’avoir un endroit pour vivre.

> Ka i borituma #lɔn o ka fisa, ni « n’ bɛ se #bori ra. »
> - Bien choisir le moment de courir, vaut mieux que de dire : « Je sais courir !»

> #Na diman tɛ #mɛn #daga kɔnɔ.
> - Bonne sauce ne dure pas dans la marmite.

> #Lon o lon cɛfari si #man ca.
> - Brave chaque jour n’a pas longue vie.

> Cɛnnikɛbaga ta #fanga ka #bon ni ɲanikɛbaga ta fanga ye.
> - Celui qui détruit a plus de force que celui qui construit.

> Bagama kɛlɛtigɛbaga #mako tɛ #jo sɔrɔri ra.
> - Celui qui fait palabre juste pour provoquer ne cherche pas de raison.

> Ŋanin firibaga be ɲina, nka min k’a #dɔn o tɛ ɲina.
> - Celui qui jette les épines oublie, mais celui qui les piétine n’oublie pas.

> #Jigi tɛ #mɔgɔ min #fɛ i #man kɛnɛ.
> - Celui qui n’a pas d’espoir est malade.

> Ni #ba tɛ #mɔgɔ min fɛ, i b’i #mamamuso sin min.
> - Celui qui n’as plus de mère, tête le sein de sa grand-mère.

> Tagamabari tɛ yirikurun #lɔn fɛnjugu ye.
> - Celui qui ne marche jamais ne sait pas que la souche de bois est dangereuse.

> #Janto n’ #yɛrɛ ra kɔrɔko ka ɲi.
> - Celui qui prend soin de lui-même, avance bien en âge.

> Ni b’i #yɛrɛ kɛ #ɲɔ ye, sisɛw b’i domu.
> - Celui qui se fait son, ls poules le picorent.

> Ɲininkarikɛbaga tɛ firi.
> - Celui qui se renseigne ne peut pas se perdre.

> #Mɔgɔ min b’a #fɔ i #diminin #ma ko i ye sabari, ni o ma kɛ i ɲin ye, i #jugu tɛ.
> - Celui qui te dit de te calmer quand tu es fâché, même s’il n’est pas ton ami, il n’est pas ton ennemi.

> #Mɔgɔ min #bena to #ji ra, #hali ni o #tigi ka #nɛgɛ #wulen le sɔrɔ a b’a mina.
> - Celui qui va se noyer saisira même un fer rougi au feu pour se sauver.

> #Mɔgɔ min be #na i ta so o ka #fisa n’i ye.
> - Celui qui vient chez toi est mieux que toi.

> #Jenbe tɛ a #yɛrɛ fɔ, #mɔgɔ le b’a fɔ.
> - Ce n’est pas le tam-tam qui parle mais celui qui le joue.

> Denmisɛn lɔnnin be min ye, #cɛkɔrɔba siginin be o ye.
> - Ce que l’enfant voit étant débout, le vieux le voit étant assis.

> Fɛn min ka #di #kogoro ye, kaganan tɛ o tana.
> - Ce que le varan terrestre aime, ne peut pas être le totem du varan d’eau.

> Fɛn min firibaga tɛ nimisa, o tɔmɔbaga tɛ ɲagari.
> - Ce que quelqu’un a jeté sans regret, celui qui le ramasse n’aura pas de joie.

> Fɛn min ka #di #mɔgɔ ye i ka #kan ka o #bla i #ɲa fɛ, n’o tɛ a kɔflɛ be caya.
> - Ce que tu aimes, mets le devant toi, sinon les coups d’œil par derrière seront nombreux.

> I b’i #yɛrɛ ye.
> - Ce que tu fait, revient sur toi-même.

> I #ma min #fɔ i talonyɔrɔ ra, ka #na o fɔ i benyɔrɔ ra.
> - Ce que tu n’as pas dit là où tu as trébuché, il ne faut pas le dire là où tu es tombé !

> N’i ka fɛn min ye a be #tagama #surugu #kɔ #fɛ su fɛ, a #den lo.
> - Ce que tu vois marcher derrière l’hyène la nuit, c’est son petit.

> Ɲafɛn tiɲɛ, tiɲɛfɛn #fana tɛ ɲa.
> - Ce qui est destiné à reussir ne se gâte pas, ce qui est destiné à être gâté, ne réussit pas.

> Fɛn min ka #ɲi #ɲɔ #ma o ye #bɔrɔ ye.
> - Ce qui est mieux pour le mil, c’est le sac.

> Min ka #fisa #ɲɔ #ma o ye #bɔrɔ ye.
> - Ce qui est mieux pour le mil est le sac.

> Min ya #nɔgɔ #ɲɔ #ma o ye #bɔrɔ ye.
> - Ce qui est mieux pour le mil est le sac.

> Fɛn min nana i #kama o tɛ i jɛn.
> - Ce qui est venu rien que pour toi, ne te manquera jamais.

> Dɔ ta kasiko ye dɔ ta yɛrɛko ko ye.
> - Ce qui fait pleurer l’un, fait rire l’autre.

> Fɛn min be #banabagatɔ kɛnɛya, o be #fa #bla kɛnɛbagatɔ ra.
> - Ce qui guérit un malade, rend fou un bien portant.

> Fɛn min ka #mɔgɔ sennateriya, o ka #kan ka i darateriya.
> - Ce qui t’a fait courir vite, doit te faire parler vite.

> Fɛn min be #mɔgɔ mina, i tɛ o senkan mɛn.
> - Ce qui va t’attraper, tu ne l’entends pas venir.

> Dɔ be so sanna, dɔ be soku tɛrɛmɛna.
> - Certains achètent le cheval, alors que d’autres discutent le prix d’une queue de cheval.

> #Cɛ dɔ ka surun, nka a ta #dereke #kan tɛ #mina dɛ !
> - Certains hommes sont courts, mais on ne peut pas attraper les colles de leur chemise.

> Dɔw tɛ #gwɛɲɛ bɔrɔraman lɔn, n’a #ma bɔ k’a #yira o ra.
> - Certains ne reconnaissent pas un fouet dans un sac, il faut l’enlever pour le leur montrer.

> #Den be #kasi sin le #nɔ fɛ, n’o dira a #ma a be je.
> - C’est à cause du sein que l’enfant pleure, quand on le lui donne il se tait.

> Yɛlɛmayɛlɛma le be ŋɔmi ɲa.
> - C’est à force de tourner et de retourner que la galette cuit bien.

> #Jusu le be #kɛlɛ kɛ.
> - C’est avec le cœur qu’on se bat.

> Sogotigi le be tasumatigi #yɔrɔ ɲini.
> - C’est celui qui a la viande qui cherche celui qui a le feu.

> #Nɛgɛ tigɛyɔrɔ ye numuntogo ye.
> - C’est dans la forge qu’on coupe le fer.

> #Kuma le be #na ni kuma ye.
> - C’est de la parole que nait la parole.

> Hakiritigi #fla le be #sisɛfan #firi #ɲɔgɔn fɛ.
> - C’est deux hommes sages qui peuvent se lancer un œuf.

> #Mɔgɔ #fla le be #kɛlɛ kɛ.
> - C’est deux personnes qui se battent.

> Ala le be #ji #di #bagabaga ma, k’a ta so lɔ.
> - C’est Dieu qui donne l’eau aux termites pour construire leur maison.

> Degiri le be #kɔnɔnin se #pan na.
> - C’est en apprenant que l’oiseau saura voler.

> #Juru le ka misidennin n’a #bamuso faran.
> - C’est la corde qui a séparé le veau de sa mère.

> #Sabari le be si bɔ #sisɛfan na.
> - C’est la douceur qui donne des plumes à l’œuf.

> #Muɲu le be si bɔ #sisɛfan na.
> - C’est la patience qui donne des plumes à l’œuf.

> #Hakiri le ye #mɔgɔya ye.
> - C’est la réflexion qui fait l’homme.

> #Miiri le #ɲɔ kɛra #dɔrɔ ye.
> - C’est la réflexion qui transforme le mil en bière.

> #Naloman ceguyafɛn ye sɛgɛn ye.
> - C’est la souffrance qui rend le stupide intelligent.

> #Wuru welenin le #bugɔ ka di.
> - C’est le chien appelé qui est facile à frapper.

> Kanbeleba le be sunguruba siyɔrɔ lɔn.
> - C’est le jeune homme frivole qui sait où dort la fille frivole.

> #Kojugu le ye #mɔgɔ #karanmɔgɔ ye.
> - C’est le malheur qui enseigne l’homme.

> #Dimi le be dimi sa.
> - C’est le mal qui guérit le mal.

> Faninyatigɛbaga le b’a #fɔ ko a seere be #ba kɔ.
> - C’est le menteur qui dit que son témoin est de l’autre côté du fleuve.

> #Muso ɲanin #den le be cɛn.
> - C’est l’enfant de la bonne femme qui est mal éduqué.

> #Toron ni yɛrɛko le be #dugu diya.
> - C’est les jeux et le rire qui rendent le village intéressant.

> Kɔnɔnw jɛnnin le be pii fɔ.
> - C’est les oiseaux rassemblés qui font du bruit.

> Sirakɔgɔmaw le be #ɲɔgɔn kinyɔrɔ lɔn.
> - C’est les tortues elles même qui savent où se mordre.

> #Mɔgɔ be mɔgɔ kɛ mɔgɔ ye.
> - C’est l’homme qui fait l’homme.

> Ka #kɔrɔtɔ Ala ma, o le be #na ni gwata lɔri ye, n’o tɛ ni su kora #yɔrɔ bɛɛ be kɛ #suma ye.
> - C’est l’impatience à l’égard de Dieu qui fait qu’on construit un hangar, sinon la nuit il y a de l’ombre partout.

> #Hakiri le be ɲɔɔ kɛ #dɔrɔ ye.
> - C’est l’intelligence qui fait la bière de mil.

> #Dɔn diyatuma le ye #mɔgɔ bɔtuma ye dɔn na.
> - C’est lorsque la danse est agréable qu’il faut sortir de l’arène.

> Diyanya le b’a to an b’an #boro la #ɲɔgɔn #kan na, n’o tɛ an si tagamatɔ tɛ ben.
> - C’est par amour qu’on se met la main sur l’épaule en marchant, sinon chacun peut marcher seul sans tomber.

> #Ji be #don #kɔ ra #dɔɔni dɔɔni le.
> - C’est peu à peu que l’eau emplit la rivière.

> Wuruwele le be a kongotaga diya.
> - C’est quand on appelle le chien qu’il est facile de l’amener à la chasse.

> #Nin ka di, nin ka gwo, totasa #fla le be #mɔgɔ #bla o #fɔ ra.
> - C’est quand tu as deux plats devant toi que tu peux dire : « Ceci est bon ! Ceci est mauvais !»

> Min be #fɔ #waraba su kunna, o tɛ fɔ a #ɲanaman kunna.
> - C’est qu’on dit près du cadavre du lion on ne peut pas dire cela près de son vivant.

> #Surugu #kelen le be surugu #tɔ bɛɛ #tɔgɔ cɛn.
> - C’est une seule hyène qui gâte le nom de toutes les autres hyènes.

> Bɛɛ ka #farin i #facɛ ta so #da ra.
> - Chacun est brave devant la porte de son père.

> Dugutaraman #kasi bɛnna #dondo #kɔrɔnin ma.
> - Chanter tard la nuit convient bien au vieux coq.

> Ko bɛɛ n’a tuma, ko be n’a wagati.
> - Chaque chose a son temps, chaque chose a son moment.

> #Ji bɛɛ n’a tɛmɛsira lo.
> - Chaque eau a son passage.

> #Ziirin bɛɛ n’a zaana.
> - Chaque histoire contient un proverbe.

> #Lon bɛɛ n’a dugugwɛkan.
> - Chaque jour a son lendemain.

> #Mori bɛɛ n’a #lɔyɔrɔ lo #misiri ra.
> - Chaque maître religieux a sa place dans la mosquée.

> Ala ka gwolo o gwolo dan, a gwanlon be yi, a sumalon be yi.
> - Chaque peau que Dieu a créée a son jour où il est froid, et son jour où il est chaud.

> #Yɛrɛ #ɲini ka #fisa ni #tɔgɔ ɲini ye.
> - Chercher sa vie est mieux que de chercher un nom.

> #Karifa ka #gwɛlɛ Ala ma.
> - Chose confiée coûte chère à Dieu.

> Fɛn ɲanin, #tigi caaman.
> - Chose réussie, (revendiquée par) plusieurs propriétaires !

> Murunin #da diyakojugu be a la faran.
> - Couteau trop tranchant déchire son fourreau.

> K’a #fɔ ko « tasuma !» o tɛ #mɔgɔ #da jɛnin.
> - Crier « au feu !» ne brûle pas la bouche.

> Kɔlɔnkɔnɔ banba, n’i benna a kan, kara, n’a benna i kan, kara.
> - Crocodile dans un puits : tu tombes sur lui, c’est grave ! Il tombe sur toi c’est grave !

> #Jɛgɛ tɛ #ji ra, i #kana #tɔgɔ la #jɔ #kolon na.
> - Dans ce marigot il n’y a pas de poisson, n’accuse pas le pauvre vieux filet.

> Ala #ma fɛn #dan min be #kanuya bɔ.
> - Dans tout ce que Dieu a créé rien ne vaut l’amour.

> Ka #tiga wɔrɔ #ɲɔgɔn kɔ, a #fara ka ca.
> - Décortiquer les arachides les uns après les autres les coques s’éparpillent et deviennent trop nombreuses.

> #Kabini Ala ka #dunuɲa #dan k’a #bla a #yɛrɛ #kɔ fɛ, a m’a kɔ #flɛ tugun.
> - Depuis que Dieu a créé le monde et la mis derrière lui, il ne s’est plus retourné pour le regarder.

> #Kabini Ala ka #dunuɲa #dan k’a #bla a #yɛrɛ #kɔ fɛ, a m’a kɔ #flɛ tugun.
> - Depuis que Dieu a créé le monde et la mis derrière lui, il ne s’est plus retourné pour le regarder.

> Ni yiriba ka ben, kɔnɔw be yɛrɛgɛ.
> - Dès que le gros arbre tombe, les oiseaux se dispersent.

> #Dugu o dugu #tɔgɔ ye ko bɛndugu, #kɛlɛ tɛ o dugu ci.
> - Des querelles ne peuvent pas casser un village qui s’appelle « village entente ».

> N’i ka #basa jannin ye, a su lo.
> - Dès que tu vois le margouillat sur le dos, c’est qu’il est mort.

> N’i ka i #tericɛ ta #dereke ye i #jugu #kan na, aw farantuma le sera.
> - Dès que tu vois ton ennemi s’habiller avec la chemise de ton ami, c’est que votre amitié est arrivée à son terme.

> #Ɲa jiginta tɛ sunɔgɔ.
> - Des yeux sans espoir ne dorment point.

> #Ji gwannin #fla tɛ se ka #ɲɔgɔn suma.
> - Deux eaux chaudes ne peuvent pas se refroidir.

> #Mɔgɔ bennin #fla tɛ se ka #ɲɔgɔn lawuri.
> - Deux hommes tombés ne peuvent pas se relever l’un l’autre.

> #Masacɛ #fla tɛ #kun gwolo #kelen kan.
> - Deux rois ne peuvent pas s’asseoir sur la même peau.

> Fantan #jigi ye ala ye.
> - Dieu est l’espoir du pauvre.

> Ala #ma borokandenw #dan ka o kaɲa.
> - Dieu n’a pas créé les doigts d’égale longueur.

> Ala ka #sula #lɔn kojugukɛbaga ye, k’a to a #kana yeri kɛ su fɛ.
> - Dieu sait que le singe est mauvais, c’est pourquoi il ne lui a pas permis de voir la nuit.

> K’a #fɔ fagantan #ma ko : « I ni sɔgɔma », o be bɛn, nka k’a fɔ a ma ko « hɛra #sira wa », o ye kɛlɛtigɛkan ye.
> - Dire à un pauvre « Bonjour », c’est bien, mais lui dire : « tu as bien dormi ?» c’est une provocation.

> « Hakɛ to !» O tɛ wurukinda suma.
> - Dire : « Pardon !» ne guérit pas une morsure de chien.

> #Den #wolo ka #fisa ni #jɔn #san ye.
> - Donner naissance à un enfant est mieux qu’acheter un esclave.

> Bulonkɔnɔsi tɛ se ka #den n’a #fa kɛ #flan ye.
> - Dormir dans le même vestibule ne fait pas que l’enfant et son père soient des camarades d’âge.

> #Mɔgɔ ɲumankɛtɔ be juguman kɛ.
> - En faisant le bien, parfois il arrive du mal.

> Bilankɔrɔ tɛ #mɔgɔ #ɲa gwan.
> - Epargner ne fait souffrir personne.

> #Jigi ka #fisa ni #fa ye.
> - Espoir vaut mieux qu’être rassasié.

> #Lonan jatigibugɔ, i tagamatuma sera dɛ !
> - Etranger qui frappe autochtone, ton départ est proche !

> « Kɛ n’ #yɛrɛ ra » #dan ye kɔlɔnkɔnɔboci ye.
> - Etre dans dans un puits, c’est le plus grand mal que quelqu’un puisse se faire à lui-même. 

> #Cɛya ye #gundo ye, #musoya ye gundo ye.
> - Etre homme, est un secret, être une femme, est un secret.

> Kofitininkɛ be #mɔgɔ #fadencɛ ɲadon i ra. Kobakɛ be mɔgɔ fadencɛ #ɲabɔ i ra.
> - Faire des choses médiocres fait que tes frères te méprisent. Faire de grandes choses fait que tes frères te respectent.

> #Fiyentɔya #ladegi #man gwo.
> - Faire l’aveugle n’est pas difficile.

> « Flamuso, i #kɔnɔman lo wa ?» « Ɔn hɔn. » « Den ye #cɛ le ye wa ?» « N’ #ma o lɔn !»
> - « Femme peulh, tu es enceinte ?» « Oui !» « Ton enfant est-il un garçon ?» « Ça, je ne sais pas !»

> #Muso dentan, muso saraman.
> - Femme sans enfants, femme coquette.

> Fagamadenbugɔ ni sigi tɛ bɛn.
> - Frapper le fils du chef et vouloir rester dans le village ne sont pas choses compatibles.

> #Mɔgɔ #ma bɔ bɛ mɔgɔ diya.
> - Garder la distance rend les affaires agréables.

> Mɔgɔ, n’i worora, i be neni, i be kɔrɔfɔ.
> - Homme, si tu nais, tu seras insulté, tu seras frappé.

> Mɔgɔ, i be #na mɔgɔw le boro, i be #taga mɔgɔw le boro.
> - Homme, tu viens (au monde) entre les mains des hommes, tu repars entre les mains des hommes.

> #Mɔgɔ lɔnnin #ci ka di.
> - Il est plus facile d’envoyer en mission un homme qui est débout.

> Ni #kuma #ma #gwoya #mɔgɔ min ɲana, a tɛ #diya o #tigi ɲana.
> - Il faut que la parole soit dure en ta présence, pour qu’elle devienne douce en ta présence.

> N’tɛ #bla ɲa, n’ tɛ bla kɔ, aw #mɔgɔ #fla lo.
> - Il faut que vous soyez trois en chemin pour dire : « Je ne veux ni être devant, ni être derrière !»

> Ni #bolo magara #daga ra, i b’a #lon ka gwanna.
> - Il faut toucher la marmite pour savoir qu’elle est chaude.

> #Mɔgɔ #man #kan k’a borokɔnɔ #jɛgɛ #firi a senkɔrɔta kosɔn.
> - Il ne faut pas jeter le poisson que tu as en main au profit de celui qui est à tes pieds.

> #Mɔgɔ #man #kan ka #bere to i boro, k’a to #wuru ye i kin.
> - Il ne faut pas laisser le chien te mordre alors que tu as un bâton en main.

> Kɔngɔtɔ #hakiri sigi n’i tɛ.
> - Il n’y a pas de paix de l’esprit si on a faim.

> Filanan tɛ #nin na.
> - Il n'y a pas son deux.

> Fɛn min be to #misɛnya ra, o #dan ye foni ye.
> - Il n’y a que le fonio qui reste petit.

> Ko #caaman be #mɔgɔya ra, bɛɛ tɛ o lɔn.
> - Il y a beaucoup de choses dans les relations humaines, que plusieurs ne savent pas.

> #Ɲɔ be ɲɔbugu, nka takan le b’a ra.
> - Il y a du mil au village du mil, mais il y a une condition pour le prendre.

> #Mɔgɔ saba be #dunuɲa ra minw tɛ fɛn sɔrɔ : #jeli bobo, #donso sɔgɔsɔgɔtɔ, #karanmɔgɔ aladaribari.
> - Il ya trois hommes dans le monde qui ne seront jamais riches : le griot muet, le chasseur qui tousse, le religieux qui ne prie pas.

> #Dan ye ko bɛɛ ra.
> - Il y a une fin a toute chose.

> #Daan be #koo bɛɛ ra.
> - Il y a une fin à toute chose.

> #Hakɛ be #dunuɲa ko bɛɛ ra.
> - Il y a une offense en toutes choses dans ce monde.

> Ɲamanfirisununkun be yi, nka balemafirisununkun tɛ yi.
> - Il y des endroits pour jeter des ordures, mais il y en a pas pour jeter des frères.

> « N’ta turujuru b’i ra, » o ye #tiɲɛ ye, nka « n’t’a #fɛ ka i #da turuman ye », o ye ɲadonya ye.
> - « J’ai un crédit d’huile sur toi »,c’est vrai. « Je ne veux plus te voir la bouche huileuse !» Ça c’est du mépris.

> N’ka so lɔn, n’ka #ji lɔn, yɛrɛlɔn le #ɲɔgɔn tɛ.
> - « Je sais aller à cheval, je sais nager », le mieux c’est : « je sais me prendre. »

> « N’ donna, nka n’ #ma #foyi ta !» I donbariya le tun ka fisa.
> - « Je suis entré, mais je n’ai rien pris !» le mieux serait de ne même pas entrer.

> N’bɛ #nin fɛ, n’ tɛ nin fɛ, #fla le #yira ra i ra.
> - Je veux ça, pas ça, c’est qu’on t’a montré deux choses.

> Bonbosi tɛ #kɔrɔya #sarati ye, #bakɔrɔnin be #woro n’a ta ye.
> - La barbe n’est pas une preuve qu’on est vieux, le bouc en a à sa naissance.

> Jagofɛn #ɲuman le be a #yɛrɛ fiyeere.
> - La bonne marchandise se vend d’elle-même.

> #Sisɛ #da ka #dɔgɔ burufiyɛ ma.
> - La bouche de la poule est trop petite pour jouer du cor.

> #Da ka surun, nka a #fɔɲɔ be #taga #yɔrɔ jan.
> - La bouche est courte, mais le vent qui en sort va loin.

> #Da dɔ be foroto #tɔgɔ fɔ, nka a t’a domu
> - La bouche peut dire le nom du piment mais sans le manger.

> #Sababu ko i #kana to #di ale ma, i kana #ji di ale ma, nka i kana #ɲina ale kɔ.
> - La cause dit ne pas lui donner à manger, ni a à boire, mais de ne pas l’oublier.

> #Tolen ko : « ne be n’ dulonyɔrɔ le ɲini, ne tɛ kotigiya ɲinina. »
> - La chauve-souris dit : « je cherche tout juste un coin pour m’accrocher, je ne cherche pas un grand nom. »

> Mɔgɔw k’i #lɔn ni turunin min ye n’i ka o li, mɔgɔw be #firi i ma.
> - La coupe de cheveux avec laquelle on te connaît, si tu l’enlèves, les gens ne te reconnaîtront plus.

> Gbabugu ka #kɔrɔ ni #misiri ye.
> - La cuisine est plus âgée que la mosquée.

> Ɲin gwɛra, nka #basi b’a jukɔrɔ.
> - La dent est blanche, mais il y a du sang (rouge) en bas.

> Sogomuso kɔnɔman, donsocɛ #muso kɔnɔman, dɔ #na kɛ dɔ ta #nanji ye.
> - La femme enceinte de la proie, la femme enceinte du chasseur, l’un sera sauce pour l’autre.

> #Masa #kelen #tere tɛ #dunuɲa ban.
> - La fin du règne d’un roi n’est pas la fin du monde.

> #Kuma tɛ #mɔgɔ #bon ka i jɛn.
> - La (flèche de la) parole ne manque jamais sa cible.

> #Kirisi min be #mɔgɔ bɔ kɔlɔn kɔnɔ, o be #kɔrɔbɔ ni jurufiyɛ le ye.
> - La formule magique qui peut faire sortir quelqu’un du puits, se vérifie avec une puisette d’abord.

> #Kɔrɔ tɛ #mɔgɔ sigi n’a kɛbaga tɛ i boro.
> - L’âge avancé n’oblige pas quelqu’un à s’asseoir, à moins que tu n’aies quelqu’un d’autre qui travaille à ta place.

> #Kɔrɔ tɛ fago sa.
> - L’âge n’empêche pas d’être sot.

> « A bla !» « N’ta bla !» #Tasuma tɛ.
> - « Laisse !» « Je ne laisse pas !» Ce n’est pas du feu !

> I #sɔnkɔrɔ bla, o ka #fisa ni jatigiya #yɛlɛma ye.
> - Laisser ton mauvais caractère, vaut mieux que changer de tuteur.

> An ye #jo to #bɔrɔ kɔnɔ k’a sɔn.
> - Laissons le fétiche dans le sac pour l’adorer.

> #Kolo tɛ #nɛn #na ko a tɛ yɛlɛma.
> - La langue n’a pas d’os, pour qu’elle ne tourne pas.

> #Jama jɛnna ka dugawu kɛ #mɔgɔ min ye, mɔgɔ #kelen ta danga tɛ se ka #foyi kɛ o ra.
> - La malédiction d’une seule personne ne va rien lui faire à l’homme que toute la foule a béni ensemble.

> #Namara tɛ #basi mɔn.
> - La malhonnêteté ne fait pas cuire le couscous.

> Kɔɲɔmuso nakun ye cɛko ye.
> - La mariée vient pour son mari.

> Sisɛba tɛ fɛnjugu yɛrɛgɛ k’a kɛ a #den kɔrɔ.
> - La mère poule n’éparpille rien de mauvais devant ses poussins.

> Ka #ɲɔgɔn siraratagama, #sula ni #wuru t'a #teriya #ma o bɔ.
> - L’amitié du singe et du chien n’arrive pas au point de se rendre visite souvent.

> #Mɔgɔ #kelen #saya tɛ #dugu diman ci, nka a b’a suma.
> - La mort d’un seul homme ne détruit pas le village, mais elle refroidit le village.

> #Kɔgɔ be #fali #kɔ ra, a #lɔgɔ b’a ra.
> - L’âne porte le sel sur son dos, et il en a envie.

> #Wara min ko : « a ye n’ #kun ci !», ɲama tɛ o ra.
> - L’animal qui dit lui-même : « Fracassez moi le crâne !» n’a pas de force vengeresse !

> #Sɔn bɛ pan, a #den te ŋunuman.
> - L’antilope saute, son petit ne va pas ramper.

> Ni fɛn ka #dɔgɔya #yɔrɔ min a be #kari o yɔrɔ le ra.
> - Là où c’est mince, c’est là que ça se casse.

> I gwolo tɛ se #yɔrɔ min #na n’i ko i b’a #sama k’a se yi, a be faran.
> - Là où ta peau n’arrive pas, si tu veux la tirer pour y arriver à tous prix, elle va se déchirer.

> #Kuma ko ale tigɛra #mɔgɔ #fla ra : min b’a #da #don a ra k’a sɔrɔ mɔgɔ m’a #sara a ra, ani min b’a daje, k’a sɔrɔ kuma dira a ma.
> - La parole dit qu’elle a perdu confiance en deux personnes : celui qui parle sans qu’on ne lui passe la parole, celui qui se tait alors qu’on lui a passé la parole.

> #Kuma tɛ ban, a be #lalɔ le.
> - La parole ne finit pas, on l’arrête.

> #Kuma tɛ #mɔgɔ ban.
> - La parole ne tue pas l'homme.

> Wɔlɔnin #ma #kuru ban, #kiri kɔnɔntɔn !
> - La perdrix ne couve pas encore, et déjà neuf œufs !

> Ele ka wɔlɔni min faga, dɔ ka o #fan cɛ.
> - La perdrix que tu as tuée, quelqu’un d’autre en a ramassé les œufs.

> #Bo kɛra le #mako ye #selidaga ra.
> - La personne qui veut aller a la selle a besoin de bouilloire.

> #Mɔgɔ worora ka #jori min to i #facɛ #sen na, i ka #kan ka #deri o #kasa ra.
> - La plaie que tu as trouvée sur le pied de ton père à ta naissance, tu dois être habitué à l’odeur de cette plaie.

> Fɛn #fɔlɔ le ye fɛn fɔlɔ ye, ni min bɔra o kɔ, a be #fɔ ko dɔwɛrɛ bɔra.
> - La première chose est vraiment la première, Ce qui vient après, on dit : « un autre ».

> #Kuma #man #kan ka kɛ gwɛsɛ ye k’a bɛn #mɔgɔ #kelen #da ma.
> - La prole ne doit pas être un cure-dent pour la laisser dans la bouche d’une seule personne.

> Kɔnɔ be se ka #pan ni #ji min ye, a be o le min.
> - La quantité d’eau qui peut permettre à l’oiseau de voler, c’est ce qu’il boit.

> #Diya #ma fɛn min ɲa, #kɛlɛ tɛ o ɲa.
> - La querelle ne peut pas arranger ce que la douceur n’a pas pu arranger.

> I #fa yɛlɛnna yiri min na, #hali n’i #ma se ka #yɛlɛ o yiri ra, i ka #kan ka se ka i #boro la a jura.
> - L’arbre dans lequel ton père montait, même si tu ne peux pas monter à cet arbre, tu dois pouvoir au moins poser ta main sur son tronc.

> Yirinin min be #kuru kan, a be ale #ɲana ko a ka #jan ni yiri tɔw bɛɛ ye.
> - L’arbre qui est sur la montagne pense qu’il est le plus grand de tous les arbres.

> Nanforotigiya tɛ kelennamiiri sa.
> - La richesse n’empêche pas d’être solitaire et pensif.

> #Mɔgɔ be #kɔ min lɔn, i be to #ji le ra.
> - La rivière qu’on connait, c’est dans son eau qu’on se noie.

> #Hakiri le ye bɔrɔba tajuru ye.
> - La sagesse est la corde par laquelle le gros sac lourd se prend.

> Safinaminan cira, #wuru ta mun be o ra ?
> - La savonnière est cassée, en quoi cela regarde le chien ?

> #Ɲinan be tokalama ben, nka tɛ se k’a lawuri.
> - La souris fait tomber l’ustensile mais il ne peut pas le relever.

> Dondokɔrɔ #kunkolo tɛ #dogo nan na.
> - La tête du vieux coq est toujours visible dans la sauce.

> #Misi be #basi to a #fari ra ka #nɔnɔ bɔ.
> - La vache a du sang dans son corps, mais elle donne du lait.

> Ni #misi panna a biribaga kunna, ele sigikun ye mun ye tugun ?
> - La vache a sauté par dessus la tête de celui qui la trait, et toi tu es assis pour quoi encore ?

> Ni #misi panna a biribaga kunna, ele sigikun ye mun ye tugun ?
> - La vache a sauté par dessus la tête de celui qui la trait, et toi tu es assis pour quoi encore ?

> #Kogoro #ɲa wulenna, nka a ta ko tɛ #mɔgɔ fɛ.
> - La varan terrestre a les yeux rouges mais il n’est pas fâché contre quelqu’un.

> #Koo lajɛbali, #tiɲɛ tɛ lɔn.
> - La véracité d’un chose non vérifié est inconnue.

> #Tiɲɛn be #mɔgɔ #ɲa wulen, nka a t’a ci.
> - La vérité rougit les yeux, mais elle ne les casse pas.

> #Fiyentɔ be dɔnkɛ n’a ta dɔnkɛsara ye a boro.
> - L’aveugle danse avec son salaire de danse en main.

> Warasogo gwaniman le ka di.
> - La viande d’un fauve est bonne quand c’est chaud.

> #Natabaya ye #mɔgɔ #mako sa ra.
> - L’avidité rend malheureux.

> #Dunuɲa ye #jigijigi caaman, ani yɛlɛyɛlɛ #caaman ye.
> - La vie c’est beaucoup de montées et beaucoup de descentes.

> #Dunuɲa ye sɔgɔmada #caaman ye.
> - La vie est faite de beaucoup de matins.

> Dunuɲalatigɛ ye batigɛ le ye.
> - La vie est une traversée de fleuve.

> Sufɛkunli lawurira torotigɛ le ma.
> - La vraie raison pour raser la tête la nuit c’est de couper une oreille.

> #Ji min ye bɔ ra #man #nɔgɔ #da o le suman.
> - L’eau qui sort de la gueule du silure est fraiche.

> N’i ka sa ye ka #bere min to i #boro i b’a #faga ni o ye.
> - Le bâton que tu as en main au moment où tu vois le serpent, c’est avec celui là que tu le tues.

> #Hɛrɛ #loon ka #jan #sabari tɛ.
> - Le bonheur est loin mais arrivera.

> Deregenin #kan ba
> - Le boubou est petit et l’ouverture pour la tête trop large.

> #Bakɔrɔnin ka #sibon sɔrɔ, a ko « lukɛnɛ !»
> - Le bouc a trouvé une chambre, il demande la cour.

> #Kɔrɔ #kun ye hakirisigi ye.
> - Le but de l’âge mûr c’est avoir un esprit calme.

> Dɛgɛminbaga #da kurunin lo, nka a #diminin tɛ.
> - Le buveur de dégué a la bouche bien fermée, mais il n’est fâché.

> #Ba degunin be #kinin kɛ.
> - Le cabri coincé peut mordre.

> #Ba min be kasi, #minlɔgɔ tɛ o ra.
> - Le cabri qui bêle n’a pas soif.

> #Tigi #wɛrɛ be #sogo su la min ni #duga tɛ #kelen ye.
> - Le cadavre a un autre propriétaire que le vautour.

> #Fa tɛ #ɲɛbɛrɛ ra ko a be #taga #dondo #kɔrɔnin #fo sɔgɔma.
> - Le cafard n’est pas fou pour se permettre d’aller saluer le coq le matin.

> Lakajan ka #di #ɲɔgɔmɛ ye, nka a kɔkuru tɛ sɔn.
> - Le chameau voudrait bien se coucher sur le dos, mais sa bosse refuse.

> Dugutigi ta ye a ta #dugu ye ; « n’ tɛ si » ta ye a #nin ye.
> - Le chef de village est responsable de son village, celui qui refuse de dormir au village est responsable de son âme.

> Fagama ta kumalɔn tɛ kumalɔn ye, fangantanw ta be lakari a ye, mɔgɔbaw ta be lakari a ye.
> - Le chef sait parler, mais lui-même ne sait pas parler, on lui rapporte les paroles des pauvres on lui rapporte les paroles des grands, (et il les répète).

> #Da tɛ balimaya #sira bɔ, #sen le b’a bɔ.
> - Le chemin qui relie les frères n’est pas tracé avec la bouche, mais avec les pieds.

> Denmisɛnso #sen ka teri, nka a tɛ #taga #yɔrɔ jan.
> - Le cheval de jeunesse est rapide, mais il ne va pas loin.

> Denmisɛnwuru #sen ka teri, nka a tɛ #kongo lɔn.
> - Le chien de jeunesse est rapide, mais il ne connait pas la brousse.

> Denmisɛnwuru #sen ka di, nka a tɛ #sira lɔn.
> - Le chien de jeunesse sait courir, mais il ne sait pas la route.

> #Sira #fara nugunin lo, #nga a #den be ɲagan.
> - L’écorce du baobab est lisse, mais son fruit provoque des démangeaisons.

> #Mugu min be fagama faga, jamanaden bɛɛ be o #cikan mɛn.
> - Le coup de fusil qui tue le chef, tout le peuple va l’entendre tirer.

> Ni #muru #da ka #diya #cogo o cogo, a tɛ se k’a #kala tigɛ.
> - Le couteau a beau être tranchant, il ne peut pas couper sa manche.

> #Tɔri n’a kɔnɔfɛn bɛɛ ye #duga ta ye.
> - Le crapaud appartient au vautour, avec tout ce qu’il contient .

> Kukala tɛ #tori fɛ, nka Ala b’a fifa.
> - Le crapaud n’a pas de queue, mais Dieu le souffle.

> Sigi ka #di #banba ye, nka a kukala tɛ sɔn.
> - Le crocodile aimerait bien s’asseoir, mais sa queue refuse.

> #Tasuma minana, #sogo tɛ n’ boro, ne marora.
> - Le feu est allumé, et je n’ai pas de viande, quelle honte !

> Dugutigi #den ta #sira tɛ kogokɔfɛkuma ra.
> - Le fils du chef de village n’a rien à voir dans ce qu’on dit derrière le mur.

> #Fatɔ laganviyara, nka a #man kɛnɛ.
> - Le fou est libre, mais il est malade.

> #Yiriden #gwɛrɛn be se ka #ben yiriden mɔnin ɲa.
> - Le fruit vert peut tomber avant le fruit mûr.

> Cɛnin min ka so lɔn, ani so ka cɛnin min faga, o bɛɛ ye o cɛnin #kelen le ye.
> - Le garçon qui savait bien monter à cheval, et le garçon que le cheval a tué, C’est bien le même garçon.

> Danninfɛn ye wurifɛn ye, #yiriden ye mɔfɛn ye, #mɔgɔya ye #hakiri ye.
> - Le grain qu’on sème doit pousser, le fruit de l’arbre doit mûrir, dans la vie l’homme doit réfléchir.

> Fɔɲɔba be yiribaw ben, nka a be #tɛmɛ yirimisɛnw ni binw kunna.
> - Le grand vent fait tomber les gros arbres, mais il passe au dessus des arbustes et des herbes.

> #Jeli tɛ malo, nka a tenda be wɔsi.
> - Le griot n’a pas honte, mais son front transpire.

> Ni jeliya diyara #jeli min na, o b’ a #yɛrɛ kɛ Kuyate ye.
> - Le Griot qui a bien réussi, se fait appeler Kouyaté.

> #Cɛ siranbagatɔ borikan ye ko : « Mugu banna, #kisɛ bɛɛ banna !
> - Le guerrier peureux court en disant : « Il n’y a plus de poudre, plus de plomb !»

> #Jugunin n’a kɔnɔfɛn bɛɛ ye #duga ta ye.
> - Le hérisson appartient au vautour, avec tout ce qu’il contient.

> #Toron tɛ #sɔbɛ sa.
> - Le jeux n’empêche pas d’être sérieux.

> #Lon ka jan, nka a sebari tɛ.
> - Le jour qu’on attend semble loin, mais il arrivera sans doute.

> #Nɔnɔ tɛ #dɛgɛ tiɲɛ.
> - Le lait ne gâte pas dégué.

> #Sama tɛ #bonya #kongo ma.
> - L’éléphant n’est jamais trop grand pour la forêt.

> Sonzannin bɔnin be #fali #ma #nga a #den tɛ.
> - Le lièvre ressemble a l’âne mais ce n’est pas son petit

> Sonzannin bɔra #fali fɛ, nka a #den tɛ.
> - Le lièvre ressemble à l’âne, mais il n’est pas son fils.

> Dɔnkiri ka #di ɲanibagatɔ ye, nka a b’a siɲanakun bɔ.
> - Le malheureux aime entendre la chanson, mais à force d’y penser ça va l’empêcher de dormir la nuit.

> « Lɔgɔ girin na », o ye #tiɲɛ ye, nka « ne tɛ #lɔ ni n’ #ma se so », walifɛn le b’i kun.
> - Le marché s’est terminé dans le désordre, mais celui qui dit qu’il ne s’arrêtera plus jusqu’à la maison, a certainement pris des choses d’autrui.

> #Basa be #ji min ni sisɛw ye.
> - Le margouillat boit avec les poules.

> Gundojugu be i ko #sogo kɛnɛ, n’a torira, a #kasa be bɔ.
> - Le mauvais secret c’est comme de la viande fraîche. Quand il pourrit, il sent mauvais.

> Ngalon ka teli, #nga #tiɲɛ le be laban.
> - Le mensonge est rapide, mais c’est la vérité qui finira.

> #Ɲɔ sannin, ani i #yɛrɛ ta ɲɔ sɛnɛnin, o ɲɔ #fla #diya tɛ #kelen ye.
> - Le mil acheté et le mil cultivé soi-même n’ont pas le même goût.

> #Ɲɔ sannin, ani i #yɛrɛ ta ɲɔ sɛnɛnin, o ɲɔ #fla #diya tɛ #kelen ye.
> - Le mil acheté et le mil cultivé soi-même n’ont pas le même goût.

> #Ɲɔ be #don #ji ra #lon min, a tɛ kɛ #dɔrɔ ye o lon.
> - Le mil ne devient pas du dolo, le même jour où on le met dans l’eau.

> O b’a #fɔ #tɔri #ma #yɔrɔ min ko « Bagayɔgɔ », o ye a buguyɔrɔ ye.
> - Le moment où on peut (remercier) le crapaud et lui dire : « Bakayoko » c’est le moment où il se couche.

> #Mɔgɔ denmisɛnman be lɔgɔkurun min ɲini, i cɛkɔrɔbaraman be #ja o le ra.
> - Le morceau de bois que tu as cherché pendant dans ta jeunesse, c’est avec ça que tu te chauffes dans ta vieillesse.

> Fagantan ta karafe ye a kanadereke ye.
> - Le mors du pauvre c’est la chemise qu’il porte.

> #Bobo be siko, ni #dugu gwɛra a ɲafɔko b’a kɔnɔgwan.
> - Le muet fait un rêve la nuit, le matin il ne sait comment le raconter.

> Denmisɛn tɛgɛkokoɲuman be mɔgɔkɔrɔba #dɛgɛ nɔɔni.
> - L’enfant dont la main est bien lavée peut préparer le « dégué » d’une vielle personne.

> #Den be i ko #bɔgɔ kɛnɛ, n’i ka #lalaga #cogo min na, a be #ja ten.
> - L’enfant est comme de la boue humide, la forme qu’on lui donne, il sèche en gardant cette forme.

> Woroden ye #dugalen le ye, a tɛ #flɛ ka ban.
> - L’enfant est un miroir pour les parents, on n’en a jamais assez de le regarder.

> #Den be #ben a minabaga le boro.
> - L’enfant ne tombe que de la main de celui qui le prend.

> #Den be se ka #wolo a #fa kɔ, nka a tɛ wolo a #ba kɔ.
> - L’enfant peut naître en l’absence de son père, mais pas en l’absence de sa mère.

> O be #den min #bugɔ jɛgɛjalanɲimin kosɔn, ni o kasitɔ b’a #da sin baji ma, o tɛ jɛgɛjalanɲimin dablakan ye.
> - L’enfant qu’on frappe parce qu’il mange du poisson séché, si ce dernier pleure en dirigeant sa bouche vers le fleuve, ce n’est signe qu’il veut arrêter de manger le poisson séché.

> #Ɲanafin be #fali faga.
> - L’ennui tue l’âne.

> #Fin ye #kelen ye nga, a manamanacogo tɛ kelen ye.
> - Le noir est le même mais pas ça manière de briller.

> #Cɛkɔrɔba #jamu ye ko « kogwɛlɛnkɛ. »
> - Le nom du vieux est « celui-qui-brave-difficulté. »

> N’i ka #tɔgɔ min la i ta #wuru ra, bɛɛ b’a #wele o le ra.
> - Le nom que tu donnes à ton chien, c’est par ce nom que tous vont l’appeler.

> Fagantan nana #dunuɲa #sen #mina a bosobagaw ye le.
> - Le pauvre est né pour saisir les pieds du monde, et aider les riches à le dépecer.

> Bilakoro #facɛ be se a ra, bilakoro be se #basa ra, basa be se #liden na, liden be se bilakoro n’a facɛ ra.
> - Le père du garçon est plus fort que lui, le garçon est plus fort que le margouillat, le margouillat est plus fort que l’abeille, l’abeille est plus fort que le garçon et son père.

> Sandigi ta #foyi tɛ sisɛdenin ta maɲumankokan na.
> - L’épervier s’en fout des cris de détresse du poussin.

> #Gban tɛ se ka #janya a karibaga ma.
> - Le pied de gombo ne peut pas être trop long pour celui qui coupe le gombo.

> #Sen ye duguma, #saa ye duguma o tɛ #fɔ #ɲɔgɔn kɔ.
> - Le pied est a terre, le serpent est a terre, ils se rencontreront un jour.

> #Jɛgɛ bɔra Zan ta #jɔ ra, ka #don N’Golo ta jɔ ra, a bɛɛ ye #kelen ye.
> - Le poisson a quitté le filet de Jean, pour entrer dans celui de Paul, tout est pareil.

> #Jɛgɛ ka #ji lɔn, nka #nanji ko tɛ.
> - Le poisson sait nager dans l’eau, mais pas dans l’eau de la sauce.

> #Sisɛ #da #ma #ɲi murusankokuma ra.
> - Le poulet ne doit pas parler dans une affaire d’achat de couteau.

> #Sisɛ tɛ se ka #ban #dugutaga ma.
> - Le poulet ne peut pas refuser d’aller en voyage.

> Tinakɔn be #na #wula fɛ.
> - Le regret vient à la fin de la journée.

> Wurufa #fla ye jendekala ye.
> - Le remède de la rage c’est un coup de gourdin.

> A be fɛntigiw #ɲana ko fangantanw be tugura le.
> - Le riches pensent que les pauvres font exprès.

> Forokonin #tora a logisara ra.
> - Le sac a été vendu à son prix de revient, (sans bénéfice).

> Ni forotobɔrɔ ka #kɔrɔ #cogo o cogo, tisota be sɔrɔ a ra.
> - Le sac de piment a beau être vieux, il y en aura suffisamment pour faire éternuer.

> Saraka be bɔ #lon min a tɛ #mina o lon.
> - Le sacrifice n’est pas exaucé le même jour où on l’offre.

> #Bɔrɔ #lakolon tɛ se ka lɔ.
> - Le sac vide ne peut pas tenir debout.

> Kumanci ka ca, walenci le ka dɔgɔ.
> - Les beaux parleurs sont nombreux, ce sont les gens efficaces qui manquent.

> Saninfugula min be kɛnɛbagatɔ #kun na, #banabagatɔ le be o ye.
> - Les bien portants ont des couronnes en or, que seuls les malades voient.

> Fɛnw fincogo ye #kelen ye, nka o manamanacogo tɛ kelen ye.
> - Les choses noircissent de la même manière, mais ils ne brillent pas de la même manière.

> #Kɔ mɔna ni #kan min ye, #jɛgɛ be taran n’o kan le ye.
> - Les closes sur lesquelles on a fait la pêche, c’est sur les mêmes qu’on partage les poissons.

> #Boro #fla le be #ɲɔgɔn ko.
> - Les deux mains se lavent mutuellement.

> #Fiyentɔ ta #kɔgɔ bɔnna bɛrɛkisɛ ra, takanɛnɛ dabɔra.
> - Le sel de l’aveugle est versé dans le gravier, il va goûter un à un tout ce que sa main va trouver.

> #Kɔgɔ tɛ bɔ ra nan na.
> - Le sel n’a pas quitté la sauce.

> Bɛɛ sobɛ ye i sobɛ le ye.
> - Le sérieux de chacun est son sérieux.

> Sa #ɲa ka misɛn, nka #boro tɛ su a ra.
> - Le serpent a des petits yeux, mais on ne peut y mettre le doigt (en signe de mépris).

> Sa be siran, a fagaba be siran.
> - Le serpent a peur, celui qui le tue a aussi peur.

> #Saa be duguma, seen be duguma, o tɛ #fɔn #ɲɔgɔn kɔ.
> - Le serpent est par terre, le pied est à terre, ils ne manqueront pas de se rencontrer.

> I #tɛna #ɲa #bo min #cɛ kɔ, i #kana to sisɛw y’a yɛrɛgɛ.
> - Les excréments que tu devras ramasser à tout prix, ne laisse les poules l’éparpiller.

> Ni #sula ka yiri #lɔn #cogo o cogo, a be kɔnɔ kɔ.
> - Le singe a beau savoir grimper à l’arbre, il vient après l’oiseau.

> #Sula tɛ #wuru kɔdimitɔ lɔn.
> - Le singe ne cherche pas à savoir si le chien a mal au dos ou non.

> #Sama tɛ #dɛsɛ a #ɲi kɔrɔ.
> - Les ivoires de l’éléphant ne peuvent pas être trop lourds pour lui.

> Dudunkan o, balankan o, susurikan ka #di #lonancɛ #toro ra ka #tɛmɛ #nin bɛɛ kan.
> - Le son des tam-tams c’est bien, le son des balafons c’est bien, mais le meilleur son dans l’oreille de l’étranger c’est le bruit des pilons.

> #Subaga be ɲina, nka a ka min #den #domu o tɛ ɲina.
> - Le sorcier oublie, mais celui dont il a mangé l’enfant n’oublie pas.

> #Naloman ye #dugu fagama dɔ le ye.
> - Le sot est lui aussi un personnage important du village.

> #Torogwɛrɛn tɛ sankurukan mɛn, nka ni #sanji be #ben a be o lɔn.
> - Le sourd n’entend pas le ciel gronder, mais quand il pleut il le sait.

> Mɔgɔkɔrɔbakuma ye #surugu #bo le ye, n’a kɔrɔra a be gwɛ.
> - Les paroles des anciens c’est les excréments de l’hyène, avec le temps, elles blanchissent.

> #Kojugu senkan tɛ bɔ.
> - Les pas du malheur ne font pas de bruit.

> #Kami ta ɲɛgɛnw b’a #kɔ kan, #adamaden ta b’a kɔnɔ.
> - Les points de beauté de la pintade sont sur son dos, les points de beauté de l’homme intelligent sont dans son cœur.

> #Jɛgɛ #jigi je #ji ye, #sogo jigi ye #kongo ye, #mɔgɔ jigi ye mɔgɔ ye.
> - L’espoir du poisson c’est l’eau, l’espoir du gibier c’est la forêt, l’espoir de l’homme, c’est l’homme.

> Sulaw tɛ #ɲɔgɔn kɔnɔ kabakariyɔrɔ ra.
> - Les singes ne s’attendent pas pour commencer à manger le maïs.

> #Jɛn ta #nɔgɔ tɛ nɔgɔjugu ye.
> - Les taches de l’union ne sont pas de mauvaises taches.

> Dudunkan bɛnna, balankan bɛnna, filenkan bɛnna, #mɔgɔya bɛnnin ka #fisa ni #nin bɛɛ ye.
> - Les tam-tam s’accordent bien, les balafons s’accordent bien, les flûtes s’accordent bien, mais il est encore mieux quand les hommes s’accordent bien.

> #Dugu mɔgɔkɔrɔbaw ye yiribaw le ye, ni o ka ben, o #ɲɔgɔn sɔrɔ #man di.
> - Les vieux du village sont des grands arbres, quand ils tombent, on les remplace difficilement.

> #Ɲa be ko dɔ ye, #da tɛ o fɔ, tulo be ko dɔ mɛn, da tɛ o fɔ.
> - Les yeux voient certaines choses que la bouche ne dit pas, l’oreille entend certaines choses que la bouche ne dit pas.

> #Jenbe tɛ a #yɛrɛ fɔ.
> - Le tam-tam ne résonne pas seul.

> #Lonancɛ jiminbari tɛ #tiɲɛ fɔ.
> - L’étranger qui n’a pas bu ne dit pas la vérité.

> #Misiden tɛ #jara lɔn, nka a #facɛ k’a lɔn.
> - Le veau ne connait pas le lion, mais son père le connait.

> #Fɔɲɔ be ɲɔfiyɛbaga #boro kɔrɔtanin le sɔrɔ.
> - Le vent vient trouver les mains de la vanneuse en haut.

> #Cɛkɔrɔba siginin le be siyɛnta ɲalɔn, n’a wurira ka #lɔ a be bɔ a kɔnɔ.
> - Le vieux sait bien lutter quand il reste assis, dès qu’il se lève, il ne sait plus rien.

> Kɔnɔ #dugu tɛ #san ye.
> - Le village de l’oiseau n’est pas dans le ciel.

> Sunɔgɔbagatɔ #kunun ka #di ni tugubaganci ye.
> - Le vrai dormeur est plus facile à réveiller que celui qui fait semblant.

> Bɛɛ wulituma ye, i ka sɔgɔmada ye.
> - L'heure de réveil de chacun est son matin.

> #Mɔgɔ be #tagama bi, #sini i be ŋunuma, i be ŋunuma bi, sini i be tagama.
> - L’homme marche aujourd’hui sur deux pieds, demain à quatre pattes ; il va à quatre pattes aujourd’hui, demain il marche sur deux pieds.

> #Mɔgɔ be #fasa k’a sɔrɔ #bana tɛ i ra.
> - L’homme peut maigrir sans être malade.

> #Mɔgɔ kɔrɔtɔnin tɛ #fa to gwannin na, #sani to ye #suma o b’a sɔrɔ a wurira.
> - L’homme pressé ne mange pas à sa faim quand le plat est chaud, avant que le plat ne se refroidisse il se lève.

> Tiɲɛfɔbaga #ma #man di.
> - L’homme qui dit la vérité n’est pas aimé.

> #Mɔgɔ kumabari ta #kuma b’a kɔnɔ.
> - L’homme qui ne parle pas, a sa parole dans son cœur.

> #Mɔgɔ be i diyanyako lɔn, nka Ala le be i nafako lɔn.
> - L’homme sait ce qui lui plaît, mais c’est Dieu qui sait ce qui est pour son bien.

> #Surugu ko : « serilon #na #muso bɛɛ #cɛ kaɲi. »
> - L’hyène dit « le jour de la fête toutes les femmes sont belles. » 

> #Surugu ko ale tɛ mɔgɔnafin #da domu
> - L’hyène dit qu’elle ne mange la bouche de l’homme.

> #Surugu ko, ko #mɔgɔ ka #kan k’i #yɛrɛ #degi sensabatagama ra, #lon dɔ kosɔn.
> - L’hyène dit qu’il est bon d’apprendre à marcher à trois pattes, en prévision de certains jours.

> #Surugu #senna ka #teri tabadagamin ma.
> - L’hyène n’a pas le temps de fumer une pipe.

> #Surugu tɛ forotomugu #lɔn domufɛn ye.
> - L’hyène ne reconnait pas la poudre de piment comme nourriture.

> #Surugu #fanin be basen kɛ tagamabere ye.
> - L’hyène rassasiée, se sert de la patte de cabri comme simple canne.

> #Surugu #fanin be banogo kɛ kannakɔnɔn ye.
> - L’hyène rassasiée se sert des intestins de cabri comme collier.

> #Surugu #fanin be banaji fiyeere.
> - L’hyène rassasiée vend le bouillon de cabri.

> #Karan ni kolɔn tɛ #kelen ye.
> - L’instruction est différente de la sagesse.

> Lɔnnin ye kɔlɔn le ye, #dan tɛ min na.
> - L’instruction est un puits qui n’a pas de fond.

> #Ɲa tɛ #doni ta, nka a be doni gwiriman lɔn.
> - L’œil ne porte pas de bagage, mais il sait reconnaître un bagage lourd.

> #Ji be #jaba ɲa, a #ma #fɔ ko i ye i tunun k’a #turu dɛ.
> - L’oignon aime l’eau, mais on ne dit pas de plonger sous l’eau pour le cultiver.

> Ele ka #kɔnɔnin min #fan cɛ, dɔ ka o #bamuso domu.
> - L’oiseau dont (tu te vantes d’avoir) ramassé les œufs, quelqu’un d’autre a mangé sa mère.

> Sin tɛ kɔnɔ fɛ, nka Ala b’a #den baro.
> - L’oiseau n’a pas de mamelles, mais Dieu nourrit son petit.

> Mɛɛnɲanakɔnɔnin tɛ se ka siɲanakɔnɔnin #den ta.
> - L’oiseau qui dort tard ne peut pas prendre le petit de l’oiseau qui veille toute la nuit.

> Ni #tɛgɛ #ma #tigɛ #kama tɛ #da jɔsi.
> - Lorsque la main est la, l’épaule n’essuie pas la bouche.

> Ni #basa #kɔ #ma #tigɛ a tɛ digɛnda lɔ.
> - Lorsque la queue du margouillat n’est pas sectionnée, il ne se cachera pas dans un trou.

> #Fatɔ be #yaala n’a #den fiyereta ye #tuma min na, o le y’a santuma ye.
> - Lorsque le fou promène son fils pour le vendre c’est à ce moment qu’il faut l’acheter.

> #Tinba b’a #yɛrɛ #sɛgɛ #bala ye.
> - L’oryctérope se fatigue (à creuser les trous) pour le porc-épic.

> #Daha kumunin lo #kabini a fitinin.
> - L’oseille est acide depuis qu’il n’est qu’une petite plante.

> #Surugu dɛsɛra koro min na, #wuru tɛ se ka o ta.
> - L’os que l’hyène n’a pas pu croquer, le chien ne peut pas le prendre.

> An ye taga, an ye taga, a #laban ye #bori ye.
> - Marchons vite ! Marchons vite ! C’est qu’on finira par courir.

> Yirikurun #mana mɛɛ #ji ra, #cogo o cogo a tɛ kɛ #banba ye.
> - Même si la bûche reste longtemps dans l’eau, elle ne deviendra pas un crocodile.

> #Hali ni #faninya ka #san #tan kɛ #tagama ra, #tiɲɛ ta sɔgɔmada #kelen tagama be #kun a ra.
> - Même si le mensonge fait dix ans de marche, une seule matinée de marche de la vérité le rattrape.

> #Hali ni #lonancɛ ɲakiri #bonya ka borokuru bɔ, #dugulen #fiyentɔ ka #fisa n’a ye.
> - Même si l’œil de l’étranger est gros comme un poing, l’autochtone aveugle voit mieux que lui.

> #Hali n’i #jugu ye sonzannin ye, a #lɔn ko a be bori.
> - Même si ton ennemi est un lièvre, reconnais au moins qu’il sait courir.

> #Hali n’i ka fagobagatɔ #bla turubara le kɔnɔ, a koyokoyonin be bɔ.
> - Même si tu mets le fainéant dans une boite de pommade, il en sortira le corps sec.

> #Hali ni #wari tɛ i fɛ, sanin tɛ i fɛ, ni i ni mɔgɔw be #ɲɔgɔn fɛ, i jigimafara.
> - Même si tu n’as pas d’argent, tu n’as pas d’or, si tu vis avec les hommes, tu es heureux !

> #Hali n’i #ma #fa to ra, n’i ka #miiri i sigituma ma, i be wuri.
> - Même si tu n’es pas rasasié, quand tu penses depuis le moment où tu manges, tu dois te lever.

> #Hali n’i ka i kinbiri gwan ka bagabagabɔbaga fo, o tɛ dɔ bɔ #sisɛ #sɔngɔ ra.
> - Même si tu te mets à genoux pour saluer l’éleveur de poule qui cherche les termites pour ses poules, cela ne va pas diminuer le prix des poulets.

> #Jugu #kun #kelen ka #fisa ɲinjugu ye.
> - Mieux vaut un vrai ennemi qu’un faux ami.

> Su #fɛ yirikurun finman, a tɛ #mɔgɔ mina, nka a b’i lasiran.
> - Morceau de bois noir la nuit, ça n’attrape pas l’homme, mais ça fait peur.

> #Mɔgɔ t’a #bolo kɔnɔ #jɛgɛ #bila a #sen #jukɔrɔ jɛgɛ ɲɛn.
> - Ne laissons pas le poison qui se trouve dans nos mains pour prendre celui qui se trouve sous notre pied.

> #Mɔgɔ #man #kan ka #don #kɛrɛ ra ko #jo be i fɛ, i be don kɛrɛ ra le ko mɔgɔw b’i kɔ.
> - N’entre pas dans une querelle parce que tu as raison, entre parce que tu as des gens derrière toi.

> Layiritabariya ka #fisa ni layiridafabariya ye.
> - Ne pas faire de promesse vaut mieux que d’en faire sans la tenir.

> Kɛrɛkɛcɛ kelen, burufiyɛbaga kɔnɔntɔn, ɲagari be o #kɛrɛ sa.
> - Neuf joueurs de cor pour un seul guerrier, trop de joie va annuler cette guerre.

> Ta #ma mɛnɛ, #sisi ma bɔ.
> - Ni feu, ni fumée.

> An #kana #ɲinɛ an #yɛrɛ kɔ.
> - N’oublions pas nous mêmes.

> An be ala bolo.
> - Nous sommes dans la main de Dieu.

> Sibankɔnɔ tɛ laadikan mɛn.
> - Oiseau qui cherche la mort n’écoute pas de conseils.

> Tagayɔrɔjan tɛ se ka #mɔgɔ #firi i #facɛ ta bulonda ma.
> - On a beau voyager pour aller loin, on ne peut pas ne pas reconnaître la porte de la maison paternelle.

> #Mɔgɔ be #lon dɔ kɔlɔnnin sogi, lon dɔ minlɔgɔnin kosɔn.
> - On creuse un petit puits, pour la petite soif d’un jour.

> Ko sonzannin ta ye lo ye, ni lo ka bɔ ko sonzannin y’a #ɲa datugu !
> - On dit que le masque appartient au lièvre, mais quand le masque sort, on dit au lièvre de se fermer les yeux !

> Ko tɛ ko sa, nka #fiyentɔya be donsoya sa.
> - On dit que rien ne peut rien empêcher, mais la cécité empêche d’être chasseur.

> #Mɔgɔ ka #kan ka i senkɔrɔyɔrɔ #flɛ #sani i ye cun.
> - On doit regarder par terre avant de sauter en bas.

> #Mɔgɔ ka #kan ka i #pan ko dɔw kunna, #janko i ye #dunuɲa diyabɔ.
> - On doit sauter par-dessus certaines choses afin de pouvoir jouir de la vie.

> #Mɔgɔ tɛ so #san a senkan fɛ.
> - On n’achète un cheval en se basant sur le bruit de ses pas.

> #Cɛɛ tɛ #cɛ ka ban.
> - On na pas fini de découvrir l’homme.

> #Mɔgɔ tɛ hɛra sɔrɔ n’i #ma ɲani.
> - On n’a pas le bonheur sans souffrance.

> #Mɔgɔ tɛ gwɔncɛkɔrɔba #degi tigawɔrɔ ra.
> - On n’apprend pas au vieux singe à décortiquer les arachides.

> #Mɔgɔ tɛ #bakɔrɔnin #karifa #surugu ma.
> - On ne confie pas le bouc à l’hyène.

> #Domuni diman #diya tɛ lɔn, #fɔ n’a banna #minan kɔnɔ.
> - On ne connait le bon goût de la nourriture, que lorsqu’il finit dans la marmite.

> #Mɔgɔ tɛ mɔgɔ #lɔn n’i #ma se o ta so.
> - On ne connait pas quelqu’un sans arriver chez eux.

> #Bori ni #bobara (yaga) tɛ bɛn.
> - On ne court pas en grattant les fesses.

> #Mɔgɔ tɛ i ɲaw #di i buranw #ma ka flɛri kɛ n’i #tɔn ye.
> - On ne donne pas ses yeux à ses beaux-parents, pour regarder avec sa nuque.

> #Mɔgɔ tɛ ko kɛ ka nimisa.
> - On ne faire rien pour avoir des remords.

> #Mɔgɔ tɛ #den dɔn, k’a kɔsin i #yɛrɛ ma.
> - On ne fait pas danser un bébé (sur les genoux) en ayant son dos vers soi-même.

> #Wagati sebari tɛ karaba.
> - On ne force pas un temps qui n’est pas encore arrivé.

> #Mɔgɔ tɛ i ta samafagamugu #cɛn sonzannin na.
> - On ne gaspille pas sa cartouche pour éléphant sur un lapin.

> #Mɔgɔ tɛ #bolo #jɛgɛ #bila a se #kɔrɔ jɛgɛ ɲɛ.
> - On ne laisse pas le poisson qui se trouve dans le main pour prendre celui qui est entre tes pieds.

> #Mɔgɔ tɛ #samara bɔ i ka sokɔnɔ #ɲinan kanma.
> - On n’enlève pas sa chaussure pour courir après la souris de sa maison.

> #Tɔnɔ #fla tɛ sɔrɔ #sisɛfan #kelen na.
> - On ne peut pas avoir deux bénéfices dans le même œuf.

> #Mɔgɔ tɛ se ka to bagamin #na ka yɛrɛko.
> - On ne peut pas boire la bouillie et rire en même temps.

> #Kun tɛ #ci kuntigi #yɛrɛ kɔ !
> - On ne peut pas caser la tête en l’absence de son propriétaire.

> #Mɔgɔ tɛ mɔgɔ #lɔn ni aw #ma #jɛn ko ra.
> - On ne peut pas connaître quelqu’un si vous n’avez rien fait ensemble.

> Dencɛ #wolonfla be musokɔrɔba min fɛ, o ta banna #tɔgɔ tɛ se ka fɔ, a be #fɔ ko « musokɔrɔba ta bana. »
> - On ne peut pas dire le nom de la maladie, de la vielle femme qui a sept fils ; on dit seulement : « la maladie de la vielle. »

> #Mɔgɔ tɛ #don sulaw ta #toron na, ko dɔ kukala #kana #maga i ra.
> - On ne peut pas entrer dans le jeu des singes, sans que la queue de l’un ne te touche.

> #Mɔgɔ tɛ kɛ #kɔgɔ ye ka bɔ nan bɛɛ ra.
> - On ne peut pas être du sel pour saler toutes les sauces.

> #Mɔgɔ tɛ kɛ i #yɛrɛ #jugu ye.
> - On ne peut pas être son propre ennemi.

> #Mɔgɔ tɛ i #tere kɛ, ka mɔgɔ #wɛrɛ tere kɛ.
> - On ne peut pas faire son temps, et faire le temps de quelqu’un d’autre.

> #Ji bɔnin tɛ se ka cɛ.
> - On ne peut pas ramasser l’eau versée.

> #Kun tɛ #li a #tigi kɔ.
> - On ne peut pas raser la tête sans son propriétaire.

> Wɔrɔsɔ bɔrɔraman, #mɔgɔ tɛ o si lɔn, nka fɛn dɔ kurunin be #bɔrɔ kɔnɔ, i b’o lɔn.
> - On ne peut pas reconnaître une faucille dans un sac, mais on peut au moins savoir qu’il y a un objet courbe dans le sac.

> #Mɔgɔ tɛ to kɔlɔn kɔnɔ ka i bencogo #ɲa fɔ.
> - On ne peut pas rester au fond d’un puits et expliquer comment on est tombé dedans.

> #Mɔgɔ tɛ to yiri ra ka yiri sɔrɔ, nka mɔgɔ be to mɔgɔ ra ka mɔgɔ sɔrɔ.
> - On ne peut pas rester sur un arbre et atteindre un autre arbre, mais on peut rester avec une personne et atteindre une autre.

> #Mɔgɔ tɛ #kɔ #jan #tigɛ n’i #ma i kɔ flɛ.
> - On ne peut pas traverser une large rivière sans regarder en arrière.

> #Mɔgɔ tɛ #ɲɛgɛnɛ kɛ, #banba ye bɔ o ra ka i mina.
> - On ne peut pas uriner et qu’un crocodile en sort pour vous attraper.

> Solikabɔ #fla tɛ se ka kɛ sɔgɔmada #kelen na.
> - On ne peut se lever tôt deux fois dans la même matinée.

> #Mɔgɔ tɛ bɔ dundunfɔyɔrɔ ra ka #na i kɔnɔbara #fɔ so kɔnɔ.
> - On ne quitte pas auprès du tam-tam, pour aller taper son ventre à la maison.

> #Kasi ni #ɲaji tɛ #lɔn #sanji jukɔrɔ.
> - On ne reconnait pas des pleurs et des larmes sous la pluie.

> #Mɔgɔ tɛ #jakuma #lɔgɔ girinwagati lɔn.
> - On ne sait pas à quel moment un marché de chats sera troublé.

> #Mɔgɔ tɛ to ko ra k’a #diya lɔn.
> - On ne sait pas le goût d’une chose tant qu’on est dedans.

> #Hakiri tɛ dan, nka a be kɔrɔsiyɛn.
> - On ne sème pas la sagesse, mais on en enlève les mauvaises herbes.

> #Mɔgɔ tɛ gwan mɔgɔ #kɔ gwansan, mɔgɔ #fana tɛ #faran mɔgɔ ra gwansan.
> - On ne suit pas quelqu’un sans cause, on ne se sépare pas de quelqu’un sans cause.

> #Mɔgɔ tɛ #kala ye, ka i #ɲa #turu a ra.
> - On ne voit pas la paille pour aller se piquer les yeux dessus.

> I tɛ #foyi sɔrɔ n’i #ma kuma.
> - On n’obtient riens sans parler.

> #Mɔgɔ be baroda dɔ ra, nka #ɲanafin be i ra.
> - On peut être dans une compagnie, et s’ennuyer.

> #Mɔgɔ be se ka kɛ #kolɔnbaga ye, nka i tɛ se ka kɛ ko bɛɛ lɔnbaga ye.
> - On peut être un savant, mais on ne peut pas tout savoir.

> #Mɔgɔ be #tugu ka ŋuna, nka i tɛ se tugu ka wɔsi.
> - On peut gémir par exprès, mais on ne peut pas transpirer par exprès.

> #Kun be #li nka kunnadiya tɛ li.
> - On peut raser la tête, mais on ne peut pas raser le chance (qui est dans la tête).

> #Mɔgɔ be #ban ko dɔ ma, k’a sɔrɔ i m’a #fɔ « n’ tɛ ».
> - On peut refuser une chose, sans dire « non !»

> « La n’ na » #man jugu ; « kɛ n’ #sɔn ye » le ka jugu.
> - On peut t’accuser, ce n’est pas grave, mais quand ce qu’on dit devient ton caractère, c’est ce qui est grave.

> #Mɔgɔ be Ala ye #hali n’i #ma jan.
> - On peut voir Dieu, sans même se coucher sur le dos.

> #Mɔgɔ b’i #ɲa ko, #hali n’a cinnin lo.
> - On se lave les yeux, même si on est aveugle.

> #Kuma #man di, kumabaliya man di.
> - Parler n’est pas bien, se taire non plus !

> #Kuma #fɔ #koɲuman ni #tiɲɛ tɛ #kelen ye.
> - Parole bien dite, n’est pas égale à vérité.

> #Mɔgɔ si #kelen #fa tɛ Ala ye.
> - Personne n’a Dieu pour père à lui tout seul.

> #Mɔgɔ m’a #fɔ ko lo bɔra, lo ko : « N’bɔra !».
> - Personne n’a dit que le masque est sorti et le masque lui-même dit : « Je suis sorti !»

> #Mɔgɔ si t’a #fɔ ko : « Ala ye baraka #don #sama ra !»
> - Personne ne dira : « Que Dieu donne de la force à l’éléphant !»

> #Mɔgɔ si t’a ta dugusira #yira n’a numanboro ye
> - Personne ne montre le chemin de son village avec la main gauche.

> #Mɔgɔ tɛ #surugu #lɔgɔ wurituma lɔn.
> - Personne ne sait à quel moment le marché des hyènes ferme.

> #Dɔɔni dɔɔni, kɔnɔ b'a ɲaga dan.
> - Petit à petit, l'oiseaux fait son nid.

> #Dugumɛnɛnin #sɔnkɔrɔ ye #kinin ye.
> - Piquer, est une vieille habitude de la fourmi.

> Mɔgɔdarabɔsanji tɛ #fin terebɔ fɛ.
> - Pluie de provocation ne commence pas à l’est.

> #Lɔgɔ banna, o #ma #ɲi yirimɔgɔnin daa.
> - Plus de bois de chauffage n’est pas une bonne parole dans la bouche d’une statuette de bois.

> Sɔrɔ be fɛnɲini juguya.
> - Plus on trouve plus on cherche.

> Farata ɲininkari #kojugu b’a lakasi.
> - Poser trop de questions à l’orphelin finit par le faire pleurer.

> Ɲiningalikɛla tɛ firi.
> - Poseur de question ne se trompe pas.

> #Kuma bɛɛ n’a fɔbaga lo.
> - Pour chaque parole il y a la personne pour la dire.

> N’i k’a #mɛn ko ne #ɲɔgɔn siyɛntabaga tɛ, i #sen #fla be #dugu #ma le.
> - Pour être le meilleur lutteur, il faut d’abord avoir ses deux pieds à terre.

> #Foro #damina ye wagabɔn ye.
> - Pour faire un champ, on commence par enlever les herbes.

> N’i ko sigi ye diya, #fɔ dɔ y’i la.
> - Pour que la cohabitation soit agréable, il faut que l’un se couche.

> « Kuma, mun le ka i ɲa ?» « N’fɔcogo !» « Kuma, mun le ka i tiɲɛ ?» « N’fɔcogo !»
> - Pourquoi la parole t’a rendue bonne ? -- La manière de dire !, pourquoi la parole t’a rendue mauvaise ? -- La manière de dire !

> Siradarakɔlɔn, n’i tagatɔ #ma min a ra, i sekɔtɔ be min a ra.
> - Puits au bord du chemin, si tu n’y bois pas en allant, tu y boiras au retour.

> Ni #jende ka yiri tigɛ, a #kala b’a kɔrɔfɔ.
> - Quand la hache coupe l’arbre, la manche de la hache s’en plaint.

> Su min be diya, o be #lɔn #kabini a wulada.
> - Quand la nuit va être bonne on le reconnait dans la soirée.

> Ni #kuma o kuma ɲagaminna, cɛkɔrɔbaw le b’a ɲanabɔ.
> - Quand la parole est mélangée, c’est les vieux qui l’arrangent.

> Ni sa #kunkolo tigɛra, a #tɔ kɛra jurukisɛ ye.
> - Quand la tête du serpent est coupée, il n’est plus qu’un corde.

> Mɔgɔtigi #nakan tɛ firi.
> - Quand le célébrité arrive on le sait.

> Ni so be i ben, i tɛ a #toro ye.
> - Quand le cheval va te faire tomber, tu ne vois pas ses oreilles.

> Ni #basi ka denmisɛn saran, o b’a #fɔ ko a domukojuguya lo, nka n’a ka mɔgɔkɔrɔba saran, o b’a fɔ ko #ji le m’a labɔ.
> - Quand le couscous prend l’enfant à la gorge, on dit c’est parce qu’il est gourmand ; mais quand il prend un adulte à la gorge, on dit c’est parce qu’il n’y a pas assez d’eau dans le couscous.

> « Baara #wɛrɛ tɛ ne ra ni #nin tɛ », o #kuma ka #jugu wokɔnɔfɛn #ma dɛ !
> - Quand le creuseur dit : « Je n’ai rien d’autre à faire que ça !» Cela fait peur au rat qui est dans le trou.

> #Wara kɔngɔtɔ #ma ɲi, a #fanin ma ɲi.
> - Quand le fauve est affamé il est dangereux, quand il est rassasié il est dangereux.

> Ni sosonin be dɔnkɛ, a b’a senkalaninw kɔrɔsi.
> - Quand le moustique danse, il fait attention à ses petites jambes.

> Ko to #ɲɔgɔn ta ra, o be karatabugu #mɛɛn si ra.
> - Quand les habitants de la case se soumettent les uns aux autres, ça donne longue vie à la case.

> Ni #sula #boro dɛsɛra #zaban min ma, a b’a #fɔ ko o kumunin lo.
> - Quand le singe ne peut pas atteindre la côcôta il dit que ce fruit-là est acide.

> Ni sunbara ka diya, nɛrɛbɔbaga #tɔgɔ tɛ fɔ.
> - Quand le soumbala est bon, on ne parle pas de celle qui a récolté le néré.

> Ni #dundun ka faran, a be kɛ duduntigi #kelen ta ye.
> - Quand le tam-tam se déchire, ça devient l’affaire du batteur seul.

> Ni #san ka pɛrɛn bɛɛ b’a #kun le mina.
> - Quand le tonnerre gronde, c’est sa propre tête que chacun protège.

> #Surugu ta sogoflagwɛn, dɔ be #taga k’a dan.
> - Quand l’hyène court après deux proies, une lui échappe.

> Ni ko #jɛgɛ ka #di i ye, #taga #kɔ #da ra.
> - Quand on aime du poison, il faut aller le pêcher soi-même.

> Ni #san ɲagamina, #kun tɛ karojate ra tugun.
> - Quand on a perdu le fil des années, il ne sert plus à rien de compter les mois.

> Ni #san ɲagamina, #kun tɛ karojate ra tugun.
> - Quand on a perdu le fil des années, il ne sert plus à rien de compter les mois.

> #Mɔgɔ tɛ ko diman #dabla gwansan.
> - Quand on arrête de faire quelque chose qu’on aimait, il y a une raison.

> N’a #fɔ ra ko #mɔgɔ ka di, a #sɔgɔ tɛ, a #kɛwale lo.
> - Quand on dit que les hommes c’est bon, il ne s’agit pas de leur chair, c’est leurs actes.

> Digiri #dan ye #dɛndɛn ye.
> - Quand on pousse jusqu’au mur, c’est la fin.

> #Mɔgɔ koji bɛɛ tɛ #bɔn i kan.
> - Quand on se lave, toutes les goûtes d’eau ne touche pas le corps.

> Ni #fugula fiyeerera k’a #wari #don #kɔgɔ ra, o bɛɛ ye #kunkolo ta #nafa le ye.
> - Quand on vend le chapeau pour acheter du sel, tout est à l’avantage de la tête.

> N’i k’a #mɛn ko : « An ye #taga #kunu ta #yɔrɔ ra », kunu ta tun diyara.
> - Quand quelqu’un dit : « Allons à l’endroit d’hier », c’est que cela avait été bon hier.

> N’i ka #mɛn ko #dunuɲa ka di, i b’a dimanyɔrɔ le ra.
> - Quand quelqu’un dit que le monde c’est bon, c’est qu’il est au bon endroit.

> #mɔgɔ mirila i ma…  i #miiri mɔgɔ ma. 
> - Quand quelqu'un pense à toi, pense à eux.

> N’i k’a #mɛn ko : « n’ bonbonsi ka #jan tasumafiyɛ ma », a fiyɛbaga le be i boro.
> - Quand tu dis que ta barbe est trop longue pour souffler sur le feu, c’est que tu as quelqu’un d’autre pour le souffler.

> Ni #ma sa, ko be #juru i kan.
> - Quand tu es en vie, tu as la dette de toute chose.

> N’i ka #nɛn ye borokandenw #cɛ ra, ni #li tɛ yi, #kɔgɔ le be yi.
> - Quand tu vois la langue entre les doigts, c’est qu’il y a du miel, ou du sel.

> N’i ka lagwɛrikɛbaga ye a be kasira, a #ma #koɲuman le ye lagwɛri ra.
> - Quand tu vois que le charlatan se met à pleurer, c’est qu’il n’a pas vu quelque chose de bon dans la divination.

> N’i k’a ye ko tɛgɛcira sangafobaga #ɲa kan, i b’a sɔrɔ a k’a #da #don cɛntakokuma ra le.
> - Quand tu vois qu’on gifle celui qui est venu saluer aux obsèques, c’est qu’il s’est mêlé aux problèmes d’héritage.

> Ni ko ka gwan, ka gwan, a bantɔ lo.
> - Quand une chose chauffe, chauffe, c’est qu’il arrive à sa fin.

> Tiɲɛkuma be #mɔgɔ dɔ kɔnɔ, nka a fɔda t’a fɛ.
> - Quelqu’un peut avoir une parole de vérité mais il n’a pas la bouche pour le dire.

> #Mɔgɔ dɔ be #faninya #tigɛ i ye, #sabu a be #malo i ma, walama a be siran.
> - Quelqu’un te ment, simplement parce qu’il a honte de toi, ou qu’il a peur.

> Mun le ka #di #nanbaratɔ ye, « na n b’i ta » kɔ ?
> - Qu’est ce que le paralysé aime, si ce n’est « viens que je te prenne !»

> #Derege min tɛ i kanna, i #boro be mun #ɲini o #jufa ra ?
> - Qu’est ce que ta main cherche dans la poche du boubou de quelqu’un d’autre ?

> Fagama #jo n’a #jalaki bɛɛ ye a jo ye.
> - Qu’il ait tort ou raison, le chef a toujours raison.

> Nataba ni lɔgɔlamini.
> - Qui veut beaucoup en payant moins, fera plusieurs tours dans le marché.

> A flɛ, a flɛ, n’i #ma ye, n’a turura i #ɲa ra i #bena a ye cɔ.
> - Regarde ! Regarde ! Si tu ne vois pas, quand l’objet va te piquer dans l’œil tu le verras.

> #Tiɲɛ ye #naloman #boda #flɛ ye, nka « n be #taga n’ somɔgɔw #wele ka na », i #bena a sɔrɔ ko naloman taga ra dɛ.
> - Regarder l’anus du sot, c’est vrai, mais dire : « Je vais chercher mes parents pour venir le regarder !» vous viendrez trouver que le sot est parti !

> Sigi tɛ #mɔgɔ sɔn, n’a kɛbaga t’i boro.
> - Rester assis ne récompense personne a moins que tu n’aies quelqu’un qui travaille pour toi.

> Ka #segi kumakɔrɔ kan, o ye dalamaga le ye.
> - Retourner à la même parole déjà dites, revient à remuer simplement les lèvres.

> Ka #segi i cɛkɔrɔ #ma o tɛ #kojugu ye, nka a be #mɔgɔ lɔnbaga caya.
> - Retourner chez ton ancien mari, n’est pas grave, mais ça fait que tu seras connue de beaucoup trop de gens.

> #Foyi tɛ to #kuraya ra.
> - Rien ne reste toujours à l’état neuf.

> #Foyi tɛ #yi ni #kɔrɔ tɛ min na.
> - Rien ne se fait sans cause.

> #Girindi tɛ #fa ye, nka kɔngɔtɔ t’a kɛ.
> - Roter ne veut pas dire qu’on est rassasié, mais celui qui a faim ne le fait pas.

> « A’ y’a mina, a b’a #da ra », o ye #tiɲɛ ye, nka « a’ y’a #mina a b’a kɔnɔ », o ye #faninya ye.
> - « Saisissez le, il a ça dans sa bouche !» Cela peut être vrai. Mais « Saisissez-le, il a ça dans son ventre !» C’est pas fondé.

> Aw ye Julaw mina ! aw ye Julaw bla ! #Nin bɛɛ be nankɔgɔ le ɲinina.
> - « Saisissez les commerçants ! Laissez les commerçants !» Tous ceux-là cherchent du sel à acheter.

> Ni #nin yiriboro tun tɛ ne tun #bena #kɔnɔnin nin faga, ni nin yiriboro tun tɛ #fana kɔnɔnin tun tɛ sigi yan.
> - Sans cette branche, j’aurai tué cet oiseau, mais sans cette branche, l’oiseau ne s’assoirait pas là non plus.

> Sigi ka mɛɛn, #tiɲɛ be lɔn.
> - S’assoir longtemps fait connaître la vérité.

> #Ɲɔgɔn bamusodomu le be #subagaya diya.
> - Se manger les mères les uns des autres rend la sorcellerie intéressante.

> Jatigiɲininka ka #fisa lonanya ra.
> - Se renseigner auprès d’un autochtone est bon pour l’étranger.

> N’i be #sogo #domu ra, o ya sɔrɔ ko i ɲiiw #dafanin le.
> - Seul celui qui a toutes ses dents peut manger des grillades.

> #Mɔgɔ #ɲɛ o ɲɛ, i tɛ se k’i mankutu.
> - Si bien qu’on soit, on ne peut se louer soi-même.

> N’a #ma gwan, a tɛ suma.
> - Si ça ne chauffe pas, ça ne refroidit pas.

> Ni kɔnɔboritɔ ko #dugu taranna, i b’a sɔrɔ #kɔnɔbori tɛ #tiɲɛ ye le.
> - Si celui qui a la diarrhée dit que la nuit est trop avancée pour sortir, c’est qu’il n’a pas vraiment la diarrhée.

> Ni fɛn #ɲuman tɔmɔbaga ko a tɛ #ɲa a kɔ, o burunna min #fɛ o #bena a #yɛrɛ faga.
> - Si celui qui a ramassé l’objet ne veut plus s’en passer, et que dire de celui qui l’a égaré ? il va se tuer !

> Ni #biɲɛ ɲinibaga #yɛrɛ #sen lanin be biɲɛnkala kan, o biɲɛ tɛ se ka ye.
> - Si celui qui cherche l’aiguille a lui-même son pied dessus, on ne verra jamais cetta aiguille.

> N’i ka paraw ni bɔrɔkɔnɔbla bɛɛ kɛ #kelen ye, i #bena fɛn #ɲanaman dɔ #bla i #yɛrɛ kun.
> - Si dès le premier coup de bâton tu mets l’animal dans le sac, tu finiras par mettre dans ton sac un animal encore vivant.

> Ni Ala ka #sen #kari a b’a tagamacogo yira.
> - Si Dieu casse le pied, il montre comment marcher avec.

> Ni #kɛnɛsigiri #ma bɔ, kɛnɛsigiblan ye #sekɔ a #tigi ma.
> - Si la circoncision n’a plus eu lieu, qu’on retourne le caleçon de circoncision à son propriétaire.

> Ni jidaga ni jifiyɛ bɛɛ sera kɔlɔnda ra, #foyi #wɛrɛ #ma to so tugun.
> - Si la jarre d’eau et le gobelet à boire sont tous deux au bord du puits, Il n’y a plus rien qui reste à la maison.

> Ni jidaga ni jifiyɛ bɛɛ sera kɔlɔnda ra, #foyi #wɛrɛ #ma to so tugun.
> - Si la jarre d’eau et le gobelet à boire sont tous deux au bord du puits, Il n’y a plus rien qui reste à la maison.

> Ni #bolo ye #banba da, a kɛ #dɔɔni dɔɔni.
> - Si la main est coincée dans la gueule du crocodile, retire la avec délicatesse.

> Ni #dɔrɔ #kunna a #daga kɔnɔ, a ka #kan ka #kun a minbaga kɔnɔ.
> - Si la marmite a pu contenir le dolo, le ventre du buveur aussi doit pouvoir contenir le dolo.

> N’i ko i cɛkaɲi su fɛ, ni #dugu gwɛra i be ye.
> - Si la nuit tu dis « je suis beau » Quand il fera jour, on te verra.

> Ni #sanji ka i sɔrɔ togo #kolon kɔnɔ, a tɛ #lɔ tugun.
> - Si la pluie te trouve sous une cabane délabrée, elle ne s’arrête plus de tomber.

> Ni #sanji ka i sɔrɔ togo #kolon kɔnɔ, a tɛ #lɔ tugun.
> - Si la pluie te trouve sous une cabane délabrée, elle ne s’arrête plus de tomber.

> Ni sisɛnin #ma « Kus !» mɛn, a #na « Paraw » mɛn.
> - Si la poule n’entend pas la voix qui lui demande de quitter, elle entendra le coup de bâton qui lui demande de quitter.

> Ni #miiri bɔ ra #mɔgɔya ra, mɔgɔya banna.
> - Si la réflexion manque dans les relations humaines, Il n’y a plus de relations humaines.

> Ni yirimɔgɔnin ko : « lɔgɔ tɛ #tasuma ra !» Ale #yɛrɛ tɛ #lɔgɔ dɔ ye wa ?
> - Si la statue de bois dit qu’il n’ya pas de bois dans le feu, lui-même n’est-il pas du bois ?

> Ni #tuganin kɔnna ɲɔfiyɛbagaw ɲa, a #bena dɔ kɛ sigi ra.
> - Si la tourterelle arrive avant les vanneuses il attendra longtemps.

> Ni #fiyentɔ ko #san #fin na, i b’a sɔrɔ a be #dɔɔni yera.
> - Si l’aveugle dit que le ciel est sombre, c’est qu’il voit un peu.

> Ni #ji tun be #mɔgɔ gwɛ, kɔlɔnkɔnɔtɔri tun be arabu ye.
> - Si l’eau pouvait rendre blanc, le crapaud qui vit dans le puits serait un arabe depuis.

> Ni nafigi ka #don #mɔgɔ n’i ta #kurusi cɛ, i b’a bɔ k’a firi.
> - Si le calomniateur se met entre toi et ton pantalon, tu vas enlever ton pantalon et le jeter.

> Ni #juru be so #sen na, #fali b’a #dan #bori ra.
> - Si le cheval a la corde au pied, même l’âne peut courir plus vite que lui.

> Ni #tasuma ka #ba #tigɛ a #yɛrɛ ma, a be kɛ a fagabaga #boro kɔnɔgwan ye.
> - Si le feu a traversé le fleuve tout seul, celui qui va l’éteindre aura des problèmes.

> N’i k’a ye ko #tasuma be #ji gwan, fɛn le be o #fla ni #ɲɔgɔn cɛ.
> - Si le feu chauffe l’eau, c’est parce qu’il y a quelque chose entre les deux.

> Ni kɔnɔdimi kirisitigi #yɛrɛ kɔnɔ k’a dimi, dɔwɛrɛ tɛ o ra, a ye daji kunu.
> - Si le maître des formules magiques a lui-même mal au ventre, il n’y a rien d’autre que d’avaler sa salive.

> Ni #fiyentɔ #sen ka #juru tigɛ, #mɔgɔ tɛ #jigi a ra, nka n’a #ɲi ka tigɛ, #sariya b’a mina.
> - Si le pied de l’aveugle coupe la corde, on ne peut lui en vouloir, mais si ses dents coupent la corde, il est condamnable.

> Ni mɔgɔw ka i #wele ko « Sama !» i #kana a to i wele tun ko « sonzannin !»
> - Si les gens t’appellent « Eléphant !» ne leur donne pas l’occasion de t’appeler « Lièvre !»

> Ni #surugu ka i #muso ɲamɔgɔcɛ mina, #mɔgɔ #ma surugu ci, nka surugu ma #kojugu kɛ.
> - Si l’hyène attrape l’amant de ta femme, personne n’a demandé ça à l’hyène, mais ce que l’hyène a fait là n’est pas mauvais.

> Ni #hakɛ #mɛɛn na, i b’a sɔrɔ a #taga ra #bere #kɛnɛ le tigɛ.
> - Si l’offense tarde à être punie, c’est qu’elle est allée chercher un bâton frais.

> Ka i #sen #don #ji ra, ni o be #mɔgɔ kɛ bozo ye, ni ne ka n ta #fla #sama k’a bɔ, n’ tɛ kɛ i #yɛrɛ ta ye wa ?
> - Si mettre mon pied dans l’eau fera de moi un pêcheur, j’enlève donc mes deux pieds pour être libre.

> Ka #bakɔrɔnin dɔ falen dɔ ra, i b’a sɔrɔ #kelen #jogo le #ma ɲi.
> - Si on échange un bouc contre un autre bouc, c’est que l’un d’eux a un mauvais caractère.

> N’i #boro ka i #don ko ra i be bɔ, n’i #sen ka i don i be bɔ, nka n’i #da ka i don i tɛ bɔ.
> - Si ta main te met dans une affaire, tu peux t’en sortir, Si ton pied te met dans une affaire, tu peux t’en sortir, mais si ta bouche te met dans une affaire, tu ne peux pas t’en sortir.

> N’i #bamuso n’a #sinamuso be #kɛrɛ ra, i sago ye ko i bamuso y’a ben.
> - Si ta mère et sa rivale se battent, ton souhait est que ta mère la fasse tomber.

> N'i #tericɛ kɛla #sula ye, i ta dereke, a tɛ to #san fɛ.
> - Si ton ami est devenu singe, ta chemise ne restera pas en haut.

> N’i woroden kɛra sa ye, i b’a #siri i #yɛrɛ #cɛ ra le.
> - Si ton fils est un serpent, il faut en faire une ceinture autour de toi.

> N’i tɔgɔjugu kɔnna i ɲa, i be ko nka i tɛ gwɛ.
> - Si ton mauvais nom te devance, tu vas beau te laver, tu ne seras jamais propre.

> Bɛɛ y’a ye, o ka #fisa #mɔgɔ #kelen y’a ye.
> - Si tous voient, c’est mieux qu’une seule personne voie.

> Ni #mɔgɔ saba ka #jɛn #kurusi #kelen na, dɔ ta lebu le b’a ra.
> - Si trois personnes partagent une culotte, l’un court le danger de rester tout nu un jour.

> N’i ka #ɲinan jalaki, i ye sunbara #fana jalaki.
> - Si tu accuse la souris, il faut aussi accuser le soumbala.

> N’i ka i #den #wele ko sama, a be yiri #fɔlɔ min kari, o be #ben ele #yɛrɛ le kan.
> - Si tu appelles ton fils « Eléphant », le premier arbre qu’il va faire tomber, tombera sur toi-même.

> Ni folo be i #kan na, n’i be ko, i b’a ko, n’i be mun i b’a mun, nka a #man #di i ye.
> - Si tu as le goitre, quand tu te laves, tu le laves, quand tu t’embaumes, tu l’embaumes, mais tu ne l’aimes pas.

> N’i ka jebagatɔ ta #nanji min, n’i #ma #fɔ #den bɔra a #fa fɛ, i n’a fɔ a bɔra a #ba fɛ.
> - Si tu bois la sauce de la femme qui vient d’accoucher, ou tu diras que l’enfant ressemble à son père, ou tu diras qu’il ressemble à sa mère.

> N’i be borira ko su #bena ko i ra, n’i benna ka i #woto kari, #dugu bena gwɛ i ra.
> - Si tu cours pour arriver avant la nuit, si tu tombes et te casse la jambe, tu arriveras après la nuit.

> N’i ko #mɔgɔ #ma ko :« Ala ye i sara !» N’a #digi ra a ra, i b’a sɔrɔ a ka min kɛ o ma #ɲi le.
> - Si tu dis à quelqu’un « que Dieu te le rende !» Si ça lui fait mal, c’est que ce qu’il a fait est mauvais.

> N’i k’a #fɔ i #sɔn #ma ko « n’ kɔnɔ yan, » n’i mɛnna a b’i sɛgɛrɛ.
> - Si tu dis à ton caractère : « Attends-moi ici !» si tu dures trop, il va te rejoindre.

> N’i ka #mɛn ko : « n ta #juru be banbandennin na », i k’a #layɔrɔ #kelen le lɔn.
> - Si tu dis que le petit caïman te doit, c’est que tu sais bien où il se couche.

> N’i ka #mɛn ko n’ tɛ faninyatigɛbaga lamɛn, a ɲajitɔ le m’a se i ma.
> - Si tu dis que tu n’écoutes jamais un menteur, c’est qu’il n’est pas venu chez toi en larmes.

> N’i k’a #mɛn ko :« N’tɛ #muso #kan mɛn, » i ni muso le #ma si.
> - Si tu dis que tu n’écoutes pas les femmes, c’est que tu n’as pas passé la nuit avec une femme.

> N’i ka #mɛn ko « n’ bɛ n ta #dɛgɛ #bla a ye kumu, » i be dɔ ta #kɛnɛ le kan.
> - Si tu dis que tu vas laisser ton dégué se fermenter bien, c’est que tu es en train de boire le dégué frais d’un autre.

> N'i #ma #ɲɛ i bɛ to #ji la, i ka to #ba la.
> - Si tu dois périr dans l'eau, va à la rivière.

> N’i ka #da ta #kumu bɔ a ra, a kɛra #flaburu gwansan ye.
> - Si tu enlèves l’acidité de l’oseille, ça devient une simple feuille.

> N’i ka #bon dɔ tuba #wuri ka #taga a biri dɔwɛrɛ kunna, n’a #ma #bonya a ma, a be #dɔgɔya a ma.
> - Si tu enlèves le toit d’une maison pour aller le mettre sur une autre, il sera ou trop large, ou trop étroit.

> Ka #san #tan kɛ faligwɛn na, #hali n’i #ma falikan mɛn, i be sirafaran #caaman lɔn.
> - Si tu fais dix ans derrière les ânes, même si tu ne connais la langue des ânes, tu sauras beaucoup de chemins.

> N’i ka ko kɛ sabaga ra, a be sa n’a ye a kɔnɔ, n’i ka ko kɛ mɔbaga ra, a be #mɔ n’a ye a kɔnɔ.
> - Si tu fais quelque chose au mourant, il mourra avec ça dans son cœur, si tu fais quelque chose à l’enfant, il grandira avec ça dans son cœur.

> N’i #bolo #kelen #wili ko i #mako tɛ #mɔgɔ ra, i ka tɔkelen #kɔrɔta ko mɔgɔ mako tɛ ra.
> - Si tu lèves une main en calmant que tu n’as besoin de personne, alors lève l’autre en disant que personne n’a besoin de toi.

> N’i ka #ji kɛ #naloman #kun #kolon kɔnɔ, a b’a susu.
> - Si tu mets de l’eau dans un mortier pour le sot, il va piler.

> N’i ka #tere #kuru bɛɛ kɛ ka musokɔrɔnin banba, #wula #fɛ n’i ka fofo, o fofori le be to a kɔnɔ.
> - Si tu mets la vieille femme au dos pendant toute la journée, et que dans la soirée tu la traînes à terre, c’est le fait de l’avoir traînée qu’elle va retenir.

> N’i #ma #ba #tigɛ ban, i #kana #banba neni.
> - Si tu n’as pas encore traversé le fleuve, n’insulte pas le crocodile.

> Ni #baara tɛ i ra, kɛ #mɔgɔ #fɛ min tɛ i fɛ.
> - Si tu n’as vraiment rien à faire, cherche la compagnie de quelqu’un qui ne t’aime pas.

> N’i ko i #ma fitirimawaliya lɔn, i ma ɲunan #caaman le kɛ.
> - Si tu ne connais pas l’ingratitude, c’est que tu n’a pas fait beaucoup de bonnes actions.

> Ni #kuma tɛ i da, a fɔ :« jɛgɛ be #ji ra !»
> - Si tu ne sais pas quoi dire, dis que le poisson vit dans l’eau.

> Ni #mɔgɔ #ma tuu gosi i t’a kɔnɔna #koo lɔn.
> - Si tu ne secoues pas le buisson, tu ne sais pas ce qu’il y a à l’intérieur.

> N’i ta #fɛ ka #mɔgɔ ye, i be #yɛlɛma #kongo kɔnɔ.
> - Si tu ne veux plus voir des hommes, déménages en brousse.

> N’i ka i #den #tɔgɔ la ko « baba kumandan », i #bena lɛnpo #sara a ye.
> - Si tu nommes ton fils « Monsieur le commandant » tu vas toi-même lui payer l’impôt.

> N’i ka #daga ta k’a #lase i kinbirikun na, Ala b’a lase i #kun na.
> - Si tu prends la marmite et la mets sur les genoux, Dieu t’aidera à la mettre sur la tête.

> N’i ka #dunuɲa sobagirin, i #na #ban k’a nɔnzitagama.
> - Si tu promènes le monde au grand galop tel un cheval tu finiras par le promener à pas hésitant tel un caméléon.

> N’i ka #dunuɲa yaala, n’i #ma fɛn sɔrɔ, i n’a fɛn lɔn.
> - Si tu promènes le monde, même si tu ne trouves rien, tu sauras bien des choses.

> N’i ka simanjugu dan, n’a #ma falen i ɲana, a be falen i #den ɲana.
> - Si tu sèmes le mauvais grain, s’il ne germe pas en ta présence, il germera en présence de ton fils.

> N’i ka dingakɔnɔfɛn bɔtɔ bugɔ, a be #sekɔ #dinga kɔnɔ.
> - Si tu tapes l’animal qui en train de sortir du trou il y retourne.

> N’i ka i #yɛrɛ kɛ #fiyentɔ ta dɔnkirilamɛnabaga ye, a be gwara i ra.
> - Si tu te fais répondeur du chant de l’aveugle, c’est de toi qu’il va s’approcher.

> #Wɔsi tɛ #lɔn #sanji jukɔrɔ.
> - Si tu transpires sous la pluie, on ne le saura pas.

> N’i ka i ta wurujugu faga, dɔ ta b’i kin.
> - Si tu tues ton chien méchant, celui d’un autre va te mordre.

> N’i #taga ra #dugu min #na ka taga a sɔrɔ bɛɛ #lɔnin be a #sen #kelen kan, n’i tɛ koɲinibaga ye, i #fana ka #kan ka #lɔ i sen kelen kan.
> - Si tu vas dans un village, et tu trouves que tous habitants se tiennent sur un seul pied, si tu ne cherches pas un problème, tu dois aussi te tenir sur un pied.

> N’i be #suma feere, ni #fitiri ka se i ye dɔ bɔ a #sɔngɔ ra, n’o tɛ a be gwala.
> - Si tu vends l’ombre, si le crépuscule arrive, diminue le prix, sinon plus personne n’achètera.

> N’i be #kurusi #karan #sula ye, i b’a kukala #bɔyɔrɔ kɛ a ra.
> - Si tu veux coudre une culotte pour un singe, il faut prévoir une place pour sa queue.

> N’i be ɲa, #ɲa ye #kelen ye, #kana ɲa ni #juguya jɛn.
> - Si tu veux être bon, il y a une seule façon de l’être, ne sois pas bon et mauvais à la fois.

> N’i ka #tuganin ye a ka #duga #fa neni, a jatigicɛ ye kɔnɔsogolon le ye.
> - Si tu vois que la tourterelle insulte le vautour, c’est qu’elle a l’autruche comme tuteur.

> N’i k’a ye ko #dugu diyara, dɔ le ye #naloman ye.
> - Si tu vois que le village est intéressant, c’est que quelqu’un est sot.

> N’i ka #dugulen ta filen sonya, i #bena #taga a fiyɛ #yɔrɔ #wɛrɛ le.
> - Si tu voles la flûte d’un autochtone, c’est que tu iras la jouer ailleurs.

> #Dɔn min ka #teri ni marakadɔn ye, kirikirisiyɛn #sen b’o ra.
> - Si une danse est plus rapide que la danse des maraka c’est que les danseurs doivent avoir un peu d’épilepsie.

> Ni a ye #li #dun nɔgɔn fɛ, ni jalaji nana, a ka o min #ɲɔgɔn fɛ.
> - Si vous manger du miel ensemble quand l’eau de cailcédrat vient, il faut en boire ensemble.

> #Moriya ye #cɛn ye, nka « n tɛ bɔ #misiri kɔnɔ », i b’a sɔrɔ jurunantigi dɔ be i kɔnɔna so.
> - Soyez un grand religieux, mais ne plus vouloir quitter la mosquée, c’est qu’un créancier t’attend à la maison.

> Ta be ele bonbosi ra, ta be i brancɛ bonbosi ra, #fɔ #hakiri le ye sɔrɔ o ko ra.
> - Ta barbe est en feu, la barbe de ton beau-père est en feu, Il faut beaucoup de sagesse ici.

> Ka Ala to #san fɛ, #duga tɛ #bin domu.
> - Tant que Dieu reste au ciel, le vautour ne mangera pas de l’herbe.

> Ni #mɔgɔ #ma ben, a kunnaminan #suguya tɛ lɔn.
> - Tant que l’homme ne tombe pas, on ne sait pas ce qu’il y a dans le panier qu’il porte.

> Ni #basa #ku #ma #tigɛ a tɛ #dinga #yɔrɔ lɔn.
> - Tant qu’on ne coupe la queue du margouillat, il ne saura pas où est son trou.

> Ni #tu #ma gwasi a kɔnɔnɔfɛn tɛ lɔn.
> - Tant qu’on ne frappe dans le buisson on ne sait pas ce qu’il y a dedans.

> Ni #mɔgɔ #ma sa, ko bɛɛ #juru b’i ra.
> - Tant qu’on n’est pas mort, tout peut arriver.

> Ni #ma sa, ko be #juru b’i kan.
> - Tant qu’on vit, on a la dette de toute chose.

> #Mɔgɔ yecogo be i ta batigɛsara nɔgɔya.
> - Ta physionomie peut diminuer le prix de ta traversée du fleuve.

> #Kunkolo min be #yaala o be bɛn n’a cibaga ye #lon dɔ.
> - Tête qui se promène trop, rencontrera un jour son casseur.

> #Mɔgɔ #kelen #ɲa wɔɔrɔ, o #tuma fiyen tɛ #dugu ra wa ?
> - Toi seul tu as six yeux ! Est ce qu’il n’y a pas d’aveugle dans le village ?

> #Mɔgɔ #sɔnkɔrɔ be #kiti #gwoya i ra.
> - Ton ancien caractère peut te faire perdre le procès.

> « Alahu Akbar, » bɛɛ tɛ #moriya ye.
> - Tous ceux qui disent « Alahu Akbar, » ne sont pas de grands religieux.

> Kongotaga bɛɛ tɛ donsoya ye.
> - Tous les « allés en brousse » ne sont pas pour la chasse.

> Sanfinin bɛɛ #ji tɛ na.
> - Tous les ciels nuageux ne donnent pas de la pluie.

> Dencɛ bɛɛ tɛ #cɛ ye.
> - Tous les fils ne sont pas des garçons.

> #Tiga bɛɛ ye #kelen ye, nka minw bɔ ra #fara kelen kɔnɔ, olugu ta bɔna le #ɲɔgɔn tɛ.
> - Tous les grains d’arachide se ressemblent mais ceux qui viennent de la même coque, se ressemblent le plus.

> #Cɛ #fin bɛɛ tɛ #numu ye, a #gwɛman bɛɛ tɛ #fla ye fana.
> - Tous les hommes noirs ne sont pas forgerons, tous les hommes clairs ne sont pas peulh.

> #Kanbele #cɛsiri #koɲuman bɛɛ tɛ kɛrɛkɛcɛ ye.
> - Tous les jeunes hommes aux ceintures bien attachées ne sont pas guerriers.

> #Kogo surunin bɛɛ ta #surunya tɛ bɔgɔdɛsɛ ye.
> - Tous les murs courts ne sont pas courts parce que les briques ont manqué.

> Kɔnɔ bɛɛ be #ɲɔ domu, nka min be si ɲɔtu ra, a be #fɔ o #ma ko ɲɔdomukɔnɔ.
> - Tous les oiseaux mangent le mil, mais celui qui dort dans le champ de mil, lui, on l’appelle mange-mil.

> Kurunkɔnɔmɔgɔ bɛɛ ŋaninya ye #kelen ye, Ala y’an #seko ɲa.
> - Tous les passagers d’une pirogue ont la même pensée, que Dieu permette une bonne traversée.

> Ni jɛgɛdenin ka fɛn o fɛn #fɔ #banba ta ko ra, a fɔbaga le k’a fɔ.
> - Tout ce que le petit poisson dit à propos du crocodile, il le dit en tant qu’un vrai témoin.

> Lɔfɛn bɛɛ ye lafɛn ye.
> - Tout ce qui est débout est appelé à se coucher un jour.

> So #ɲuman bɛɛ tɛ #mɔgɔ siyɔrɔ ye.
> - Toutes les belles maisons ne sont pas pour y passer la nuit.

> #Dugukolo #ɲuman bɛɛ tɛ sɛnɛkɛyɔrɔ ye.
> - Toutes les bonnes terres ne sont pas cultivables.

> Sagajigi ta #kunbiri bɛɛ tɛ siralɔnbariya ye.
> - Toutes les fois que le bélier baisse la tête ne veut pas dire qu’il ne sait pas la route.

> #Hakɛ bɛɛ tɛ #bla ka lahara kɔnɔ.
> - Toutes les offenses n’attendent pas le dernier jugement.

> #Kuma bɛɛ #ma #na fɔri kama.
> - Toutes les paroles ne sont pas venues pour être dites.

> Sufɛmisi bɛɛ ye misifin.
> - Toutes les vaches sont noires la nuit.

> #Jɛn o jɛn, faranlon b’a ra.
> - Toute union a son jour de séparation.

> Bɛɛ k’a #lɔn ko galama tɛ kongosɔgɔw minyɔrɔ ra.
> - Tout le monde sait que là où les animaux boivent il n’y a de louche.

> Fɛn bɛɛ #kura #cɛ ka ɲi, #fɔ kaburu kura.
> - Tout nouveau tout beau, sauf la tombe.

> Fɛn bɛɛ be #taga ka #segi #mɔgɔ muɲuribaga ma.
> - Tout part et reviens vers l’homme patient.

> Fɛn saba furura fɛn saba ma, ka fɛn saba woro : sigi ka fagantanya woro, #tagama ka #sɛgɛ woro, wɔsiji #ka sɔrɔ woro.
> - Trois se sont mariés à trois, pour mettre au monde trois : l’oisiveté a mis au monde la pauvreté, la marche a mis au monde la fatigue, la sueur a mis au monde le gain.

> #Toto #dinga sogibagajugu, #lon dɔ i be bɔ i #bɛmacɛ dɔ sukoro kan.
> - Trop grand creuseur de trou de rat, un jour tu risques de déterrer les os d’un de tes ancêtres !

> Ni #ba #nɔnɔ ka #diya i #da #cogo o cogo, i tɛ #sɔn ka #bakɔrɔnin #wele ko i belencɛ.
> - Tu as beau aimer le lait de cabri, tu ne vas pas quand même appeler le bouc ton oncle.

> #Bori min tɛ #dabla k’a ye, n’i #sen ka #di #cogo o cogo i be #mina o ra.
> - Tu as beau être bon coureur, dans une course qui n’en finit pas, tu seras un jour rattrapé.

> I ka #kɔ tigɛ, kɔ #wɛrɛ b’i ɲa.
> - Tu as traversé une rivière, mais il y en a une autre devant toi.

> Sagafaga tɛ wurufaga sa, #sogo bɛɛ n’a domubaga lo.
> - Tuer le mouton, n’empêche pas de tuer le chien ; chaque viande a son consommateur.

> I kɛra dɛgɛsusukolonkalan ye, i #kun gwɛra, i #ma #mugu domu.
> - Tu es devenu comme le pilon qui a pilé le dégué, ta tête est blanche, mais tu n’as mangé de farine.

> N’i tɛ #jɛgɛ #fɛ i be mun kɛ kɔda ra ?
> - Tu n’aimes pas le poisson, et qu’est ce que tu fais au bord du marigot ?

> « N’kun gwanana », o #ma se kariri ma, n’i ka banfla bɔ a be lɔn.
> - Tu n’as besoin de jurer que tu es rasé, si tu enlèves le bonnet on saura.

> I #wuri ka bɔ n ta siginan kan, o ye #tiɲɛ ye, nka wuri ka bɔ n #nakan kan, o ye #faninya ye.
> - « Tu occupes ma chaise, lève-toi !» Cela est vrai. Mais « tu occupes mon destin, lève-toi !», c’est faux.

> #Mɔgɔ be #jɔn #tu dɔ ra, nka i sirifu be bɔ a ra.
> - Tu peux mépriser une forêt, mais il y aura suffisamment de lianes dedans pour t’attacher.

> I be #cɛ dɔ ta k’a ben, nka a dugumatereke tɛ #kun i ra.
> - Tu peux terrasser un homme, mais perdre la vie en voulant le maintenir à terre.

> I be #mɔgɔ dɔ #jɛn muru, ka #na #muru n’a #kala bɛɛ #di dɔwɛrɛ ma.
> - Tu refuses de donner le couteau à quelqu’un mais tu donnes et le couteau, et sa manche à un autre.

> Yirinin min b’a #kelen na, #suma #ɲanaman tɛ sɔrɔ o ra.
> - Un arbre solitaire, ne donne pas beaucoup d’ombre.

> #Fiyentɔ jusuba, i #ɲa yɛlɛlon le fɔra i ye cɔ.
> - Un aveugle qui a un gros cœur ? On t’a certainement dit le jour où tes yeux s’ouvriront.

> Jalayiri ka #bon ni jalayiri ye, nka jalayiri #man #kuna ni jalayiri ye.
> - Un cailcédrat peut être plus gros qu’un autre cailcédrat, mais un cailcédrat n’est pas plus amère qu’un autre cailcédrat.

> Borokanden tɛ so ci, nka a b’a #yira a cibagaw ra.
> - Un doigt ne peut pas détruire la maison, mais il peut la montrer à ceux qui détruisent.

> #Borokumanden #kelen tɛ se ka bɛlɛkisɛ ta.
> - Un doigt ne peut pas lever un cailloux.

> Fɛnbanbari #diya tɛ #lɔn tugun.
> - Une affaire sans fin, finit par ne plus avoir de goût.

> N ɲun, n jigi, kunlakolontagama ka #fisa o bɛɛ ye.
> - Une charge, une décharge, marcher sans rien sur la tête est mieux que tout cela.

> Fɛn #kelen #tigi #caaman #ma ɲi, fɛn kelen tigintan ma ɲi.
> - Une chose avec beaucoup de propriétaires, c’est pas bien, une chose sans propriétaire, c’est pas bien.

> Ko baranin be ko sulanin sa.
> - Une chose imprévue qui arrive, nulle une chose qui était prévue.

> Badenyakɛlɛ be #sisi bɔ, nka a tɛ mana.
> - Une dispute entre frères fait de la fumée mais ne s’enflamme pas.

> #Tɔgɔ ye sɔrɔfɛn ye, tɔgɔ tɛ karabara.
> - Un grand nom, on vous l’attribue, un grand nom, on ne le force pas.

> #Cɛ #tan be #fani min karan, cɛ tan b’a bɔrɔtɔ, o fani tɛ #karan ka #ban ka ye.
> - Un habit cousu par dix hommes, et à nouveau déchiré par dix autres, on aura jamais fini de coudre cet habit.

> #Mɔgɔ be bɔ #jɔ fɛ, nka a ɲamisɛn ko tɛ.
> - Un homme peut passer à travers un filet, mais ce n’est pas le filet aux petites mailles.

> #Mɔgɔ #kelen #hakiri kelen, mɔgɔ #fla hakiri fla.
> - Un homme, une idée, deux hommes, deux idées.

> Dɔ ta sonzannin ye dɔ ta #sama ye.
> - Un lapin chez certains vaut un éléphant chez d’autres.

> Gbɛrɛgwɛrɛko sɔrɔ #man gwo.
> - Un malheur arrive facilement.

> #Mɔgɔ boritɔ be #pan ka #fla min tigɛ, dagasigi tɛ #masɔrɔ o ra tugun.
> - Un médicament qu’on coupe en courant, (s’agissant des feuilles) on n’a pas le temps de le préparer dans un canari.

> #Kɔnɔnin min k’a ta #fan la #lɔgɔfɛ ra, #mankan tɛ se ka o lawuri tugun.
> - Un oiseau qui a fait son nid dans un marché, quel bruit peut le faire fuir ?

> #Minan dɔ be jɛngɛ nka a kɔnɔfɛn tɛ bɔn.
> - Un panier peut se pencher sans que son contenu ne verse.

> I ta ye jɔncɛnin dɔ ye, nka a kɔnɔnɔkirisi tɛ i ta ye.
> - Un petit esclave peut t’appartenir, mais la formule magique qu’il connait ne t’appartient pas.

> Siranbagatɔ kungo #man dɔgɔ.
> - Un peureux ne manque de problèmes.

> Mun be #sen ni #kun se #ɲɔgɔn ma, ni lakojuguya tɛ ?
> - Un pied et une tête ne se toucheront jamais, si ce n’est à cause d’une mauvaise manière de se coucher.

> #Bala sara, ka bala #wɛrɛ sa, ni #jugunin tɛ ɲamɔgɔden ye, dugutigiya sera a #yɛrɛ #ma sa dɛ !
> - Un porc-épic est mort, un autre porc-épic est mort, si le hérisson n’est pas un bâtard, c’est lui qui doit être le chef de village maintenant.

> Ni dalateriya ka kungo min #lase #mɔgɔ ma, sennateriya tɛ se ka o bɔ i kunna.
> - Un problème que tu as créé en parlant trop vite, tu ne peux pas t’en défaire même en courant vite.

> #Bɔrɔ #lankolon tɛ jɔ.
> - Un sac vide ne tient pas debout.

> Misikuru gwɛnbere ye #kelen ye, nka #mɔgɔ kelen kelen bɛɛ n’a ta gwɛnbere lo.
> - Un seul bâton suffit pour conduire un troupeau, pour conduire des hommes il faut un bâton pour chacun.

> #Tiga torinin #kelen be a #tɔ bɛɛ #gwoya #mɔgɔ #da ra.
> - Un seul grain d’arachide pourri rend tous les autres mauvais dans la bouche.

> #Kɔnɔnin #kelen tɛ denminsɛnnin #fla ɲanagwɛ.
> - Un seul oiseau ne peut pas amuser deux enfants.

> #Sen #kelen tɛ #sira bɔ.
> - Un seul pied ne peut tracer un chemin.

> #Karisa #taga ra #lɔgɔ ra, ne #fana be taga, nka karisa taga ra ni min ye, ni ele #ma taga n’o ye, karisa #bena #segi ni min ye, ele #tɛna segi n’o ye dɛ !
> - Untel est allé au marché, moi aussi j’y vais, mais si tu n’as pas apporté au marché ce qu’un tel a apporté, tu ne pourras pas rapporter du marché ce qu’un tel va rapporter.

> #Dugu min mɔgɔw be surugukasi kɛ, ni ele ka bakasi kɛ yi, o b’i domu.
> - Un village où les habitants imitent le cri de l’hyène, si toi tu imites le cri d’un cabri dans ce village, tu seras mangé.

> « Na n ɲu ! #na n jigi !» Kunnankolon #tagama le #ɲɔgɔn tɛ.
> - « Venez me charger ! Venez me décharger !» Le mieux c’est de voyager sans bagages.

> #Kɔrɔ ni kɛnɛya, a #fla sɔrɔ #man di.
> - Vieillesse et bonne santé, il n’est pas facile d’avoir les deux.

> #Na an ye jɛn, o #kɔrɔ le ye na an ye kɛrɛ.
> - « Viens qu’on vive ensemble !» est égal à « Viens qu’on fasse palabre !»

> #Fiyentɔya o, #nanbaraya o, wurudennin ka #nin bɛɛ kɛ ka tɛmɛn.
> - Vivre sans voir, sans marcher, le petit chiot a passé par toutes ces étapes.

> Ka #mɔgɔ ye, ani k’a lɔn, o tɛ #kelen ye.
> - Voir l’homme et connaître l’homme sont deux choses différentes.

> 
> - 

