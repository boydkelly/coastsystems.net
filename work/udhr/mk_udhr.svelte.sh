#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

# Determine the script's directory
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PROJECT_ROOT="$(cd "$SCRIPT_DIR/../.." && pwd)" # Ensures absolute path to project root

function maindoc() {
  lang=$1
  target="$PROJECT_ROOT/src/routes/$lang/docs/udhr/Content.svelte"
  source="$SCRIPT_DIR/udhr_$lang.xml"
  xslt="$SCRIPT_DIR/udhr.xsl"

  echo "$target"
  echo "$source"

  [[ -d $PROJECT_ROOT/src/routes/$lang/docs/udhr ]] || mkdir -p "$PROJECT_ROOT/src/routes/$lang/docs/udhr"
  npx xslt3 -xsl:"$xslt" -s:"$source" >"$target"
  echo "Copied $source to $target"
}

# Loop through languages
for lang in en fr bm dyu bci; do
  maindoc "$lang"
done
